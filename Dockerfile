FROM archstable

COPY target/debug/damas /damas
COPY target/debug/as.sh as.sh
COPY light.py /light.py

WORKDIR /
ENTRYPOINT ["/damas"]
