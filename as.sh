#!/bin/bash

FILE=$(mktemp -u)
ASM="$FILE.s"
OBJ="$FILE.o"
BIN="$FILE.bin"

touch "$BIN"

# Get assembly instructions from stdin.
cat <(echo ".globl _start") <(echo "_start:") - > $ASM

set -e

# Assemble the input and print it using objdump.
as -o "$OBJ" "$ASM"

if [ $# -eq 1 ]; then
  ld -Ttext="$1" -o "$FILE" "$OBJ"
  objcopy -O binary -j .text "$FILE" "$BIN"
  rm "$FILE"
else
  objcopy -O binary -j .text "$OBJ" "$BIN"
fi

cat "$BIN"

rm "$ASM"
rm "$OBJ"
rm "$BIN"
