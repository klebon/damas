import sys

from angr import Project
from capstone.x86_const import *
from cle.loader import Loader
from networkx.exception import NetworkXError

class LightCFG:
    """
    Lightweight CFG protection that enforces a very coarse-grained CFG.
    """

    DEFAULT_LOAD_OPTIONS = {
        'auto_load_libs': False
    }

    DEFAULT_MAIN_OPTIONS = {
        'base_addr': 0,
        'force_rebase': True
    }

    JUMP_INSTRUCTIONS = [
        "ret", "retd", "retq", "ja", "jae", "jb", "jbe", "jc", "jcxz",
        "je", "jecxz", "jg", "jge", "jl", "jle", "jmp", "jna", "jnae", "jnb",
        "jnbe", "jne", "jng", "jnge", "jnl", "jnle", "jno", "jnp", "jns",
        "jnz", "jo", "jp", "jpe", "jpo", "jrcxz", "js", "jz", "call", "callq"
    ]

    def __init__(self, path: str, **kwargs):
        """
        Initialize the LightCFG project with a target executable path.
        Optional arguments will be passed to angr's Project class constructor.
        """

        if 'load_option' not in kwargs.keys():
            self.load_options = self.DEFAULT_LOAD_OPTIONS
        else:
            self.load_options = kwargs['load_options']
            del kwargs['load_options']

        if 'main_opts' not in kwargs.keys():
            self.main_options = self.DEFAULT_MAIN_OPTIONS
        else:
            self.main_options = kwargs['main_opts']
            del kwargs['main_opts']

        self.executable_path = path
        self.project = Project(self.executable_path,
                               load_options=self.load_options,
                               main_opts=self.main_options,
                               **kwargs)

    def is_pic(self):
        """
        Tell if the binary is position independant.
        """
        return self.project.loader.main_object.pic

    def generate_cfg_with_emulated(self, indirect_jumps=False, functions=None):
        """
        Generate the CFG of the whole program using the CFGEmulated algorithm.
        By default, indirect jumps are not resolved.

        :param indirect_jumps   Tells if the indirect jumps shall be resolved.
        :param functions        The list of function-start addresses to use.
        """
        self.cfg = analyses.CFGEmulated(resolve_indirect_jumps=indirect_jumps,
                                        context_sensitivity_level=1,
                                        normalize=True)
        for start in functions:
            try:
                self.cfg.resume([start])
            except NetworkXError as e:
                import traceback
                import parse
                msg = "The node <BlockNode at 0x{:x} (size {})> is not in the digraph."
                res = parse.parse(msg, str(e))
                if res is not None:
                    addr = res.fixed[0]
                    if self.cfg.get_any_node(addr) is not None:
                        print("Wtf the block at %d exists" % (addr))
            except Exception as e:
                print("Error with function at 0x%x: %s" % (start, str(e)))

        for addr in self.project.kb.functions:
            func = self.project.kb.functions.function(addr)
            func.normalize()

        return self.cfg

    def generate_cfg(self, indirect_jumps=True, functions=None):
        """
        Generate the CFG of the whole program using the CFGFast algorithm. By
        default, indirect jumps are resolved.

        :param indirect_jumps   Tells if the indirect jumps shall be resolved.
        :param functions        The list of function-start addresses to use.
        """
        analyses = self.project.analyses
        self.cfg = analyses.CFGFast(resolve_indirect_jumps=indirect_jumps,
                                    data_references=True,
                                    symbols=True,
                                    elf_eh_frame=True,
                                    force_complete_scan=True,
                                    function_starts=functions,
                                    normalize=True)

        return self.cfg

    def functions(self):
        """
        Retrieve every functions in the binary that angr was able to discover.
        """
        def extract_info(func):
            blocks = sorted(func.blocks, key=lambda blk: blk.addr)
            if len(blocks) > 0:
                end_addr = blocks[-1].addr + blocks[-1].size
            else:
                end_addr = func.addr
            block_bounds = [(blk.addr, blk.size) for blk in blocks]
            return (func.addr, end_addr, block_bounds)

        fm = self.project.kb.functions
        funcs = [func for func in fm]
        return [extract_info(fm.function(func)) for func in funcs]


    def blocks(self):
        """
        Retrieve every basic blocks in the CFG recovered by angr.
        """
        blocks = []
        for node in self.cfg.model.nodes():
            if node is not None:
                block = node.block
                if block is not None:
                    pair = (block.addr, block.size)
                    blocks.append(pair)
        return sorted(blocks, key=lambda blk: blk[0])

    def block_from_address(self, addr):
        """
        Get a block in the CFG using its address.
        """
        node = self.cfg.model.get_any_node(addr)
        if node is not None:
            return node.block
        return None

    def block_terminator(self, block):
        """
        Get the block's last instruction.
        """
        terminator = None
        for terminator in block.capstone.insns:
            pass
        return terminator

    def block_successor_addresses(self, block):
        """
        Get the successors of a block according to the computed CFG.
        """
        node = self.cfg.model.get_any_node(block.addr)
        [node.addr for node in self.cfg.model.get_successors(node)]

    def block_targets(self, block):
        """
        Get the possible targets of the given block. If the jump is direct, the
        targets are obvious however if it is indirect, there are two
        possibilities:

        - it is a call : every functions are valid targets
        - it is a jump : every addresses within the function are valid targets
        """
        if len(block.instruction_addrs) == 0:
            return None
        terminator = self.block_terminator(block)

        insn = terminator.mnemonic
        if insn in ["ret", "retq"]:
            return ("ret", self.block_successor_addresses(block))

        op = terminator.operands[0]
        if insn in ["call", "callq"]:
            if op.type in [X86_OP_IMM]:
                return ("call", [op.value.imm])
            return ("call", [addr for addr in self.functions()])
        elif insn in self.JUMP_INSTRUCTIONS:
            if op.type in [X86_OP_IMM]:
                return ("jump", [op.value.imm])
            func = self.project.kb.functions.floor_func(terminator.address)
            tgts = []
            for blk in func.blocks:
                for insn in blk.capstone.insns:
                    tgts.append(insn.address)
            return ("jump", sorted(tgts))
        else:
            return ("misc", [terminator.address + terminator.size])
