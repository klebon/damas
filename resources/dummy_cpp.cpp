#include <iostream>

class Logger {
public:
  void print(const std::string& str) {
    std::cout << str << std::endl;
  }
};

int main() {
  Logger log;
  log.print("Hello world!");

  return 0;
}
