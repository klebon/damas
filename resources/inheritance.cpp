#include <iostream>

class A {
public:
  virtual ~A() {}
  virtual void hello() = 0;
};

class B : public A {
public:
  void hello() {
    std::cout << "Hello, I'm B" << std::endl;
  }
};

class C : public A {
public:
  void hello() {
    std::cout << "Hello, I'm C" << std::endl;
  }
};

int main(int argc, char* argv[]) {
  A* a;

  if (argc <= 2) {
    a = new B;
  } else {
    a = new C;
  }

  a->hello();

  delete a;
  return 0;
}
