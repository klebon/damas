#include <algorithm>
#include <vector>

template<typename It>
void quicksort(It begin, It end) {
  if(begin == end) return;

  auto pivot = *begin;
  It sep1 = std::partition(begin, end, [pivot](const auto& e) { return e < pivot; });
  It sep2 = std::partition(sep1, end, [pivot](const auto& e) { return !(e < pivot); });

  quicksort(begin, sep1);
  quicksort(sep2, end);
}

int main() {
  std::vector<int> v = {1, 4, 3, 5, 3, 0, 2};
  quicksort(v.begin(), v.end());
  return 0;
}

