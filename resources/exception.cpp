#include <iostream>

void stuff() {
  std::cout << "Before" << std::endl;
  throw std::string("error");
  std::cout << "After" << std::endl;
}

int main() {
  try {
    stuff();
  }

  catch(const std::string& e) {
    std::cout << e << std::endl;
  }

  return 0;
}
