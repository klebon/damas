#include <stdio.h>

enum Cmd {
  STOP, PLAY, NEXT
};

int main(int argc, char* argv[]) {
  if (argc < 1 || 3 < argc) {
    puts("Wrong number of arguments");
  } else {
    enum Cmd cmd = argc - 1;

    switch(cmd) {
      case STOP:
        puts("Stop please");
        break;
      case PLAY:
        puts("Play please");
        break;
      case NEXT:
        puts("Next please");
        break;
    }
  }

  return 0;
}
