#include <stdio.h>

void hello() {
  puts("Hello World");
}

int main(void) {
  void (*f)() = hello;
  f();
  return 0;
}

