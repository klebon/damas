#include <stdio.h>
#include <stdlib.h>

void print_message(void) {
  puts("Hello");
}

void exit_0x99(void) {
  exit(0x99);
}

int main(void) {
  print_message();
  exit_0x99();
  return 0;
}
