#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

void* thread_kernel(void* arg) {
  const char* str = (const char*)(arg);
  puts(str);

  pthread_exit(NULL);
}

int main(void) {
  pthread_t thread1;

  if(pthread_create(&thread1, NULL, thread_kernel, "Hello World") == -1) {
    perror("pthread_create");
    return 1;
  }

  puts("Main hello world");

  if(pthread_join(thread1, NULL)) {
    perror("pthread_join");
    return 1;
  }

  return 0;
}

