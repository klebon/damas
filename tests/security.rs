use std::io::Write;
use std::path::Path;

use elf::types::SectionHeader;

use nix::sys::wait::WaitStatus;

use sorry::TargetProcess;
use sorry::breakpoint::{Breakpoint, Mode};
use sorry::buffer::Buffer;
use sorry::buffer::codecache::CodeCache;
use sorry::error::Error as SorryError;

use damas::disass::{Disassembly, EdgeKind};
use damas::disass::light_disass::LightDisassembly;
use damas::eh_frame::{EhFrameFunction, function_symbols, scan_eh_frame};
use damas::error::Result;
use damas::log::Logger;
use damas::reloc::dispatch::DispatchTable;
use damas::reloc::stext::{create_stext_section, STextSection};

static dummy2: &str = "resources/dummy2";
static dummy6: &str = "resources/dummy6";

fn reach_main_function(target: &TargetProcess) -> Result<()> {
    let mut ctrl = target.get_controller();

    let load_addr = ctrl.get_absolute_load_address();
    let main_offset = target.get_main_function_offset()?;
    let mut main_breakpoint = Breakpoint::new(load_addr + main_offset, Mode::OneShot);

    main_breakpoint.set(&ctrl)?;
    ctrl.resume(false)?;

    let brk_status = target.wait(None)?;
    main_breakpoint.clean_code(&ctrl)?;
    ctrl.load_map_file().map_err(&SorryError::into)
}

fn reach_main_insn(target: &TargetProcess, offset: u64) -> Result<()> {
    let mut ctrl = target.get_controller();
    let mut regs = ctrl.get_registers()?;

    let mut call_breakpoint = Breakpoint::new(regs.rip.wrapping_add(offset), Mode::OneShot);

    call_breakpoint.set(&ctrl)?;
    ctrl.resume(false)?;

    target.wait(None)?;
    call_breakpoint.clean_code(&ctrl).map_err(&SorryError::into)
}

fn reach_indirect_call(target: &TargetProcess) -> Result<()> {
    reach_main_insn(target, 0x1c)
}

fn reach_ret(target: &TargetProcess) -> Result<()> {
    reach_main_insn(target, -1 as i64 as u64)
}

fn is_rip_at_indirect_call(target: &TargetProcess) -> Result<()> {
    let ctrl = target.get_controller();
    let regs = ctrl.get_registers()?;
    let load_addr = ctrl.get_absolute_load_address();
    let main_addr = load_addr + target.get_main_function_offset()?;
    assert_eq!(main_addr + 0x1c, regs.rip);

    Ok(())
}

/// Create a LightDisassembly from the given target process.
fn light_disass(logger: &mut Logger, target: &TargetProcess)
-> Result<(Vec<EhFrameFunction>, LightDisassembly)> {
    let executable = Path::new(target.get_executable_name());
    let load_addr = target.get_controller().get_absolute_load_address();

    writeln!(logger, "Process loaded at 0x{:x}", load_addr)?;
    let eh_funcs = scan_eh_frame(Path::new(executable))?;
    let mut funcs = Vec::with_capacity(eh_funcs.len());

    for func in eh_funcs {
        let f = EhFrameFunction {
            start: load_addr + func.start,
            end: load_addr + func.end,
            fde_offset: func.fde_offset,
            fde_length: func.fde_length
        };

        funcs.push(f);
    }

    funcs.extend(function_symbols(target).into_iter());
    funcs.sort();
    funcs.dedup();

    let cfg = LightDisassembly::new(executable, load_addr)?;
    cfg.generate_cfg(true, Some(funcs.clone()))?;

    writeln!(logger, "Disassembly successfully built")?;
    Ok((funcs, cfg))
}

fn apply_damas(target: &TargetProcess, logger: &mut Logger)
-> Result<STextSection<CodeCache, (), () , String, SectionHeader>> {
    let (_, disass) = light_disass(logger, &target)?;
    let error = "mov $1,%eax\nmov $42,%ebx\nint $0x80\n".to_string();

    let dt: DispatchTable<(), _, _> = DispatchTable::new(error.clone());
    let rdt: DispatchTable<(), _, _> = DispatchTable::new(error.clone());
    let mut stext = create_stext_section(&target, &disass, dt, rdt, true)?;
    let blocks = disass.basic_blocks()?;
    disass.populate_dispatch_table(&mut stext.dt.dt, &blocks, EdgeKind::Call)?;
    stext.relocate(&target, &disass, |_addr| {
        error.clone()
    }, false)?;

    Ok(stext)
}

#[test]
fn successful_attack_non_protected() -> Result<()> {
    let mut target = TargetProcess::new(dummy2, vec![dummy2]);
    target.start().ok_or_else( || SorryError::CustomError("Could not start".to_string()))?;

    reach_main_function(&target)?;
    reach_indirect_call(&target)?;

    is_rip_at_indirect_call(&target)?;

    let ctrl = target.get_controller();
    let mut regs = ctrl.get_registers()?;

    let exit_symbol = ctrl.get_libc_symbol("exit")?;
    let (libc_start, _) = ctrl.get_libc_address_range().unwrap();

    regs.rdx = libc_start + exit_symbol.value + 8; // move past the endbr64 and sub rsp,8
    regs.rdi = 0x99;

    ctrl.set_registers(regs);
    ctrl.resume(false)?;

    let status = target.wait(None)?;

    assert_eq!(WaitStatus::Exited(target.get_pid(), 0x99), status);
    Ok(())
}

#[test]
fn failed_attack_protected() -> Result<()> {
    let mut target = TargetProcess::new(dummy2, vec![dummy2]);
    target.start().ok_or_else( || SorryError::CustomError("Could not start".to_string()))?;

    reach_main_function(&target)?;
    reach_indirect_call(&target)?;

    is_rip_at_indirect_call(&target)?;

    let mut logger = Logger::new(true);

    let ctrl = target.get_controller();
    let mut regs = ctrl.get_registers()?;
    let rip_backup = regs.rip;
    let stext = apply_damas(&target, &mut logger)?;

    regs.rip = stext.relocs.relocated_address(rip_backup).unwrap();

    assert!(stext.cache.get_address() < regs.rip);
    writeln!(logger, "Control-flow redirected from {:#x} to {:#x}", rip_backup, regs.rip)?;

    let exit_symbol = ctrl.get_libc_symbol("exit")?;
    let (libc_start, _) = ctrl.get_libc_address_range().unwrap();

    regs.rdx = libc_start + exit_symbol.value + 8; // move past the endbr64 and sub rsp,8
    regs.rdi = 0x99;

    ctrl.set_registers(regs);
    ctrl.resume(false)?;

    let status = target.wait(None)?;

    assert_eq!(WaitStatus::Exited(target.get_pid(), 42), status);
    Ok(())
}

#[test]
fn successful_rop_non_protected() -> Result<()> {
    let mut target = TargetProcess::new(dummy2, vec![dummy2]);
    target.start().ok_or_else( || SorryError::CustomError("Could not start".to_string()))?;

    reach_main_function(&target)?;
    reach_ret(&target)?;

    let ctrl = target.get_controller();
    let mut regs = ctrl.get_registers()?;

    let (libc_start, _) = ctrl.get_libc_address_range().unwrap();

    ctrl.write_word(libc_start + 0xfa169, regs.rsp)?;
    ctrl.write_word(0x99, regs.rsp + 0x8)?;
    ctrl.write_word(libc_start + 0x4cf86, regs.rsp + 0x10)?;
    ctrl.write_word(libc_start + 0x120b22, regs.rsp + 0x18)?;

    ctrl.resume(false)?;

    let status = target.wait(None)?;

    assert_eq!(WaitStatus::Exited(target.get_pid(), 0x99), status);
    Ok(())
}

#[test]
fn failed_rop_protected() -> Result<()> {
    let mut target = TargetProcess::new(dummy2, vec![dummy2]);
    target.start().ok_or_else( || SorryError::CustomError("Could not start".to_string()))?;

    reach_main_function(&target)?;
    reach_ret(&target)?;

    let mut logger = Logger::new(true);

    let stext = apply_damas(&target, &mut logger)?;

    let ctrl = target.get_controller();
    let mut regs = ctrl.get_registers()?;
    let rip_backup = regs.rip;
    regs.rip = stext.relocs.relocated_address(regs.rip).unwrap();

    assert!(stext.cache.get_address() < regs.rip);
    writeln!(logger, "Control-flow redirected from {:#x} to {:#x}", rip_backup, regs.rip)?;

    let (libc_start, _) = ctrl.get_libc_address_range().unwrap();

    ctrl.write_word(libc_start + 0xfa169, regs.rsp)?;
    ctrl.write_word(0x99, regs.rsp + 0x8)?;
    ctrl.write_word(libc_start + 0x4cf86, regs.rsp + 0x10)?;
    ctrl.write_word(libc_start + 0x120b22, regs.rsp + 0x18)?;

    ctrl.set_registers(regs)?;
    ctrl.resume(false)?;

    let status = target.wait(None)?;

    assert_eq!(WaitStatus::Exited(target.get_pid(), 42), status);
    Ok(())
}

#[test]
fn successful_rop_protected() -> Result<()> {
    let mut target = TargetProcess::new(dummy6, vec![dummy6]);
    target.start().ok_or_else( || SorryError::CustomError("Could not start".to_string()))?;

    reach_main_function(&target)?;
    reach_main_insn(&target, -0xf as i64 as u64)?;

    let mut logger = Logger::new(true);

    let stext = apply_damas(&target, &mut logger)?;

    let ctrl = target.get_controller();
    let mut regs = ctrl.get_registers()?;
    let rip_backup = regs.rip;
    regs.rip = stext.relocs.relocated_address(regs.rip).unwrap();

    assert!(stext.cache.get_address() < regs.rip);
    writeln!(logger, "Control-flow redirected from {:#x} to {:#x}", rip_backup, regs.rip)?;

    ctrl.write_word(rip_backup + 0x1d, regs.rsp)?;

    ctrl.set_registers(regs)?;
    ctrl.resume(false)?;

    let status = target.wait(None)?;

    assert_eq!(WaitStatus::Exited(target.get_pid(), 0), status);
    Ok(())
}
