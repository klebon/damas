use std::convert::TryInto;
use std::fmt::Display;
use std::fs::File;
use std::io::Write;

use sorry::TargetProcess;
use sorry::buffer::Buffer;

use crate::disass::Function;
use crate::eh_frame::EhFrameFunction;
use crate::error::Result;
use crate::log::Logger;
use crate::reloc::SectionRange;
use crate::reloc::dispatch::DispatchCase;
use crate::reloc::feature::{Counter, Feature};
use crate::reloc::stext::{FunctionTableMap, STextSection};
use crate::reloc::trans::InjectableDispatchTable;

#[allow(dead_code)]
pub fn print_eh_frame_functions<'a, I>(logger: &mut Logger, funcs: I) -> Result<()>
where I: Iterator<Item = &'a EhFrameFunction> {
    for func in funcs.filter( |func| func.fde_offset != 0 ) {
        writeln!(logger, "At {:#x}: {:#x}-{:#x}", func.fde_offset, func.start, func.end)?;
    }

    Ok(())
}

pub fn print_every_tables<B, F1, F2, F3, E, R>(
    logger: &mut Logger,
    target: &TargetProcess,
    stext: &STextSection<B, F1, F2, E, R>,
    jdts: &FunctionTableMap<B, F3, E>) -> Result<()>
where B: Buffer,
      F1: Feature, F2: Feature, F3: Feature,
      E: Clone + Display, R: Clone + SectionRange {
    let main_table = &stext.dt;

    writeln!(logger, "Main table at {:#x} has {} cases",
        main_table.address.unwrap_or(0),
        main_table.dt.cases.len())?;

    for (func, dt) in jdts.iter() {
        let func_name = function_name_from_address(target, func);

        writeln!(logger, "Table for function {} at {:#x} has {} cases",
            func_name,
            dt.address.unwrap_or(0),
            dt.dt.cases.len())?;
    }

    Ok(())
}

pub fn function_name_from_address(target: &TargetProcess, func: &Function) -> String {
        let func_offset = func.start_address - target.get_controller().get_absolute_load_address();
        let sym_opt = target.get_all_symbols()
            .into_iter()
            .find( |sym| sym.value == func_offset );

        match sym_opt {
            Some(sym) => sym.name,
            None => format!("{:#x}", func_offset)
        }
}

pub fn print_counter<Case, W>(
    writer: &mut W,
    value: u32,
    idx: usize,
    cases: &[Case]) -> Result<()>
where Case: DispatchCase<Counter>, W: Write {
    // If idx is null, then it is the table counter, it must be told to the user.
    if idx == 0 {
        writeln!(writer, "Table counter: {}", value)?;
    } else {
        let case = &cases[idx-1];
        writeln!(writer, "Counter #{} for {:#x}: {}", idx, case.identifier(), value)?;
    }

    Ok(())
}

pub fn print_table_counters<B, Case, E, W>(
    writer: &mut W,
    table: &InjectableDispatchTable<B, Counter, Case, E>) -> Result<()>
where B: Buffer, Case: DispatchCase<Counter>, E: Clone + Display, W: Write {
    let mut cases = table.dt.cases.clone();
    cases.sort_by_key( |k| k.identifier() );

    let size = cases.len();
    let buffer = table.cache.as_ref().unwrap();

    for idx in 0 .. size+1 {
        let longword = buffer.read(4, idx * 4)?;
        // We know from the read operation that the Vec is 4 bytes long.
        let value = u32::from_ne_bytes(longword.try_into().unwrap());

        print_counter(writer, value, idx, &cases)?;
    }

    Ok(())
}

pub fn print_all_table_counters<B, E, R, W>(
    writer: &mut W,
    target: &TargetProcess,
    stext: &STextSection<B, Counter, Counter, E, R>,
    jdts: &FunctionTableMap<B, Counter, E>) -> Result<()>
where B: Buffer, E: Clone + Display, R: Clone + SectionRange, W: Write {
    let title = format!("Dispatch table counters for the execution of {} (PID: {})",
        target.get_executable_name(),
        target.get_pid());

    writeln!(writer, "{}\n{:=<2$}\n", title, "", title.len())?;

    writeln!(writer, "Main table\n----------")?;
    print_table_counters(writer, &stext.dt)?;

    writeln!(writer, "\nReturn table\n------------")?;
    print_table_counters(writer, &stext.rdt)?;

    for (func, dt) in jdts.iter() {
        let func_name = function_name_from_address(target, func);
        let title = format!("\nTable for {} at {:#x}", func_name, dt.address.unwrap());
        writeln!(writer, "{}\n{:-<2$}", title, "", title.len())?;

        print_table_counters(writer, dt)?;
    }

    Ok(())
}

pub fn dump_tables<B, F1, F2, F3, E, R>(
    mut file: File,
    target: &TargetProcess,
    stext: &STextSection<B, F1, F2, E, R>,
    jdts: &FunctionTableMap<B, F3, E>) -> Result<()>
where B: Buffer,
      F1: Feature, F2: Feature, F3: Feature,
      E: Clone + Display,
      R: Clone + SectionRange {
    writeln!(file, "Main table:\n{}\n", stext.dt.dt)?;
    writeln!(file, "Return table:\n{}\n", stext.rdt.dt)?;

    for (func, dt) in jdts.iter() {
        let func_name = function_name_from_address(target, func);
        writeln!(file, "Table for {} at {:#x}\n{}\n", func_name, dt.address.unwrap(), dt.dt)?;
    }

    Ok(())
}
