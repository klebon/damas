#![allow(dead_code)]
//! The performance monitoring system of Damas.
//!
//! Damas is not only a dynamic-binary-translation software, it will also analyse the target
//! process' execution in order to modify it further and improve its execution time. The purpose of
//! this module is to implement the monitoring of the target process. It uses the
//! [`perf-event API`] of the Linux kernel.
//!
//! [`perf-event API`]: https://man7.org/linux/man-pages/man2/perf_event_open.2.html

pub mod counter;

use std::io::Error as IOError;
use std::mem::size_of;
use std::ops::{Deref, DerefMut};

use nix::errno::errno;
use nix::poll::{PollFd, PollFlags};
use nix::sys::mman::{mmap, MapFlags, ProtFlags};
use nix::unistd::{close, Pid, sysconf, SysconfVar};

use perf_event_open_sys::perf_event_open;
use perf_event_open_sys::bindings::{
    perf_event_attr,
    perf_event_header,
    perf_event_mmap_page,
    perf_event_sample_format_PERF_SAMPLE_ADDR as PERF_SAMPLE_ADDR,
    perf_event_sample_format_PERF_SAMPLE_CALLCHAIN as PERF_SAMPLE_CALLCHAIN,
    perf_event_sample_format_PERF_SAMPLE_CPU as PERF_SAMPLE_CPU,
    perf_event_sample_format_PERF_SAMPLE_ID as PERF_SAMPLE_ID,
    perf_event_sample_format_PERF_SAMPLE_IP as PERF_SAMPLE_IP,
    perf_event_sample_format_PERF_SAMPLE_PERIOD as PERF_SAMPLE_PERIOD,
    perf_event_sample_format_PERF_SAMPLE_STREAM_ID as PERF_SAMPLE_STREAM_ID,
    perf_event_sample_format_PERF_SAMPLE_TID as PERF_SAMPLE_TID,
    perf_event_sample_format_PERF_SAMPLE_TIME as PERF_SAMPLE_TIME,
    perf_type_id_PERF_TYPE_HARDWARE as PERF_TYPE_HARDWARE,
};
use perf_event_open_sys::ioctls::{DISABLE, ENABLE};

use crate::error::{DamasError, Result};

/// MMAP size (i.e. number of pages) must be a power of 2 + 1, where the first page is a meta-data
/// page ([`struct perf_event_mmap_page`]).
///
/// [`perf_event_mmap_page`]: https://man7.org/linux/man-pages/man2/perf_event_open.2.html
const PROFILE_MMAP_SIZE: usize = 5;

/// Error in the monitor module.
#[derive(Debug, PartialEq, Eq)]
pub enum Error {
    /// The perf event has not been opened by perf_event_open.
    NotOpen,
}

/// Representation of a perf event in the Linux kernel.
///
/// This structure is used by both [`Sample`] and [`Counter`] to manipulate the common perf-event
/// interface.
///
/// [`Counter`]: ./struct.Counter.html
/// [`Sample`]: ./struct.Sample.html
pub struct PerfEvent {
    pub fd: Option<i32>,
    pub pid: Pid,
    pub attr: perf_event_attr
}

impl Drop for PerfEvent {
    /// If the perf-event was open, the file descriptor must be closed properly on destruction.
    fn drop(&mut self) {
        if let Some(fd) = self.fd {
            let _ = close(fd);
        }
    }
}

impl PerfEvent {
    /// Create a new default and uninitalized perf event.
    pub fn new(pid: Pid) -> PerfEvent {
        PerfEvent {
            fd: None,
            pid: pid,
            attr: perf_event_attr::default()
        }
    }

    /// Open the perf event and bind the corresponding file descriptor for subsequent reads.  If
    /// the call to [`perf_event_open`] succeeded, the file descriptor is stored. Otherwise an
    /// [`IO error`] is returned.
    ///
    /// [`perf_event_open`]: https://man7.org/linux/man-pages/man2/perf_event_open.2.html
    /// [`IO error`]: ./../error/enum.DamasError.html#variant.IOError
    pub fn open(&mut self) -> Result<i32> {
        let raw_pid = self.pid.as_raw();
        let fd = unsafe { perf_event_open(&mut self.attr as *mut _, raw_pid, -1, -1, 0) };

        if fd == -1 {
            let err = IOError::from_raw_os_error(errno());
            Err(DamasError::IOError(err))
        } else {
            self.fd = Some(fd);
            Ok(fd)
        }
    }

    /// Enable the perf-event.
    ///
    /// The perf-event is enabled using the syscall [`ioctl`]. If it fails, an [`IO error`] is
    /// returned.
    ///
    /// [`ioctl`]: https://man7.org/linux/man-pages/man2/ioctl.2.html
    /// [`IO error`]: ./../error/enum.DamasError.html#variant.IOError
    pub fn enable(&mut self) -> Result<()> {
        match self.fd {
            None => Err(DamasError::from(Error::NotOpen)),
            Some(fd) => {
                let res = unsafe { ENABLE(fd, 0) };

                if res != 0 {
                    let err = IOError::from_raw_os_error(errno());
                    Err(DamasError::IOError(err))
                } else {
                    Ok(())
                }
            }
        }
    }

    /// Disable the counter.
    pub fn disable(&mut self) -> Result<()> {
        match self.fd {
            None => Err(DamasError::from(Error::NotOpen)),
            Some(fd) => {
                let res = unsafe { DISABLE(fd, 0) };

                if res != 0 {
                    let err = IOError::from_raw_os_error(errno());
                    Err(DamasError::IOError(err))
                } else {
                    Ok(())
                }
            }
        }
    }
}

/// Representation of a perf counter.
///
/// A counter counts occurences of a certain event (e.g. cache misses, executed instructions...).
/// The counting results are retrieved by reading from the perf-event file descriptor. By default,
/// a counter is not enabled and must be enabled with [`enable`] to start counting.
///
/// [`enable`]: ./struct.Counter.html#method.enable
pub struct Counter {
    event: PerfEvent
}

impl Counter {
    /// Create a new counter and initialize it. This function creates the perf-event and opens it.
    pub fn new(pid: Pid, config: u64) -> Result<Counter> {
        let mut event = PerfEvent::new(pid);

        event.attr.type_ = PERF_TYPE_HARDWARE;
        event.attr.size = size_of::<perf_event_attr>() as u32;
        event.attr.config = config;
        event.attr.set_exclude_kernel(1);
        event.attr.set_exclude_hv(1);
        event.attr.set_exclude_idle(1);
        event.attr.set_inherit_stat(1);
        event.open()?;

        Ok(Counter {
            event: event,
        })
    }
}

impl Deref for Counter {
    type Target = PerfEvent;

    fn deref(&self) -> &Self::Target {
        &self.event
    }
}

impl DerefMut for Counter {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.event
    }
}

/// Representation of a perf sampling event.
///
/// A sampling event periodically writes measurements to a buffer that can be accessed via
/// [`mmap`]. It is used to have results of measurements during a certain period of time unlike
/// counters.
///
/// [`mmap`]: https://man7.org/linux/man-pages/man2/mmap.2.html
pub struct Sampling {
    event: PerfEvent,
    pollfd: PollFd,
    offset: usize,
    ring_buffer: u64
}

impl Deref for Sampling {
    type Target = PerfEvent;

    fn deref(&self) -> &Self::Target {
        &self.event
    }
}

impl DerefMut for Sampling {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.event
    }
}

/// Retrieve the size of a page in bytes. If the size has no limit, [`sysconf`] returns -1, this is
/// represented by the maximum unsigned value.
///
/// [`sysconf`]: https://man7.org/linux/man-pages/man3/sysconf.3.html
fn pagesize() -> Result<usize> {
    match sysconf(SysconfVar::PAGE_SIZE) {
        Err(e) => Err(DamasError::NixError(e)),
        Ok(None) => Ok(std::usize::MAX),
        Ok(Some(val)) => Ok(val as usize)
    }
}

impl Sampling {
    /// Create a new sampling counter and initialize it. This function opens the perf-event and
    /// mmap the associated ring buffer. If the mmap fails somehow, the opened file descriptor is
    /// closed safely.
    pub fn new(pid: Pid, config: u64, frequency: u64, sample_type: u64) -> Result<Sampling> {
        let mut event = PerfEvent::new(pid);

        event.attr.type_ = PERF_TYPE_HARDWARE;
        event.attr.size = size_of::<perf_event_attr>() as u32;
        event.attr.config = config;
        event.attr.sample_type = sample_type;
        event.attr.__bindgen_anon_1.sample_freq = frequency;

        event.attr.set_exclude_kernel(1);
        event.attr.set_exclude_hv(1);
        event.attr.set_exclude_idle(1);
        event.attr.set_inherit_stat(1);
        event.attr.set_freq(1);
        event.attr.set_precise_ip(0);
        event.attr.set_watermark(0);

        let prot = ProtFlags::PROT_READ | ProtFlags::PROT_WRITE;
        let size = PROFILE_MMAP_SIZE * pagesize()?;

        let fd = event.open()?;

        unsafe {
            match mmap(0 as *mut _, size, prot, MapFlags::MAP_SHARED, fd, 0) {
                // If the mmap call returned successfully, we can return a Sampling, otherwise the
                // file descriptor opened by PerfEvent::new is closed before returning an error.
                Ok(rmap) => Ok(Sampling {
                    event: event,
                    pollfd: PollFd::new(fd, PollFlags::POLLIN),
                    offset: 0,
                    ring_buffer: rmap as u64
                }),

                Err(e) => {
                    close(fd)?;
                    Err(e.into())
                }
            }
        }
    }

    /// From [`PERF_EVENT_OPEN(2)`] linux programmer's manual
    ///
    /// data head:
    /// This points to the head of the data section. The value continuously
    /// increases, it does not wrap. The value needs to be manually wrapped
    /// by the size of the mmap buffer before accessing the samples.
    ///
    /// TODO: On SMP-capable platforms, after reading the data_head value, user
    /// space should issue an rmb().
    ///
    /// [`PERF_EVENT_OPEN(2)`]: http://www.man7.org/linux/man-pages/man2/perf_event_open.2.html
    fn ring_buffer_head(&self) -> u64 {
        let pemp = unsafe { *(self.ring_buffer as *const perf_event_mmap_page) };
        pemp.data_head
    }

    /// Write to ring buffer data_tail to reflect the last read data. According to perf_event.h,
    /// the kernel will then not overwrite unread data.
    fn write_ring_buffer_tail(&mut self, tail: u64) {
        unsafe {
            let ptr = self.ring_buffer as *mut perf_event_mmap_page;
            (*ptr).data_tail = tail;
        }
    }

    /// Retrieve the next sample event from the ring buffer. This does not increase the offset
    /// cursor.
    fn next_sample_event<'a>(&'a mut self) -> Result<&SampleEvent<'a>> {
        let pagesize = pagesize()?;
        let rmask: usize = (PROFILE_MMAP_SIZE - 1) * pagesize - 1;

        let head = self.ring_buffer_head() as usize;
        let data = self.ring_buffer as usize + pagesize;
        let ptr = (data + (head & rmask)) as *mut SampleEvent<'a>;

        unsafe { Ok(&*ptr) }
    }
}

#[derive(Default)]
struct SampleRecord {
    header: perf_event_header,
    pc: u64,
    pid: u32,
    tid: u32,
    time: u64,
    addr: u64,
    id: u64,
    stream_id: u64,
    cpu: u32,
    period: u64,
    callchain: Vec<u64>,
    // Seems unused.
    #[allow(unused)]
    data: Vec<u8>
}

impl SampleRecord {
    /// Parse the perf-event and create a record with it.
    pub fn from_event(event: SampleEvent<'_>, sample_type: u64) -> Self {
        let mut record = Self::default();
        let mut idx = 0;

        if sample_type & PERF_SAMPLE_IP != 0 {
            record.pc = event.data[idx];
            idx += 1;
        }

        if sample_type & PERF_SAMPLE_TID != 0 {
            let data32 = unsafe { &*(event.data as *const [u64] as *const [u32]) };
            record.pid = data32[2 * idx];
            record.tid = data32[2 * idx + 1];
            idx += 1;
        }

        if sample_type & PERF_SAMPLE_TIME != 0 {
            record.time = event.data[idx];
            idx += 1;
        }

        if sample_type & PERF_SAMPLE_ADDR != 0 {
            record.addr = event.data[idx];
            idx += 1;
        }

        if sample_type & PERF_SAMPLE_ID != 0 {
            record.id = event.data[idx];
            idx += 1;
        }

        if sample_type & PERF_SAMPLE_STREAM_ID != 0 {
            record.stream_id = event.data[idx];
            idx += 1;
        }

        if sample_type & PERF_SAMPLE_CPU != 0 {
            let data32 = unsafe { &*(event.data as *const [u64] as *const [u32]) };
            record.pid = data32[2 * idx];
            idx += 1;
        }

        if sample_type & PERF_SAMPLE_PERIOD != 0 {
            record.period = event.data[idx];
            idx += 1;
        }

        if sample_type & PERF_SAMPLE_CALLCHAIN != 0 {
            // The first 64-bit integer here is the number of elements.
            let count = event.data[idx] as usize;
            idx += 1;

            let mut callchain = Vec::with_capacity(count);

            while idx < count {
                callchain.push(event.data[idx]);
                idx += 1;
            }

            record.callchain = callchain;
        }

        record
    }
}

#[repr(C)]
struct SampleEvent<'a> {
    header: perf_event_header,
    size: usize,
    data: &'a [u64]
}
