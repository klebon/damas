use std::collections::BTreeMap;
use std::convert::TryInto;
use std::fmt::Display;
use std::sync::{Arc, Condvar, Mutex};
use std::thread::sleep;
use std::time::Duration;

use clap::ArgMatches;

use nix::libc::user_regs_struct;
use nix::sys::signal::Signal;

use sorry::TargetController;
use sorry::buffer::Buffer;

use crate::disass::Function;
use crate::error::{DamasError, Result};
use crate::reloc::dispatch::{DispatchCase, DispatchFunction, DispatchRet};
use crate::reloc::dispatch::commands;
use crate::reloc::feature::{Counter, Feature};
use crate::log::Logger;
use crate::reloc::SectionRange;
use crate::reloc::patch::TablePatcher;
use crate::reloc::stext::{FunctionTableMap, STextSection};
use crate::reloc::trans::InjectableDispatchTable;

/// Structure that encapsulates the whole monitoring context. This makes the rest of the code
/// easier to maintain.
pub struct MonitorContext<'a> {
    matches: &'a ArgMatches<'a>,
    ctrl: Arc<TargetController>,
    end_condition: Arc<Mutex<bool>>,
    currently_emitting: Arc<Mutex<bool>>,
    need_registers: Arc<(Mutex<(Need, user_regs_struct)>, Condvar)>
}

pub enum Need {
    RegWrite,
    RegRead,
    Nothing
}

impl<'a> MonitorContext<'a> {
    pub fn new(
        matches: &'a ArgMatches<'a>,
        ctrl: Arc<TargetController>,
        end_condition: Arc<Mutex<bool>>,
        currently_emitting: Arc<Mutex<bool>>,
        need_registers: Arc<(Mutex<(Need, user_regs_struct)>, Condvar)>) -> Self {
        MonitorContext {
            matches: matches,
            ctrl: ctrl,
            end_condition: end_condition,
            currently_emitting: currently_emitting,
            need_registers: need_registers
        }
    }

    pub fn must_end(&self) -> bool {
        self.end_condition.is_poisoned() || !(*self.end_condition.lock().unwrap())
    }

    pub fn emitting<F, R>(&self, mut func: F) -> R
    where F: FnMut() -> R {
        let mut lock = self.currently_emitting.lock().unwrap();
        *lock = true;
        func()
    }

    pub fn need_register_read(&self) -> Result<user_regs_struct> {
        let mut lock = self.need_registers.0.lock().unwrap();
        lock.0 = Need::RegRead;

        let mut lock = self.need_registers.1.wait(lock)
            .map_err( |err| DamasError::CustomError(format!("Mutex poisoned: {:?}", err)) )?;
        lock.0 = Need::Nothing;
        Ok(lock.1)
    }

    pub fn need_register_write(&self, regs: user_regs_struct) -> Result<()> {
        let mut lock = self.need_registers.0.lock().unwrap();
        lock.0 = Need::RegWrite;
        lock.1 = regs;

        let mut lock = self.need_registers.1.wait(lock)
            .map_err( |err| DamasError::CustomError(format!("Mutex poisoned: {:?}", err)) )?;
        lock.0 = Need::Nothing;

        Ok(())
    }
}

pub trait MonitorForFeature: Feature {
    fn monitor<B, E, R>(
        logger: &mut Logger,
        ctx: MonitorContext,
        stext: Arc<Mutex<STextSection<B, Self, Self, E, R>>>,
        jdts: &mut FunctionTableMap<B, Self, E>) -> Result<()>
    where B: Buffer, E: Clone + Display, R: Clone + SectionRange;
}

impl MonitorForFeature for Counter {
    fn monitor<B, E, R>(
        _logger: &mut Logger,
        ctx: MonitorContext,
        stext: Arc<Mutex<STextSection<B, Self, Self, E, R>>>,
        jdts: &mut FunctionTableMap<B, Self, E>) -> Result<()>
    where B: Buffer, E: Clone + Display, R: Clone + SectionRange {
        let default_sorted = ctx.matches.is_present("linear");
        let period = ctx.matches.value_of("period")
            .map( |x| x.parse() )
            .unwrap_or(Ok(1000))
            .map_err( |e| DamasError::CustomError(format!("{:?}", e)) )?;

        let mut dt_sorted = default_sorted;
        let mut rdt_sorted = default_sorted;

        let mut jdt_sorted: BTreeMap<Function, bool> = BTreeMap::new();

        while !ctx.must_end() {
            sleep(Duration::from_millis(period));

            let mut st = stext.lock().unwrap();
            let mut counters = table_counter_values(&st.dt, &st.rdt, jdts)?;
            // Remove the tables that were never used for better performance.
            let _ = counters.jump_counters.drain_filter( |_, v| *v == 0 ).collect::<Vec<_>>();

            if ctx.must_end() {
                return Ok(());
            }

            handle_table(&mut st.dt, counters.call_counter, &mut dt_sorted, &ctx)?;

            if ctx.must_end() {
                return Ok(());
            }

            handle_table(&mut st.rdt, counters.ret_counter, &mut rdt_sorted, &ctx)?;

            /*for (func, counter) in counters.jump_counters.iter() {
                if ctx.must_end() {
                    return Ok(());
                }

                let table = jdts.get_mut(func).unwrap();
                // TODO: Find a better performance criteria.
                //if *counter > 1000 {
                if *counter as usize > table.dt.cases.len() {
                    if !jdt_sorted.contains_key(func) {
                        jdt_sorted.insert(func.clone(), default_sorted);
                    }

                    let is_sorted = jdt_sorted.get_mut(func).unwrap();

                    eprint!("handling {:#x} ", func.start_address);
                    handle_table(table, *counter, is_sorted, &ctx)?;
                }
            }*/
        }
        Ok(())
    }
}

impl MonitorForFeature for () {
    fn monitor<B, E, R>(
        _logger: &mut Logger,
        _ctx: MonitorContext,
        _stext: Arc<Mutex<STextSection<B, Self, Self, E, R>>>,
        _jdts: &mut FunctionTableMap<B, Self, E>) -> Result<()>
    where B: Buffer, E: Clone + Display, R: Clone + SectionRange {
        Ok(())
    }
}

fn handle_table<B, Case, E>(
    table: &mut InjectableDispatchTable<B, Counter, Case, E>,
    counter: u32,
    sorted: &mut bool,
    ctx: &MonitorContext) -> Result<()>
where B: Buffer, Case: DispatchCase<Counter>, E: Clone + Display {
    let counters = table_counters(table)?;
    if should_sort(counters.clone()) {
        if !(*sorted && most_used_cases_in_order(table, &counters)) {
            emit_sorted_table(table, counter, counters, ctx)?;
            *sorted = true;
        }
    } else if *sorted {
        emit_tree_table(table, sorted, ctx)?;
    }

    Ok(())
}

fn rip_in_table<B, Case, E>(
    table: &mut InjectableDispatchTable<B, Counter, Case, E>,
    rip: u64) -> bool
where B: Buffer, Case: DispatchCase<Counter>, E: Clone + Display {
    let table_begin = table.address.unwrap();
    let table_end = table_begin + table.dt.estimate_size() as u64 - table.dt.table_offset(0);

    table_begin <= rip && rip < table_end
}

fn relocate_rip_if_in_table<B, Case, E>(
    table: &mut InjectableDispatchTable<B, Counter, Case, E>,
    ctx: &MonitorContext) -> Result<()>
where B: Buffer, Case: DispatchCase<Counter>, E: Clone + Display {
    let mut regs = ctx.need_register_read()?;
    if rip_in_table(table, regs.rip) {
        regs.rip = table.address.unwrap();
        ctx.need_register_write(regs)?;
    }
    Ok(())
}

fn emit_tree_table<B, Case, E>(
    table: &mut InjectableDispatchTable<B, Counter, Case, E>,
    is_sorted: &mut bool,
    ctx: &MonitorContext) -> Result<()>
where B: Buffer, Case: DispatchCase<Counter>, E: Clone + Display {
    table.dt.display_strategy = commands::tree_table_strategy();
    let address = table.address.unwrap();
    let buffer = table.dt.emit(address)?;
    let offset = table.dt.table_offset(0) as usize;

    ctx.emitting( || {
        let cache = table.cache.as_mut().unwrap();
        ctx.ctrl.stop(Signal::SIGTRAP)?;

        cache.write(&buffer, 0)?;
        //cache.write(&buffer.as_slice()[offset..], offset)?;
        *is_sorted = false;

        relocate_rip_if_in_table(table, ctx)
    })
}

fn emit_sorted_table<B, Case, E>(
    table: &mut InjectableDispatchTable<B, Counter, Case, E>,
    table_counter: u32,
    counters: Vec<u32>,
    ctx: &MonitorContext) -> Result<()>
where B: Buffer, Case: DispatchCase<Counter>, E: Clone + Display {
    let cases = sort_table(table, counters.clone())?;

    let offset = table.dt.table_offset(0) as usize;
    let counter_bytes: Vec<u8> = counters.into_iter()
        .map( |counter| counter.to_ne_bytes() )
        .flatten()
        .collect();

    ctx.emitting( || {
        ctx.ctrl.stop(Signal::SIGTRAP)?;

        let buffer = table.cache.as_mut().unwrap();
        //buffer.write(&table_counter.to_ne_bytes(), 0)?;
        //buffer.write(counter_bytes.as_ref(), 4)?;
        //buffer.write(&cases.as_slice()[offset..], offset)?;
        buffer.write(&cases, 0)?;

        relocate_rip_if_in_table(table, ctx)
    })
}

fn sort_table<B, Case, E>(
    table: &mut InjectableDispatchTable<B, Counter, Case, E>,
    counters: Vec<u32>) -> Result<Vec<u8>>
where B: Buffer, Case: DispatchCase<Counter>, E: Clone + Display {
    let mut cases: Vec<Case> = table.dt.cases.clone();
    cases.sort_by_key( |a| a.identifier() );

    let mut zipped: Vec<(u32, Case)> = counters.into_iter()
        .zip(cases.into_iter())
        .collect();

    zipped.sort_by( |(a, _), (b, _)| b.cmp(a) );

    table.dt.display_strategy = commands::linear_table_strategy();
    let mut patcher = TablePatcher::new(table);

    for (ndx, (_, c)) in zipped.into_iter().enumerate() {
        patcher.replace_at(ndx, c.clone());
    }

    patcher.patch_whole_table_no_write()
}

fn most_used_cases_in_order<B, Case, E>(
    table: &InjectableDispatchTable<B, Counter, Case, E>,
    counters: &[u32]) -> bool
where B: Buffer, Case: DispatchCase<Counter>, E: Clone + Display {
    let h = (table.dt.cases.len() as f64).log(2.0) as usize;

    let mut sorted_counters: Vec<(usize, u32)> = counters.to_owned().into_iter()
        .enumerate()
        .collect();
    sorted_counters.sort_by( |(_, a), (_, b)| b.cmp(a) );
    sorted_counters.truncate(h);

    let mut sorted_cases: Vec<(usize, u64)> = table.dt.cases.iter()
        .map( |case| case.identifier() )
        .enumerate()
        .collect();
    sorted_cases.sort_by( |(_, a), (_, b)| a.cmp(b) );
    //sorted_cases.truncate(h);

     sorted_counters.into_iter().enumerate().all( |(exp, (counter_idx, _))| {
        let (got, _) = sorted_cases[counter_idx];
        exp == got
    })
}


fn should_sort(mut counters: Vec<u32>) -> bool {
    counters.sort_by(|a, b| b.cmp(a) );
    let with_idx: Vec<(u64, u64)> = counters.into_iter()
        .enumerate()
        .map( |(idx, x)| (idx as u64, x as u64) )
        .collect();

    let tree_height = (with_idx.len() as f64).log(2.0) as u64;

    let tree_cost = with_idx.iter().map( |(_, x)| *x ).sum::<u64>() * tree_height;
    let sort_cost = with_idx.iter().map( |(idx, x)| *x * (*idx + 1) ).sum::<u64>();

    sort_cost < tree_cost
}

fn table_counters<B, Case, E>(
    table: &InjectableDispatchTable<B, Counter, Case, E>) -> Result<Vec<u32>>
where B: Buffer, Case: DispatchCase<Counter>, E: Clone + Display {
    let size = table.dt.cases.len();
    let buffer = table.cache.as_ref().unwrap();

    (1 .. size+1).map( |idx| {
        let longword = buffer.read(4, idx * 4)?;
        Ok(u32::from_ne_bytes(longword.try_into().unwrap()))
    }).collect()
}

pub struct CounterValues {
    pub call_counter: u32,
    pub ret_counter: u32,
    pub jump_counters: BTreeMap<Function, u32>
}

/// Retrieve the table counter value of every given tables.
pub fn table_counter_values<B, E>(
    dt: &InjectableDispatchTable<B, Counter, DispatchFunction<Counter>, E>,
    rdt: &InjectableDispatchTable<B, Counter, DispatchRet<Counter>, E>,
    jdts: &FunctionTableMap<B, Counter, E>) -> Result<CounterValues>
where B: Buffer, E: Clone + Display {
    let mut map = BTreeMap::new();

    for (func, table) in jdts.iter() {
        map.insert(func.clone(), get_table_counter_value(table)?);
    }

    Ok(CounterValues {
        call_counter: get_table_counter_value(dt)?,
        ret_counter: get_table_counter_value(rdt)?,
        jump_counters: map
    })
}

fn get_table_counter_value<B, C, E>(dt: &InjectableDispatchTable<B, Counter, C, E>) -> Result<u32>
where B: Buffer, C: DispatchCase<Counter>, E: Clone + Display {
    let input = dt.cache.as_ref().unwrap().read(4, 0)?;
    let array: [u8 ; 4] = input.try_into().unwrap();

    Ok(u32::from_ne_bytes(array))
}

#[cfg(test)]
mod tests {
    use super::*;

    use std::path::Path;

    use nix::sys::ptrace::{Event, Options, setoptions};
    use nix::sys::signal::Signal;
    use nix::sys::wait::WaitStatus;

    use sorry::TargetProcess;
    use sorry::breakpoint::{Breakpoint, Mode};
    use sorry::controller::AllocationStrategy;
    use sorry::error::Error;

    use crate::disass::{Disassembly, EdgeKind, LightDisassembly};
    use crate::reloc::dispatch::{DispatchTable, FunctionDispatchTable};
    use crate::reloc::dispatch::commands;
    use crate::reloc::stext::create_stext_section;

    #[test]
    fn test_count_values() -> Result<()> {
        let mut target = TargetProcess::new(
            "resources/freq",
            vec!["resources/dummy5", "-p", "resources/freq.c"]);
        target.start().ok_or(Error::CustomError("Could not start".to_string()))?;

        target.controller_mut_do( |ctrl| {
            ctrl.allocation_strategy = AllocationStrategy::Mmap;
            Ok(())
        })?;

        let mut ctrl = target.get_controller();

        let load_addr = ctrl.get_absolute_load_address();
        let disass = LightDisassembly::new(Path::new("resources/freq"), load_addr)?;
        disass.generate_cfg(true, None)?;

        let main_offset = target.get_main_function_offset()?;
        let load_addr = target.get_controller().get_absolute_load_address();
        let mut main_breakpoint = Breakpoint::new(load_addr + main_offset, Mode::OneShot);

        main_breakpoint.set(&ctrl)?;
        ctrl.resume(false)?;
        target.wait(None)?;

        main_breakpoint.clean_code(&ctrl)?;
        ctrl.load_map_file()?;

        let error = "int3\nmov 0,%rax\nmov $1,%eax\nmov $42,%ebx\nint $0x80\n".to_string();

        let mut dt: DispatchTable::<Counter, _, _> = DispatchTable::new(error.clone());
        let mut rdt: DispatchTable::<Counter, _, _> = DispatchTable::new(error.clone());

        dt.display_strategy = commands::linear_table_strategy();
        rdt.display_strategy = commands::linear_table_strategy();

        let mut stext = create_stext_section(&target, &disass, dt, rdt, true)?;

        setoptions(target.get_pid(), Options::PTRACE_O_TRACEEXIT)?;

        let blocks = disass.basic_blocks()?;
        disass.populate_dispatch_table(&mut stext.dt.dt, &blocks, EdgeKind::Call)?;


        let mut jdts = stext.relocate(&target, &disass, |addr| {
            format!("popf\nmovl ${:#x}, -0x4(%rsp)\nmovl ${:#x}, -0x8(%rsp)\njmpq *-0x8(%rsp)\n",
            (addr >> 32) as u32,
            addr as u32)
        }, true)?;


        let mut regs = ctrl.get_registers()?;
        regs.rip = stext.relocs.relocated_address(regs.rip).unwrap();
        ctrl.set_registers(regs)?;

        ctrl.resume(false)?;
        let status = ctrl.wait(None)?;

        match status {
            WaitStatus::PtraceEvent(_, Signal::SIGTRAP, event)
                if event == Event::PTRACE_EVENT_EXIT as i32 => {
                    let values = table_counter_values(&stext.dt, &stext.rdt, &jdts)?;

                    assert_eq!(778, values.ret_counter);
                }

            _ => assert!(false)
        }

        Ok(())
    }

    fn that_table<'a>()
    -> Result<InjectableDispatchTable<Vec<u8>, Counter, DispatchFunction<Counter>, &'a str>> {
        let mut dt = FunctionDispatchTable::<Counter, _>::new("ret\n");
        dt.add_function(1, 1001);
        dt.add_function(2, 1002);
        dt.add_function(3, 1003);
        dt.add_function(4, 1004);
        dt.add_function(5, 1005);
        dt.add_function(6, 1006);
        dt.add_function(7, 1007);

        let mut idt = InjectableDispatchTable::<Vec<u8>, _, _, _>::new(dt);
        idt.emit(vec![0x0 ; idt.dt.estimate_size()], 0)?;

        Ok(idt)
    }

    #[test]
    fn correctly_sort_table() -> Result<()> {
        let mut idt = that_table()?;

        let buffer = idt.cache.as_mut().unwrap();
        buffer.write(&(15 as u32).to_ne_bytes(), 0)?;
        buffer.write(&(2 as u32).to_ne_bytes(), 4)?;
        buffer.write(&(4 as u32).to_ne_bytes(), 8)?;
        buffer.write(&(1 as u32).to_ne_bytes(), 12)?;
        buffer.write(&(3 as u32).to_ne_bytes(), 16)?;
        buffer.write(&(0 as u32).to_ne_bytes(), 20)?;
        buffer.write(&(4 as u32).to_ne_bytes(), 24)?;
        buffer.write(&(1 as u32).to_ne_bytes(), 28)?;

        sort_table(&mut idt, vec![2, 4, 1, 3, 0, 4, 1])?;
        let cases: Vec<_> = idt.dt.cases.iter()
            .map ( |case| case.identifier() )
            .collect();

        assert_eq!(vec![2, 6, 4, 1, 3, 7, 5], cases);
        Ok(())
    }

    #[test]
    fn test_should_sort() -> Result<()> {
        let mut idt1 = that_table()?;

        let buffer1 = idt1.cache.as_mut().unwrap();
        buffer1.write(&(15 as u32).to_ne_bytes(), 0)?;
        buffer1.write(&(2 as u32).to_ne_bytes(), 4)?;
        buffer1.write(&(4 as u32).to_ne_bytes(), 8)?;
        buffer1.write(&(1 as u32).to_ne_bytes(), 12)?;
        buffer1.write(&(3 as u32).to_ne_bytes(), 16)?;
        buffer1.write(&(0 as u32).to_ne_bytes(), 20)?;
        buffer1.write(&(4 as u32).to_ne_bytes(), 24)?;
        buffer1.write(&(1 as u32).to_ne_bytes(), 28)?;

        let counters1 = table_counters(&idt1)?;
        assert_eq!(vec![2, 4, 1, 3, 0, 4, 1], counters1);
        assert_eq!(false, should_sort(counters1));

        let mut idt2 = that_table()?;

        let buffer2 = idt2.cache.as_mut().unwrap();
        buffer2.write(&(15 as u32).to_ne_bytes(), 0)?;
        buffer2.write(&(2 as u32).to_ne_bytes(), 4)?;
        buffer2.write(&(4 as u32).to_ne_bytes(), 8)?;
        buffer2.write(&(0 as u32).to_ne_bytes(), 12)?;
        buffer2.write(&(1 as u32).to_ne_bytes(), 16)?;
        buffer2.write(&(0 as u32).to_ne_bytes(), 20)?;
        buffer2.write(&(8 as u32).to_ne_bytes(), 24)?;
        buffer2.write(&(0 as u32).to_ne_bytes(), 28)?;

        let counters2 = table_counters(&idt2)?;
        assert_eq!(vec![2, 4, 0, 1, 0, 8, 0], counters2);
        assert_eq!(true, should_sort(counters2));
        Ok(())
    }

    #[test]
    fn table_in_order() -> Result<()> {
        let mut idt = that_table()?;

        let buffer = idt.cache.as_mut().unwrap();
        buffer.write(&(15 as u32).to_ne_bytes(), 0)?;
        buffer.write(&(2 as u32).to_ne_bytes(), 4)?;
        buffer.write(&(4 as u32).to_ne_bytes(), 8)?;
        buffer.write(&(0 as u32).to_ne_bytes(), 12)?;
        buffer.write(&(1 as u32).to_ne_bytes(), 16)?;
        buffer.write(&(0 as u32).to_ne_bytes(), 20)?;
        buffer.write(&(8 as u32).to_ne_bytes(), 24)?;
        buffer.write(&(0 as u32).to_ne_bytes(), 28)?;

        let counters = table_counters(&idt)?;
        assert_eq!(vec![2, 4, 0, 1, 0, 8, 0], counters);
        assert_eq!(false, most_used_cases_in_order(&idt, &counters));

        let mut patcher = TablePatcher::new(&mut idt);
        patcher.swap_at(0, 2);
        patcher.swap_at(0, 5);
        patcher.patch_whole_table_no_write()?;

        let counters = table_counters(&idt)?;
        let identifiers: Vec<u64> = idt.dt.cases.iter().map( |case| case.identifier() ).collect();

        assert_eq!(vec![6, 2, 1, 4, 5, 3, 7], identifiers);
        assert_eq!(true, most_used_cases_in_order(&idt, &counters));
        Ok(())
    }
}
