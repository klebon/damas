//! The eh_frame module provides functions to retrieve function addresses from the .eh_frame
//! section of an ELF file.

use std::cmp::Ordering;
use std::convert::TryInto;
use std::path::Path;

use elf::File as ElfFile;
use elf::{ParseError, Section};
use elf::types::STT_FUNC;

use gimli::{BaseAddresses, EhFrame, NativeEndian, Reader, ReaderOffset, UnwindSection};
use gimli::read::CieOrFde;
use gimli::read::Error as ReadError;

use sorry::TargetProcess;
use sorry::buffer::Buffer;
use sorry::buffer::remote::RemoteBuffer;

use crate::error::Result as DamasResult;
use crate::reloc::{RelocationManager, SectionRange};

/// Error type for the eh_frame module. This type encapsulates the elf and dwarf parsers errors as
/// well as a section-missing constructor.
#[derive(Debug)]
pub enum Error {
    ElfParseError(ParseError),
    GimliReadError(ReadError),
    SectionNotFound(String)
}

impl From<ParseError> for Error {
    fn from(err: ParseError) -> Self {
        Error::ElfParseError(err)
    }
}

impl From<ReadError> for Error {
    fn from(err: ReadError) -> Self {
        Error::GimliReadError(err)
    }
}

impl PartialEq for Error {
    fn eq(&self, rhs: &Self) -> bool {
        use Error::*;

        match (self, rhs) {
            (ElfParseError(_), ElfParseError(_)) => true,
            (GimliReadError(err1), GimliReadError(err2)) => err1 == err2,
            (SectionNotFound(err1), SectionNotFound(err2)) => err1 == err2,
            _ => false
        }
    }
}

type Result<T> = std::result::Result<T, Error>;

/// Representation of a function as found in the .eh_frame section in the binary.
#[derive(Clone, Debug, Eq)]
pub struct EhFrameFunction {
    /// The start address of the function.
    pub start: u64,
    /// The end address of the function.
    pub end: u64,
    /// The offset of the corresponding FDE.
    pub fde_offset: u64,
    /// The size of the FDE.
    pub fde_length: usize
}

impl PartialEq for EhFrameFunction {
    fn eq(&self, other: &EhFrameFunction) -> bool {
        self.start == other.start
    }
}

impl Ord for EhFrameFunction {
    fn cmp(&self, other: &Self) -> Ordering {
        self.start.cmp(&other.start)
    }
}

impl PartialOrd for EhFrameFunction {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

/// Open the ELF file and continue with the given function, providing it with the computed ELF file
/// object.
fn with_elf<F, R>(path: &Path, f: F) -> Result<R>
where F: Fn(ElfFile) -> Result<R> {
    ElfFile::open_path(path)
        .map_err(Error::from)
        .and_then( |file| f(file) )
}

fn get_elf_section<'a>(elf: &'a ElfFile, s: &str) -> Result<&'a Section> {
    elf.get_section(s).ok_or_else( || Error::SectionNotFound(s.to_string()))
}

/// Create the needed BaseAddresses object using the corresponding ELF file object.
fn base_addresses_from_elf(elf: &ElfFile) -> Result<BaseAddresses> {
    let eh_frame_hdr = get_elf_section(elf, ".eh_frame_hdr")?;
    let eh_frame = get_elf_section(elf, ".eh_frame")?;
    let text = get_elf_section(elf, ".text")?;
    let got = get_elf_section(elf, ".got")?;

    let bases = BaseAddresses::default()
        .set_eh_frame_hdr(eh_frame_hdr.shdr.addr)
        .set_eh_frame(eh_frame.shdr.addr)
        .set_text(text.shdr.addr)
        .set_got(got.shdr.addr);

    Ok(bases)
}

/// Scan the .eh_frame entry. If it is a CIE, nothing is done, if it is a FDE, its initial address
/// and final address (initial address + its length) are printed to stdout.
fn scan_eh_frame_entry<R>(
    eh_addr: u64,
    eh: &EhFrame<R>,
    entry: CieOrFde<'_, EhFrame<R>, R>)
-> Result<Option<EhFrameFunction>> where R: Reader {
    match entry {
        CieOrFde::Cie(_) => Ok(None),
        CieOrFde::Fde(pfde) => {
            // Parse the partial FDE using the lambda expression to get the corresponding CIE.
            let fde = pfde.parse(|_, bases, offset| eh.cie_from_offset(bases, offset) )?;
            let addr = fde.initial_address();
            let length = fde.len();
            let offset = fde.offset().into_u64() + eh_addr;
            let fde_length = fde.entry_len().into_u64() as usize;

            Ok(Some(EhFrameFunction {
                start: addr,
                end: addr + length,
                fde_offset: offset,
                fde_length: fde_length
            }))
        }
    }
}

/// Scan the whole .eh_frame section and iterate over its entries to print the bounds of each FDE.
pub fn scan_eh_frame(path: &Path) -> Result<Vec<EhFrameFunction>> {
    with_elf(path, |elf| {
        get_elf_section(&elf, ".eh_frame").and_then( |eh_frame| {
            let mut res = Vec::new();
            let dwarf_eh_frame = EhFrame::new(&eh_frame.data, NativeEndian);

            let eh_addr = eh_frame.shdr.addr;
            let bases = base_addresses_from_elf(&elf)?;
            let mut entries = dwarf_eh_frame.entries(&bases);

            while let Some(entry) = entries.next()? {
                if let Some(func) = scan_eh_frame_entry(eh_addr, &dwarf_eh_frame, entry)? {
                    res.push(func);
                }
            }

            Ok(res)
        })
    })
}

pub fn translate_eh_frame<'a, S, I>(target: &TargetProcess,
                                    relocs: &RelocationManager<S>,
                                    frames: I) -> DamasResult<()>
where I: Iterator<Item = &'a EhFrameFunction>, S: Clone + SectionRange {
    let ctrl = target.get_controller();

    for fde in frames.filter( |fde| fde.fde_offset != 0 ) {
        let frame_addr = ctrl.get_absolute_load_address() + fde.fde_offset;
        let mut remote = RemoteBuffer::new(ctrl.get_pid(), frame_addr, fde.fde_length);

        let begin_addr = frame_addr + 8;

        let pc_begin_vec = remote.read(4, 8)?;
        let pc_begin = i32::from_ne_bytes(pc_begin_vec.as_slice().try_into().unwrap()) as i64
            + begin_addr as i64;

        // Future purpose
        let pc_range_vec = remote.read(4, 12)?;
        let pc_range = i32::from_ne_bytes(pc_range_vec.as_slice().try_into().unwrap());

        let pc_end = pc_begin + pc_range as i64;

        if let Some(relocated) = relocs.relocated_address(pc_begin as u64) {
            let new_pc_begin = (relocated - begin_addr) as u32;
            remote.write(&new_pc_begin.to_ne_bytes(), 8)?;

            if let Some(relocated_range) = relocs.relocated_address(pc_end as u64) {
                let new_pc_range = relocated_range.wrapping_sub(relocated) as u32;
                remote.write(&new_pc_range.to_ne_bytes(), 12)?;
            }
        }
    }

    Ok(())
}

fn eh_frame_hdr_bounds(target: &TargetProcess) -> Result<Option<(u64, usize)>> {
    let ctrl = target.get_controller();
    let path = ctrl.get_executable_name();
    let load_addr = ctrl.get_absolute_load_address();

    with_elf(Path::new(path), |elf| {
        let bounds = elf.get_section(".eh_frame_hdr")
            .map( |hdr| (hdr.shdr.addr, hdr.shdr.size as usize) )
            .map( |(addr, size)| (load_addr + addr, size) );

        Ok(bounds)
    }).map_err(Error::from)
}

pub fn translate_offset<B, S>(buffer: &mut B, relocs: &RelocationManager<S>, offset: usize)
-> DamasResult<()> where B: Buffer, S: Clone + SectionRange {
    let pc_begin_vec = buffer.read(4, offset)?;
    let pc_begin_slice = pc_begin_vec.as_slice().try_into().unwrap();
    let pc_begin = i32::from_ne_bytes(pc_begin_slice) as i64 + buffer.get_address() as i64;

    if let Some(relocated) = relocs.relocated_address(pc_begin as u64) {
        let new_pc_begin = (relocated - buffer.get_address()) as u32;
        buffer.write(&new_pc_begin.to_ne_bytes(), offset)?;
    }

    Ok(())
}

pub fn translate_eh_frame_hdr<S>(target: &TargetProcess, relocs: &RelocationManager<S>)
-> DamasResult<()> where S: Clone + SectionRange {
    if let Some((addr, size)) = eh_frame_hdr_bounds(target)? {
        let mut remote = RemoteBuffer::new(target.get_pid(), addr, size);

        // The first 12 bytes of the .eh_frame_hdr section are metadata we don't care about here.
        // The rest of the section is made of pairs of (offset to the frame address, some kind of
        // pointer to the right FDE in .eh_frame).
        let mut offset = 12;

        while offset < size - 1 {
            translate_offset(&mut remote, relocs, offset)?;
            offset += 8;
        }
    }

    Ok(())
}

pub fn function_symbols(target: &TargetProcess) -> Vec<EhFrameFunction> {
    let elf = target.get_target_metadata();
    let load_addr = target.get_controller().get_absolute_load_address();

    [".plt", ".symtab"].iter().flat_map( |section_name| {
        elf.get_section(section_name)
            .map( |text| -> Vec<_> {
                elf.get_symbols(text)
                    .unwrap_or_default()
                    .into_iter()
                    .filter( |sym| sym.symtype == STT_FUNC )
                    .map( |sym| EhFrameFunction {
                        start: load_addr + sym.value,
                        end: load_addr + sym.value + sym.size,
                        fde_offset: 0,
                        fde_length: 0
                    }).collect()
            }).unwrap_or_default()
    }).collect()
}
