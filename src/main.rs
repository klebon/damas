#![feature(drain_filter)]
#![feature(btree_drain_filter)]

extern crate capstone;
extern crate clap;
extern crate crossbeam;
extern crate elf;
extern crate gimli;
extern crate perf_event_open_sys;
#[macro_use]
extern crate pyo3;
extern crate serde;
extern crate serde_lexpr;
extern crate sorry;

pub mod asm;
pub mod disass;
pub mod eh_frame;
pub mod error;
pub mod insn;
pub mod log;
pub mod logging;
pub mod monitor;
pub mod reloc;
pub mod wrapper;

use std::collections::HashSet;
use std::convert::TryFrom;
use std::fmt::Display;
use std::fs::{File, read_to_string};
use std::io::Write;
use std::io::prelude::BufRead;
use std::path::{Path, PathBuf};
use std::str::FromStr;
use std::sync::{Arc, Condvar, Mutex};

use clap::{Arg, ArgGroup, ArgMatches, App};

use crossbeam::thread;

use elf::types::Symbol;

use nix::sys::ptrace::{detach, Event, getevent, Options, setoptions};
use nix::sys::signal::{kill, Signal};
use nix::sys::wait::{WaitPidFlag, WaitStatus};

use serde_lexpr::{from_str, to_string};

use sorry::TargetProcess;
use sorry::assembly::create_capstone_object;
use sorry::breakpoint::{Breakpoint, Mode};
use sorry::buffer::Buffer;
use sorry::buffer::remote::RemoteBuffer;
use sorry::controller::AllocationStrategy;
use sorry::error::Error;

use disass::Disassembly;
use eh_frame::{
    EhFrameFunction, function_symbols, scan_eh_frame, translate_eh_frame, translate_eh_frame_hdr
};
use error::{DamasError, Result};
use disass::{EdgeKind, LightDisassembly};
//use disass::light_disass::get_exn_str;
use insn::{CALL_GROUP, Insn};
use log::{BenchedFunction, BenchmarkManager, Logger};
use logging::{dump_tables, print_all_table_counters, print_every_tables};
use monitor::counter::{MonitorContext, MonitorForFeature, Need};
use reloc::dispatch::{DamasSerializer, DispatchTable};
use reloc::dispatch::commands;
use reloc::feature::{Counter, Feature};
use reloc::stext::{create_stext_section, FunctionTableMap, STextSection};
use reloc::SectionRange;
use wrapper::write_wrapper;

/// Specification of the commandline of Damas.
fn commandline_arguments<'a, 'b>() -> App<'a, 'b> {
    App::new("Damas")
        .version("0.1")
        .author("Camille Le Bon <camille.le-bon@inria.fr>")
        .about("Dynamic CFI solution using execution traces to improve protection performance")
        .arg(Arg::with_name("pid")
             .short("p")
             .long("pid")
             .help("PID of the process to protect")
             .takes_value(true))
        .arg(Arg::with_name("file")
             .short("f")
             .long("file")
             .help("ELF executable file to run protected")
             .takes_value(true))
        .arg(Arg::with_name("args")
             .long("args")
             .multiple(true)
             .use_delimiter(false)
             .takes_value(true)
             .allow_hyphen_values(true)
             .requires("file"))
        .arg(Arg::with_name("verbose")
             .short("v")
             .long("verbose")
             .help("Enable logging of information"))
        .arg(Arg::with_name("bench")
             .long("bench")
             .help("Enable the benchmarking of the process execution time"))
        .arg(Arg::with_name("bench function")
            .long("bench-func")
            .help("Specify a function to benchmark")
            .takes_value(true)
            .requires("bench"))
        .arg(Arg::with_name("loop")
             .long("loop")
             .help("Does not restart the process but detach from it and loops infinitely"))
        .arg(Arg::with_name("log-relocations")
            .long("log-relocations")
            .help("Show the original and target addresses and sizes of each relocated basic block")
            .takes_value(false))
        .arg(Arg::with_name("show-tables")
            .long("show-tables")
            .help("Show the each table address and associated function name"))
        .arg(Arg::with_name("dump-tables")
            .long("dump-tables")
            .help("Dump the code of each tables")
            .takes_value(true))
        .arg(Arg::with_name("table-counters")
            .long("table-counters")
            .help("Make the dispatch tables count each time their cases are matched")
            .conflicts_with("loop"))
        .arg(Arg::with_name("table-counter-log")
            .long("table-counter-log")
            .help("Specify a file to store the table-counter results to")
            .takes_value(true)
            .requires("table-counters"))
        .arg(Arg::with_name("serialize-tables")
            .short("s")
            .long("serialize-tables")
            .help("Serialize the table layouts in a file such that the most used cases come first")
            .takes_value(true)
            .requires("table-counters"))
        .arg(Arg::with_name("deserialize-tables")
            .short("d")
            .long("deserialize-tables")
            .help("Deserialize the table layouts from a file")
            .takes_value(true)
            .requires("table-counters")
            .conflicts_with("serialize-tables"))
        .arg(Arg::with_name("lazy")
            .long("lazy")
            .help("Do not inject tables for function without any indirect jump (except ret)"))
        .arg(Arg::with_name("linear-tables")
            .long("linear")
            .help("Inject linear tables instead of the default tree representation of tables"))
        .arg(Arg::with_name("monitor")
            .short("m")
            .long("monitor")
            .conflicts_with("linear")
            .help("Activate the runtime monitoring"))
        .arg(Arg::with_name("period")
            .short("t")
            .long("period")
            .requires("monitor")
            .help("Set the period of the runtime monitoring in milliseconds (default: 1000 ms)")
            .takes_value(true))
        .group(ArgGroup::with_name("program")
               .required(true)
               .args(&["pid", "file"]))
}

/// Extract the target program arguments from Damas' command-line.
fn extract_target_arguments<S: AsRef<str>>(prgm: &S, matches: &ArgMatches) -> Vec<String> {
    let mut res = vec![prgm.as_ref().to_string()];

    if let Some(args) = matches.values_of_lossy("args") {
        for arg in args {
            res.push(arg);
        }
    }

    res
}

/// Create a TargetProcess object using the commandline arguments.
fn create_target_process(matches: &ArgMatches) -> Result<TargetProcess> {
    match matches.value_of("pid") {
        Some(pid_str) => {
            let err_msg = format!("Could not parse PID '{}'", pid_str);
            let pid = FromStr::from_str(pid_str).or(Err(Error::CustomError(err_msg)))?;
            TargetProcess::from_pid(pid).map_err(DamasError::from)
        }

        None => {
            let filename = matches.value_of("file").unwrap().to_string();
            let args = extract_target_arguments(&filename, matches);

            let pathbuf = PathBuf::from(filename).canonicalize()?;
            let path = pathbuf.to_string_lossy().into_owned();

            // Create the TargetProcess object and start it.
            let target = TargetProcess::new(path, args);
            target.start().ok_or_else( || Error::CustomError("Could not start".to_string()))?;
            Ok(target)
        }
    }
}

fn infinite_loop<B, F1, F2, E, R>(
    target: &TargetProcess,
    stext: &STextSection<B, F1, F2, E, R>) -> Result<()>
where B: Buffer, F1: Feature, F2: Feature, E: Clone + Display, R: Clone + SectionRange {
    detach(target.get_pid(), Signal::SIGSTOP) .map_err(Error::NixError)?;

    let stdin = std::io::stdin();
    for res in stdin.lock().lines() {
        let _ = res.ok().and_then( |line| u64::from_str_radix(&line, 16).ok() )
            .map( |addr| {
                let _ = stext.relocs.original_address(addr)
                    .or_else( || stext.relocs.relocated_address(addr) )
                    .map( |other| {
                        let a = std::cmp::min(addr, other);
                        let b = std::cmp::max(addr, other);
                        println!("{:#x} => {:#x}", a, b)
                    });
            });
    }

    Ok(())
}

/// Create a LightDisassembly from the given target process.
fn light_disass(logger: &mut Logger, target: &TargetProcess)
-> Result<(Vec<EhFrameFunction>, LightDisassembly)> {
    let executable = Path::new(target.get_executable_name());
    let load_addr = target.get_controller().get_absolute_load_address();

    writeln!(logger, "Process loaded at 0x{:x}", load_addr)?;
    let eh_funcs = scan_eh_frame(Path::new(executable))?;
    let mut funcs = Vec::with_capacity(eh_funcs.len());

    for func in eh_funcs {
        let f = EhFrameFunction {
            start: load_addr + func.start,
            end: load_addr + func.end,
            fde_offset: func.fde_offset,
            fde_length: func.fde_length
        };

        funcs.push(f);
    }

    funcs.extend(function_symbols(target).into_iter());
    funcs.sort();
    funcs.dedup();

    let cfg = LightDisassembly::new(executable, load_addr)?;
    cfg.generate_cfg(true, Some(funcs.clone()))?;

    writeln!(logger, "Disassembly successfully built")?;
    Ok((funcs, cfg))
}

/// Set a breakpoint at the main function's beginning and reach it.
fn reach_main_breakpoint(target: &TargetProcess) -> Result<WaitStatus> {
    let ctrl = target.get_controller();

    let main_offset = target.get_main_function_offset()?;
    let load_addr = target.get_controller().get_absolute_load_address();
    let mut main_breakpoint = Breakpoint::new(load_addr + main_offset, Mode::OneShot);

    main_breakpoint.set(&ctrl)?;
    ctrl.resume(false)?;
    let brk_status = target.wait(None)?;

    main_breakpoint.clean_code(&ctrl)?;
    ctrl.load_map_file()?;

    Ok(brk_status)
}

fn find_function_symbol(target: &TargetProcess, name: &str) -> Option<Symbol> {
    target.get_all_symbols().into_iter().find( |sym| sym.name == name )
}

/// Given a function symbol, wrap every calls to the function behind a wrapper that measures its
/// execution time. This function retrieves the function corresponding to the given symbol, creates
/// a buffer for the wrapper and tells the stext section to take it into account when relocating
/// code.
fn install_bench_wrapper<'a, B, F1, F2, E, R>(
    logger: &mut Logger,
    target: &TargetProcess,
    stext: &mut STextSection<B, F1, F2, E, R>,
    matches: &ArgMatches<'a>) -> Result<()>
where B: Buffer,
      F1: Feature, F2: Feature,
      E: Clone + Display, R: Clone + SectionRange {
    if let Some(name) = matches.value_of("bench function") {
        find_function_symbol(target, name)
            .ok_or_else( || DamasError::CustomError(format!("Could not find the symbol {}", name)))
            .and_then( |symbol| {
                let origin = target.get_controller().get_absolute_load_address() + symbol.value;

                let mut buf = stext.cache.make_another();
                buf.allocate(300)?;

                let (offset, wrapper) = write_wrapper(target, buf.get_address())?;
                buf.write(&wrapper, 0)?;

                let bf = BenchedFunction::<B> {
                    origin: origin,
                    target: 0, // Will be overriden by stext when needed.
                    prologue: 0x5c,
                    offset_to_call: offset,
                    wrapper: buf
                };

                writeln!(logger, "Wrapper at: {:#x}", bf.wrapper.get_address())?;

                stext.set_benched_function(bf);
                Ok(())
            })
    } else {
        Ok(())
    }
}

#[derive(Default)]
pub struct ForeignCallHandler {
    memo: HashSet<u64>,
    ret_memo: HashSet<u64>
}

impl ForeignCallHandler {
    #[inline(always)]
    pub fn new() -> Self {
        ForeignCallHandler::default()
    }

    pub fn handle_call(&mut self, logger: &mut Logger, target: &TargetProcess) -> Result<()> {
        let ctrl = target.get_controller();
        let mut regs = ctrl.get_registers()?;
        let rax = regs.rax;

        match self.memo.get(&rax) {
            Some(_) => {
                regs.rip = rax;
                ctrl.set_registers(regs)?;
            }

            None => {
                match ForeignCallHandler::get_symbol_at(target, rax)? {
                    None => writeln!(logger, "Could not find any symbol at {:#x}", rax)?,
                    Some(sym) => {
                        writeln!(logger, "Found symbol {} at {:#x}", sym.name, rax)?;

                        self.memo.insert(rax);
                        regs.rip = rax;
                        ctrl.set_registers(regs)?;
                    }
                }
            }
        }

        Ok(())
    }

    pub fn handle_ret(&mut self, logger: &mut Logger, target: &TargetProcess) -> Result<()> {
        let ctrl = target.get_controller();
        let mut regs = ctrl.get_registers()?;

        match self.ret_memo.get(&regs.r11) {
            Some(_) => {
                regs.rip = regs.r11;
                ctrl.set_registers(regs)?;
            },

            None => {
                let buf_start = if regs.r11 >= 30 { regs.r11 - 30} else { 0 };
                let buf_size = (regs.r11 - buf_start) as usize;

                let remote = RemoteBuffer::new(target.get_pid(), buf_start, buf_size);
                let buf = remote.read(buf_size, 0)?;

                let disasm = create_capstone_object();
                let insns = disasm.disasm_all(&buf, buf_start)?;
                let last = insns.iter().last().unwrap();

                let insn = Insn::try_from(last).unwrap();

                if insn.is_a(CALL_GROUP) {
                    regs.rip = regs.r11;
                    ctrl.set_registers(regs)?;

                    self.ret_memo.insert(regs.r11);
                } else {
                    writeln!(logger, "Instruction is not a call: {:#x}: {} {}",
                        insn.address, insn.mnemonic, insn.op_str)?;
                }
            }
        }

        Ok(())
    }

    fn get_symbol_at(target: &TargetProcess, rax: u64) -> Result<Option<Symbol>> {
        let ctrl = target.get_controller();
        ctrl.load_map_file()?;

        // Get the memory map entry that correspond to the given jump target address.
        Ok(ctrl.find_memory_map_entry(|entry|
            entry.start_addr <= rax
            && rax < entry.end_addr
            && entry.permissions.is_executable() )
            // Get the symbol corresponding to said address if any.
            .and_then( |entry| {
                let elf = entry.elf.as_ref()?;
                let text = elf.get_section(".text")?;

                // Get the program header that correspond to the .text section of the program.
                let text_addr = text.shdr.addr;
                let phdr = elf.phdrs.iter()
                    .find( |phdr| phdr.vaddr <= text_addr && text_addr < phdr.vaddr + phdr.memsz )?;

                // Find the symbol.
                entry.get_all_symbols()
                    .into_iter()
                    .find( |sym| entry.start_addr - phdr.vaddr + sym.value == rax )
            }))
    }
}

fn is_fatal_signal(signal: Signal) -> bool {
    use Signal::*;

    [SIGABRT, SIGHUP, SIGILL, SIGINT, SIGSEGV].contains(&signal)
}

fn relocate<B, C, F1, F2, R>(
    logger: &mut Logger,
    target: &TargetProcess,
    stext: &mut STextSection<B, F1, F2, String, R>,
    matches: &ArgMatches,
    cfg: &C) -> Result<FunctionTableMap<B, F1, String>>
where B: Buffer,
      C: Disassembly,
      F1: Feature,
      F2: Feature,
      R: Clone + SectionRange {
    if matches.is_present("table-counters") {
        setoptions(target.get_pid(), Options::PTRACE_O_TRACEEXIT)?;
    }

    let blocks = cfg.basic_blocks()?;
    cfg.populate_dispatch_table(&mut stext.dt.dt, &blocks, EdgeKind::Call)?;

    let indjump_count = cfg.basic_blocks()?
        .into_iter()
        .filter( |bb| bb.terminator.is_indirect_jump() )
        .count();

      writeln!(logger, "Total count of indirect jumps in the main binary: {}", indjump_count)?;

      install_bench_wrapper(logger, target, stext, matches)?;

      writeln!(logger, "Relocating...")?;
      stext.relocate(target, cfg, |addr| {
          format!("popf\nmovl ${:#x}, -0x4(%rsp)\nmovl ${:#x}, -0x8(%rsp)\njmpq *-0x8(%rsp)\n",
          (addr >> 32) as u32,
          addr as u32)
              //error.clone()
      }, matches.is_present("linear-tables"))
}

/// Execute the main algorithm of the program and returns early with Err if needed.
fn actual_main() -> Result<()> {
    let matches = commandline_arguments().get_matches();
    let mut logger = Logger::new(matches.is_present("verbose"));
    let bench = BenchmarkManager::new();

    let mut target = create_target_process(&matches)?;
    target.controller_mut_do( |ctrl| {
        ctrl.allocation_strategy = AllocationStrategy::Mmap;
        Ok(())
    })?;

    writeln!(logger, "Target PID: {}", target.get_pid())?;

    let (eh_funcs, disass) = light_disass(&mut logger, &target)?;
    //print_eh_frame_functions(&mut logger, eh_funcs.iter())?;

    if matches.value_of("pid") == None {
        let status = reach_main_breakpoint(&target)?;
        assert_eq!(status, WaitStatus::Stopped(target.get_pid(), Signal::SIGTRAP));
    }

    let error = "int3\nmov 0,%rax\nmov $1,%eax\nmov $42,%ebx\nint $0x80\n".to_string();

    let lazy = matches.is_present("lazy");

    if matches.is_present("table-counters") {
        let mut dt: DispatchTable::<Counter, _, _> = DispatchTable::new(error.clone());
        let mut rdt: DispatchTable::<Counter, _, _> = DispatchTable::new(error);

        if matches.is_present("linear-tables") {
            dt.display_strategy = commands::linear_table_strategy();
            rdt.display_strategy = commands::linear_table_strategy();
        }

        let mut stext = create_stext_section(&target, &disass, dt, rdt, lazy)?;

        writeln!(
            logger,
            "Section .secure_text (with counters) located at: {:#x}",
            stext.cache.get_address())?;

        let mut jdts = relocate(&mut logger, &target, &mut stext, &matches, &disass)?;
        if let Some(filename) = matches.value_of("deserialize-tables") {
            let contents = read_to_string(filename)?;

            match from_str::<DamasSerializer>(contents.as_ref()) {
                Err(err) => writeln!(logger, "Error loading table configuration: {}", err)?,
                Ok(mut serializer) => serializer.patch_tables(&target, &mut stext, &mut jdts)?
            }
        }

        // The option here depends only on the presence of the --loop option. Since we know that
        // --table-counters has been provided, the --loop option was not. It can be unwrapped
        // safely.
        let stext_mutex = Arc::new(Mutex::new(stext));
        prepare_and_restart(
            &mut logger, bench, &target, &matches, eh_funcs, stext_mutex.clone(), &mut jdts)?;

        match Arc::try_unwrap(stext_mutex) {
            Err(_) => {},
            Ok(stext_mutex) => {
                let stext = stext_mutex.into_inner().unwrap();

                if let Some(filename) = matches.value_of("table-counter-log") {
                    let mut file = File::create(filename)?;
                    print_all_table_counters(&mut file, &target, &stext, &jdts)?;
                } else {
                    print_all_table_counters(&mut logger, &target, &stext, &jdts)?;
                }

                // If the user asked for the tables to be serialized, we retrieve the associated
                // filename and write the information in the file.
                if let Some(filename) = matches.value_of("serialize-tables") {
                    let mut file = File::create(filename)?;
                    let serializer = DamasSerializer::new(&target, &stext, &jdts)?;

                    writeln!(logger, "Serializing tables into {}", filename)?;

                    let sexpr = to_string(&serializer).unwrap();
                    file.write_all(sexpr.as_ref())?;
                }
            }
        }
    } else {
        let mut dt: DispatchTable::<(), _, _> = DispatchTable::new(error.clone());
        let mut rdt: DispatchTable::<(), _, _> = DispatchTable::new(error);

        if matches.is_present("linear-tables") {
            dt.display_strategy = commands::linear_table_strategy();
            rdt.display_strategy = commands::linear_table_strategy();
        }

        let mut stext = create_stext_section(&target, &disass, dt, rdt, lazy)?;

        writeln!(logger, "Section .secure_text located at: {:#x}", stext.cache.get_address())?;
        let mut jdts = relocate(&mut logger, &target, &mut stext, &matches, &disass)?;

        let stext_mutex = Arc::new(Mutex::new(stext));
        prepare_and_restart(
            &mut logger, bench, &target, &matches, eh_funcs, stext_mutex, &mut jdts)?;
    }

    Ok(())
}

fn prepare_and_restart<B, F1, R>(
    logger: &mut Logger,
    mut bench: BenchmarkManager,
    target: &TargetProcess,
    matches: &ArgMatches,
    eh_funcs: Vec<EhFrameFunction>,
    stext_mutex: Arc<Mutex<STextSection<B, F1, F1, String, R>>>,
    jdts: &mut FunctionTableMap<B, F1, String>) -> Result<()>
where B: Buffer + Send + Sync,
      F1: Feature + MonitorForFeature + Send + Sync,
      R: Clone + SectionRange + Send + Sync {
    writeln!(logger, "Relocated!")?;

    let stext = stext_mutex.lock().unwrap();

    if logger.verbose && matches.is_present("show-tables") {
        print_every_tables(logger, target, &stext, jdts)?;
    }

    match matches.value_of("dump-tables") {
        Some(filename) => {
            let dump_file = File::create(filename)?;
            dump_tables(dump_file, target, &stext, &jdts)?;
        }

        None => {}
    }

    translate_eh_frame(target, &stext.relocs, eh_funcs.iter())?;
    translate_eh_frame_hdr(target, &stext.relocs)?;

    let dt_addr = stext.dt.address.unwrap();
    writeln!(logger, "Main dispatch table at: {:#x}", dt_addr)?;

    let ctrl = target.get_controller();
    let mut regs = ctrl.get_registers()?;
    writeln!(logger, "RIP when the process stopped: {:#x}", regs.rip)?;

    // If we attached to a running process we need its program counter to be inside the main binary
    // for relocation.
    if matches.is_present("pid") {
        ctrl.load_map_file()?;
        let load_address = ctrl.get_absolute_load_address();
        let text = ctrl.find_memory_map_entry( |entry|
            entry.end_addr > load_address &&
            entry.permissions.is_executable() ).unwrap();

        while !(text.start_addr <= regs.rip && regs.rip < text.end_addr) {
            ctrl.resume(false)?;
            kill(ctrl.get_pid(), Some(Signal::SIGTRAP)).map_err(DamasError::from)?;
            ctrl.wait(None)?;

            regs = ctrl.get_registers()?;
        }
    }

    regs.rip = stext.relocs.relocated_address(regs.rip).unwrap();
    ctrl.set_registers(regs)?;

    if matches.is_present("log-relocations") {
        for reloc in stext.relocs.iter() {
            writeln!(logger, "[RELOC] {:x} {:x} {:x} {:x}",
                reloc.origin,
                reloc.size,
                reloc.target,
                reloc.new_size)?;
        }
    }

    writeln!(logger, "RIP before the process restarts: {:#x}", regs.rip)?;

    if matches.is_present("loop") {
        writeln!(logger, "PID: {}", target.get_pid())?;
        infinite_loop(target, &stext)?;
        return Ok(());
    }

    bench.warm_up_done();

    drop(stext);
    event_loop(logger, target, matches, stext_mutex, jdts, bench)?;
    Ok(())
}

/// Main loop of Damas. When everything has been setup and the code is relocated properly, the
/// target process has been restarted. The main loop waits for the target process and checks
/// whether the process exited or if some special treatment is needed.
fn event_loop<B, F1, R>(
    logger: &mut Logger,
    target: &TargetProcess,
    matches: &ArgMatches,
    stext: Arc<Mutex<STextSection<B, F1, F1, String, R>>>,
    jdts: &mut FunctionTableMap<B, F1, String>,
    mut bench: BenchmarkManager) -> Result<()>
where B: Buffer + Send + Sync,
      F1: Feature + MonitorForFeature + Send + Sync,
      R: Clone + SectionRange + Send + Sync {
    let ctrl = target.get_controller();
    let mut call_handler = ForeignCallHandler::new();

    let stop_monitor = Arc::new(Mutex::new(true));
    let currently_emitting = Arc::new(Mutex::new(false));
    let mut logger_clone = (*logger).clone();

    let need_registers = Arc::new((Mutex::new(
                (Need::Nothing, ctrl.get_registers()?)),
                Condvar::new()));

    thread::scope(|s| {
        // If the runtime monitoring is activated, thr_handle is initialized with a thread handle,
        // otherwise it is None.
        let thr_handle = match matches.is_present("monitor") {
            false => None,
            true => Some(s.spawn( |_| {
                let ctx = MonitorContext::new(
                    matches,
                    ctrl.clone(),
                    stop_monitor.clone(),
                    currently_emitting.clone(),
                    need_registers.clone()
                );

                MonitorForFeature::monitor(&mut logger_clone, ctx, stext.clone(), jdts)
            }))
        };

        let mut still_alive = false;

        loop {
            if thr_handle.is_some() {
                if let Ok(mut lock) = need_registers.0.try_lock() {
                    match lock.0 {
                        Need::Nothing => {}
                        Need::RegRead => {
                            lock.1 = ctrl.get_registers()?;
                            need_registers.1.notify_one();
                        }
                        Need::RegWrite => {
                            ctrl.set_registers(lock.1)?;
                            need_registers.1.notify_one();
                        }
                    }
                }
            }

            if let Ok(mut lock) = currently_emitting.try_lock() {
                if !still_alive || *lock { ctrl.resume(false)?; }
                let status = ctrl.wait(Some(WaitPidFlag::WNOHANG))?;
                *lock = false;

                still_alive = false;

                match status {
                    WaitStatus::StillAlive => { still_alive = true; },

                    WaitStatus::Exited(_, _) => {
                        if matches.is_present("bench") {
                            bench.finish();
                            writeln!(logger, "\n{}", bench)?;
                        }

                        writeln!(logger, "Target process exited with status: {:?}", status)?;
                        break;
                    }

                    WaitStatus::Stopped(_, Signal::SIGTRAP) => {
                        let regs = ctrl.get_registers()?;
                        let rdt = &stext.lock().unwrap().rdt;

                        println!("SIGTRAP at {:#x}!", regs.rip);

                        let address = rdt.address.unwrap();
                        let end_address = address + rdt.dt.estimate_size() as u64;

                        if address <= regs.rip && regs.rip < end_address {
                            call_handler.handle_ret(logger, target)?;
                        } else {
                            call_handler.handle_call(logger, target)?;
                        }
                    }

                    WaitStatus::PtraceEvent(_, Signal::SIGTRAP, event)
                        if event == Event::PTRACE_EVENT_EXIT as i32 => {
                            let code = getevent(target.get_pid())?;

                            bench.finish();
                            if matches.is_present("bench") {
                                writeln!(logger, "\n{}", bench)?;
                            }

                            writeln!(logger, "Target process exited with code: {:?}", code)?;
                            break;
                        }

                    WaitStatus::Stopped(_, signal) if is_fatal_signal(signal) => {
                        bench.finish();
                        writeln!(logger, "Target process exited with status: {:?}", status)?;

                        let regs = ctrl.get_registers()?;
                        writeln!(logger, "Registers:")?;
                        writeln!(logger, "RAX: {:#16x}\t\tRIP: {:#16x}", regs.rax, regs.rip)?;
                        writeln!(logger, "RBX: {:#16x}\t\tRBP: {:#16x}", regs.rbx, regs.rbp)?;
                        writeln!(logger, "RCX: {:#16x}\t\tRSP: {:#16x}", regs.rcx, regs.rsp)?;
                        writeln!(logger, "RDX: {:#16x}\t\tRDI: {:#16x}", regs.rdx, regs.rdi)?;
                        writeln!(logger, "R8: {:#16x} \t\tRSI: {:#16x}", regs.r8, regs.rsi)?;

                        break;
                    }

                    _ => {}
                }
            }
        }

        thr_handle.map( |handle| {
            *stop_monitor.lock().unwrap() = false;
            writeln!(logger, "\nAttempt to join the monitor thread")?;
            handle.join().unwrap()
        }).unwrap_or(Ok(()))
    }).unwrap()
}

fn main() -> Result<()> {
    //match actual_main() {
        //Ok(()) => (),
        //Err(DamasError::PyError(py_err)) => {
            //let err = get_exn_str(py_err);
            //panic!("Python error: {:?}", err);
        //}
        //Err(err) => panic!("{:?}", err),
    //}

    actual_main()
}

#[cfg(test)]
mod tests {
    use nix::sys::wait::WaitStatus;

    use sorry::assembly::create_capstone_object;
    use sorry::breakpoint::*;
    use sorry::buffer::Buffer;
    use sorry::buffer::remote::RemoteBuffer;

    use super::*;
    use crate::reloc::stext::{create_stext_section, padding};

    #[test]
    fn load_from_pid() {
        let matches = commandline_arguments().get_matches_from(&["damas", "--pid", "1024"]);
        assert_eq!(matches.value_of("pid"), Some("1024"));
    }

    #[test]
    fn load_from_file() {
        let matches = commandline_arguments().get_matches_from(&["damas", "-f", "/bin/true"]);
        assert_eq!(matches.value_of("file"), Some("/bin/true"));
    }

    #[test]
    fn cannot_load_from_both_pid_and_file() {
        let res = commandline_arguments()
            .get_matches_from_safe(&["damas", "-f", "/bin/true", "-p", "1024"]);
        assert!(res.is_err());
    }

    fn demo_main_function((bin, args): &(&str, Vec<&str>)) -> Result<()> {
        let mut target = TargetProcess::new(*bin, args.clone());
        target.start().ok_or(Error::CustomError("Could not start".to_string()))?;

        target.controller_mut_do( |ctrl| {
            ctrl.allocation_strategy = AllocationStrategy::Mmap;
            Ok(())
        })?;

        println!("========================================");
        println!("=    Beginning of process execution    =");
        println!("========================================");

        let ctrl = target.get_controller();
        let load_addr = ctrl.get_absolute_load_address();
        println!("Load address: 0x{:x}", load_addr);

        let _ = target.get_target_metadata().get_section(".plt").map( |plt| {
            let plt_addr = load_addr + plt.shdr.addr;
            println!("PLT section loaded at 0x{:x}", plt_addr);
            println!("puts entry should be at 0x{:x}", plt_addr + 0x10);
        }).or_else( || {
            println!("No PLT section found");
            None
        });

        let (_eh_frame, disass) = light_disass(&mut Logger::new(true), &target)?;
        //let cfg = BapDisassembly::new(target.get_executable_name(), load_addr);
        println!("Disassembly created");

        let main_offset = target.get_main_function_offset()?;
        let mut main_breakpoint = Breakpoint::new(load_addr + main_offset, Mode::OneShot);
        main_breakpoint.set(&ctrl)?;
        ctrl.resume(false)?;
        let brk_status = target.wait(None)?;
        main_breakpoint.clean_code(&ctrl)?;
        ctrl.load_map_file()?;

        let mut regs = ctrl.get_registers()?;
        println!("After breakpoint, RIP = {:x}", regs.rip);

        assert_eq!(load_addr + main_offset, regs.rip);

        if let WaitStatus::Exited(_, _) = brk_status {
            println!("Process exited early: {:?}", brk_status);
        }

        let error = "mov $1,%eax\nmov $42,%ebx\nint $0x80\n".to_string();

        let dt: DispatchTable<(), _, _> = DispatchTable::new(error.clone());
        let rdt: DispatchTable<(), _, _> = DispatchTable::new(error.clone());
        let mut stext = create_stext_section(&target, &disass, dt, rdt, true)?;
        let blocks = disass.basic_blocks()?;
        disass.populate_dispatch_table(&mut stext.dt.dt, &blocks, EdgeKind::Call)?;
        stext.relocate(&target, &disass, |_addr| {
            //format!("callq *_main_table\n_main_table:\n  .quad {:#x}\n", addr)
            error.clone()
        }, false)?;

        regs = ctrl.get_registers()?;
        println!("Before relocation, RIP = {:x}", regs.rip);
        regs.rip = stext.relocs.relocated_address(regs.rip).unwrap();

        ctrl.set_registers(regs)?;

        {
            {
                println!("\nMain Dispatch Table:");

                let cs = create_capstone_object();
                let buffer = stext.dt.cache.as_ref().unwrap();
                let contents = buffer.read(buffer.get_size(), 0)?;
                let insns = cs.disasm_all(&contents, buffer.get_address())?;

                println!("{}", insns);
            }

            let mut last = 0;
            for reloc in stext.relocs.iter() {
                if (reloc.target >> 8) != last {
                    println!("--------------------------------------------");
                }

                println!("0x{:x} => 0x{:x} ({} bytes)", reloc.origin, reloc.target, reloc.size);
                last = reloc.target >> 8;
            }
        }

        println!("Before resuming the process, RIP = {:x}", regs.rip);
        println!("{} PID: {}", target.get_executable_name(), target.get_pid());

        if cfg!(feature = "infinite_loop") {
            return infinite_loop(&target, &stext);
        }

        ctrl.resume(false)?;

        let status = ctrl.wait(None)?;

        if status != WaitStatus::Exited(target.get_pid(), 0) {
            println!("Status: {:?}", status);

            regs = ctrl.get_registers()?;
            println!("After resuming the process, RIP = {:x}", regs.rip);

            let cs = create_capstone_object();

            stext.relocs.original_address(regs.rip).and_then( |origin| {
                disass.basic_blocks().ok()?.into_iter()
                    .find( |bb| bb.address <= origin && origin < bb.address + bb.size )
            })
            .map( |bb| {
                let reloc = stext.relocs.relocated_address(bb.address).unwrap();
                let remote = RemoteBuffer::new(target.get_pid(), reloc, bb.size as usize );

                let padding = padding(&bb.terminator);
                let contents = remote.read(bb.size as usize + padding, 0).unwrap();
                let insns = cs.disasm_all(&contents, reloc).unwrap();
                println!("{}", insns);
            });
        }

        assert_eq!(status, WaitStatus::Exited(target.get_pid(), 0));
        println!("========================================");
        println!("=      End of process execution        =");
        println!("========================================");
        Ok(())
    }

    #[test]
    fn demo1() {
        let bins = vec![
            ("resources/dummy", vec!["resources/dummy"]),
            ("resources/dummy2", vec!["resources/dummy2"]),
            ("resources/dummy3", vec!["resources/dummy3"]),
            ("resources/dummy3", vec!["resources/dummy3", "a", "b"]),
            ("resources/dummy4", vec!["resources/dummy4"]),
            ("resources/dummy4", vec!["resources/dummy4", "a", "b"]),
            ("resources/dummy5", vec!["resources/dummy5", "a", "b"]),
            ("resources/dummy_cpp", vec!["resources/dummy_cpp"]),
            // Angr-related problems
            ("resources/inheritance", vec!["resources/inheritance"]),
            ("resources/inheritance", vec!["resources/inheritance", "a", "b"]),
        ];

        for bin in bins {
            if let Err(e) = demo_main_function(&bin) {
                panic!("For binary {}:\n{:?}", bin.0, e);
            }
        }
    }

    #[test]
    fn demo2() {
        let bins = vec![
            ("resources/quicksort", vec!["resources/quicksort"]),
            ("resources/freq", vec!["resources/freq", "-p", "src/main.rs"]),
        ];

        for bin in bins {
            if let Err(e) = demo_main_function(&bin) {
                panic!("For binary {}:\n{:?}", bin.0, e);
            }
        }
    }

    #[test]
    fn demo3() {
        let bins = vec![
            ("resources/thread", vec!["resources/thread"]),
        ];

        for bin in bins {
            if let Err(e) = demo_main_function(&bin) {
                panic!("For binary {}:\n{:?}", bin.0, e);
            }
        }
    }
}
