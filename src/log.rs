use std::fmt::{Display, Formatter};
use std::fmt::Result as FmtResult;
use std::io::stdout;
use std::io::{Error, Write};
use std::time::Instant;

use sorry::buffer::Buffer;

#[derive(Clone)]
pub struct Logger {
    pub verbose: bool
}

impl Write for Logger {
    fn write(&mut self, buf: &[u8]) -> Result<usize, Error> {
        if self.verbose {
            let stdout = stdout();
            let mut handle = stdout.lock();
            handle.write(buf)
        } else {
            Ok(buf.len())
        }
    }

    fn flush(&mut self) -> Result<(), Error> {
        Ok(())
    }
}

impl Logger {
    pub fn new(verb: bool) -> Self {
        Logger {
            verbose: verb
        }
    }
}

pub struct BenchmarkManager {
    pub beginning: Instant,
    pub after_warm_up: Instant,
    pub end: Instant
}

impl Display for BenchmarkManager {
    fn fmt(&self, fmt: &mut Formatter) -> FmtResult {
        let damas = self.end.duration_since(self.beginning);
        let target = self.end.duration_since(self.after_warm_up);

        let damas_f = damas.as_nanos() as f64 / 1_000_000_000.0;
        let target_f = target.as_nanos() as f64 / 1_000_000_000.0;

        writeln!(fmt, "[BENCH] Damas execution time: {} seconds", damas_f)?;
        writeln!(fmt, "[BENCH] Target execution time: {} seconds", target_f)
    }
}

impl BenchmarkManager {
    pub fn new() -> Self {
        BenchmarkManager {
            beginning: Instant::now(),
            after_warm_up: Instant::now(),
            end: Instant::now()
        }
    }

    pub fn warm_up_done(&mut self) {
        self.after_warm_up = Instant::now();
    }

    pub fn finish(&mut self) {
        self.end = Instant::now();
    }
}

#[derive(Clone, Copy, Debug)]
pub struct BenchedFunction<B: Buffer> {
    pub origin: u64,
    pub target: u64,
    pub prologue: u64,
    pub offset_to_call: usize,
    pub wrapper: B,
}
