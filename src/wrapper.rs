//! This module's purpose is to implement the wrapper used to benchmark a unique function
//! relocalised by Damas.

use std::cmp::min;
use std::io::{ErrorKind, Read, Seek, SeekFrom};
use std::io::Error as IOError;
use std::io::Result as IOResult;
use std::iter::repeat;

use elf::File as ElfFile;

use sorry::{TargetController, TargetProcess};
use sorry::buffer::Buffer;
use sorry::buffer::remote::RemoteBuffer;
use sorry::error::Error;

use crate::error::{DamasError, Result};

static FORMAT: &[u8] = "[BENCH] Function execution time: %ld.%09ld\n".as_bytes();

/// Wrapper type over Buffer to manipulate it through the traits Read and Seek.
struct BufferStream<'a, B: Buffer> {
    pos: usize,
    buffer: &'a B
}

impl<'a, B: Buffer> Read for BufferStream<'a, B> {
    fn read(&mut self, buf: &mut [u8]) -> IOResult<usize> {
        let size = buf.len();
        let available = min(size, self.buffer.get_size() - self.pos);

        match self.buffer.read(available, self.pos) {
            Ok(vec) => {
                buf.copy_from_slice(&vec);
                self.pos += available;
                Ok(available)
            }

            Err(Error::IOError(n)) => Err(IOError::from_raw_os_error(n)),
            Err(e) => {
                let err = format!("{:?}", e);
                Err(IOError::new(ErrorKind::Other, err))
            }
        }
    }
}

impl<'a, B: Buffer> Seek for BufferStream<'a, B> {
    fn seek(&mut self, pos: SeekFrom) -> IOResult<u64> {
        self.pos = match pos {
            SeekFrom::Start(offset) => offset as usize,
            SeekFrom::End(offset) => self.buffer.get_size() - offset as usize,
            SeekFrom::Current(offset) => self.pos + offset as usize
        };

        Ok(self.pos as u64)
    }
}

fn find_vsdo_gettimeofday(ctrl: &TargetController) -> Result<u64> {
    let buf = ctrl.find_memory_map_entry( |entry| entry.filename == Some("[vdso]".to_string()) )
        .map( |rc| (rc.start_addr, rc.end_addr) )
        .map( |(start, end)| RemoteBuffer::new(ctrl.get_pid(), start, (end - start) as usize) )
        .unwrap();

    let mut bufstream = BufferStream {
        pos: 0,
        buffer: &buf
    };

    let elf = ElfFile::open_stream(&mut bufstream).map_err( |e| {
        let msg = format!("{:?}", e);
        DamasError::CustomError(msg)
    })?;

    for section in elf.sections.iter() {
        let symbols = elf.get_symbols(section).map_err( |e| {
            let msg = format!("{:?}", e);
            DamasError::CustomError(msg)
        })?;

        for sym in symbols {
            if sym.name.contains("gettimeofday") {
                return Ok(sym.value + buf.get_address());
            }
        }
    }

    Err(DamasError::CustomError("Could not find gettimeofday".to_string()))
}

/// The context structure contains the addresses of the functions needed by the wrapper to
/// calculate time, call the benchmarked function and print its results.
struct Context {
    gettimeofday: u64,
    printf: u64,

    before_time: u64,
    after_time: u64,
    res_duration: u64,
    format: u64
}

impl Context {
    pub fn new(target: &TargetProcess, address: u64) -> Result<Self> {
        let ctrl = target.get_controller();
        let (libc_address, _) = ctrl.get_libc_address_range().unwrap();

        let printf = ctrl.get_libc_symbol("printf")
            .map( |sym| sym.value + libc_address )?;

        let gettimeofday = find_vsdo_gettimeofday(target.get_controller().as_ref())?;

        Ok(Context {
            gettimeofday: gettimeofday,
            printf: printf,

            before_time: address,
            after_time: address + 16,
            res_duration: address + 32,
            format: address + 48,
        })
    }
}

fn offset_to_symbol(rip: u64, res: &[u8], insn_size: u64, symbol: u64) -> u64 {
    let actual_rip = rip + res.len() as u64 + insn_size;
    symbol.wrapping_sub(actual_rip)
}

fn push_registers(res: &mut Vec<u8>) {
    res.extend(&[0x50, 0x57, 0x56, 0x52, 0x51, 0x41, 0x50, 0x41, 0x51]);
}

fn pop_registers(res: &mut Vec<u8>) {
    res.extend(&[0x41, 0x59, 0x41, 0x58, 0x59, 0x5a, 0x5e, 0x5f, 0x58]);
}

/// Generate the code to call gettimeofday using the given context structure and an address in
/// which to put the results.
fn call_gettimeofday(ctx: &Context, rip: u64, dst: u64) -> Vec<u8> {
    let mut res = vec![];
    push_registers(&mut res);
    let dst_offset = offset_to_symbol(rip, &res, 7, dst) as u32;
    res.extend(&[0x48, 0x8d, 0x05]);
    res.extend(&dst_offset.to_ne_bytes());                  // lea dst, %rax
    res.extend(&[0xbe, 0x00, 0x00, 0x00, 0x00]);            // mov $0, %esi
    res.extend(&[0x48, 0x89, 0xc7]);                        // mov %rax, %rdi
    res.extend(&[0x48, 0xb8]);
    res.extend(&ctx.gettimeofday.to_ne_bytes());            // mov gettimeofday, %rax
    res.extend(&[0xff, 0xd0]);                              // callq *%rax
    pop_registers(&mut res);
    res
}

fn do_timersub(ctx: &Context, rip: u64) -> Vec<u8> {
    let mut res = vec![];
    res.extend(&[0x50, 0x52]);                         // push [%rax, %rdx]
    let after1 = offset_to_symbol(rip, &res, 7, ctx.after_time) as u32;
    res.extend(&[0x48, 0x8b, 0x05]);
    res.extend(&after1.to_ne_bytes());                 // mov    after,%rax
    let before1 = offset_to_symbol(rip, &res, 7, ctx.before_time) as u32;
    res.extend(&[0x48, 0x8b, 0x15]);
    res.extend(&before1.to_ne_bytes());                // mov    before,%rdx
    res.extend(&[0x48, 0x29, 0xd0]);                   // sub    %rdx,%rax
    let result1 = offset_to_symbol(rip, &res, 7, ctx.res_duration) as u32;
    res.extend(&[0x48, 0x89, 0x05]);
    res.extend(&result1.to_ne_bytes());                // mov    %rax,result
    let after2 = offset_to_symbol(rip, &res, 7, ctx.after_time + 8) as u32;
    res.extend(&[0x48, 0x8b, 0x05]);
    res.extend(&after2.to_ne_bytes());                 // mov    after_msec,%rax
    let before2 = offset_to_symbol(rip, &res, 7, ctx.before_time + 8) as u32;
    res.extend(&[0x48, 0x8b, 0x15]);
    res.extend(&before2.to_ne_bytes());                // mov    before_msec,%rdx
    res.extend(&[0x48, 0x29, 0xd0]);                   // sub    %rdx,%rax
    let result2 = offset_to_symbol(rip, &res, 7, ctx.res_duration + 8) as u32;
    res.extend(&[0x48, 0x89, 0x05]);
    res.extend(&result2.to_ne_bytes());                // mov    %rax,result_msec
    // mov    -0x18(%rip),%rax NOTE: useless, so it is not translated
    res.extend(&[0x48, 0x85, 0xc0]);                   // test   %rax,%rax
    res.extend(&[0x79, 0x26]);                         // jns    pop_insns
    let result3 = offset_to_symbol(rip, &res, 7, ctx.res_duration) as u32;
    res.extend(&[0x48, 0x8b, 0x05]);
    res.extend(&result3.to_ne_bytes());                 // mov    result,%rax
    res.extend(&[0x48, 0x83, 0xe8, 0x01]);             // sub    $0x1,%rax
    let result4 = offset_to_symbol(rip, &res, 7, ctx.res_duration) as u32;
    res.extend(&[0x48, 0x89, 0x05]);
    res.extend(&result4.to_ne_bytes());                // mov    %rax,result
    let result5 = offset_to_symbol(rip, &res, 7, ctx.res_duration + 8) as u32;
    res.extend(&[0x48, 0x8b, 0x05]);
    res.extend(&result5.to_ne_bytes());                // mov    result_msec,%rax
    res.extend(&[0x48, 0x05, 0x40, 0x42, 0x0f, 0x00]); // add    $0xf4240,%rax
    let result6 = offset_to_symbol(rip, &res, 7, ctx.res_duration + 8) as u32;
    res.extend(&[0x48, 0x89, 0x05]);
    res.extend(&result6.to_ne_bytes());                // mov    %rax,result_msec
    res.extend(&[0x5a, 0x58]);                         // pop [%rax, %rdx]

    res
}

/// Generate the code to call printf using the given context.
fn call_printf(ctx: &Context, rip: u64) -> Vec<u8> {
    let mut res = vec![];
    push_registers(&mut res);
    let sec = offset_to_symbol(rip, &res, 7, ctx.res_duration) as u32;
    res.extend(&[0x48, 0x8b, 0x35]);
    res.extend(&sec.to_ne_bytes());                     // mov sec, %rsi
    let msec = offset_to_symbol(rip, &res, 7, ctx.res_duration + 8) as u32;
    res.extend(&[0x48, 0x8b, 0x15]);
    res.extend(&msec.to_ne_bytes());                    // mov msec, %rdx
    let format_offset = offset_to_symbol(rip, &res, 7, ctx.format) as u32;
    res.extend(&[0x48, 0x8d, 0x3d]);
    res.extend(&format_offset.to_ne_bytes());           // lea format, %rdi
    res.extend(&[0xb8, 0x00, 0x00, 0x00, 0x00]);        // mov $0, %eax
    res.extend(&[0x48, 0xb9]);
    res.extend(&ctx.printf.to_ne_bytes());              // mov printf, %rcx
    res.extend(&[0xff, 0xd1]);                          // callq *%rcx
    pop_registers(&mut res);

    res
}

pub fn write_wrapper(target: &TargetProcess, address: u64)
-> Result<(usize, Vec<u8>)> {
    let ctx = Context::new(target, address)?;
    let mut rip = ctx.format + FORMAT.len() as u64 + 1;

    let prologue: Vec<u8> = repeat(0x00).take((ctx.format - address) as usize).collect();

    let first = call_gettimeofday(&ctx, rip, ctx.before_time);
    rip += first.len() as u64;

    let call_benched = vec![0xe8, 0x00, 0x00, 0x00, 0x00];
    let func_call = (rip - address + 1) as usize;
    rip += call_benched.len() as u64;

    let second = call_gettimeofday(&ctx, rip, ctx.after_time);
    rip += second.len() as u64;

    let third = do_timersub(&ctx, rip);
    rip += third.len() as u64;

    let fourth = call_printf(&ctx, rip);
    //rip = rip + fourth.len() as u64;

    let epilogue = vec![0xc3];

    let wrapper = [
        prologue, FORMAT.to_vec(), vec![0x00], first, call_benched, second, third, fourth, epilogue
    ].concat();
    Ok((func_call, wrapper))
}
