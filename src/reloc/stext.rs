//! The module stext is the module that handles the creation and manipulation of the .secure_text
//! that contains the protected copy of the program's code.
//!
//! This module is the most important in the whole codebase of Damas. It handles many things among
//! the following:
//!
//! + Creation of dispatch tables for calls, jumps and ret instructions
//! + Creation of buffers to inject the protected code and the tables into the target process
//! + Translation of jump, store and load instructions to make them target the right instructions
//! + Injection of the code into the buffers
//!
//! Most of these tasks are handled by the [`STextSection`] struct. It is the central struct of
//! Damas where everything happens once the original code is disassembled.
//!
//! [`STextSection`]: ./struct.STextSection.html

use std::cmp::min;
use std::collections::BTreeMap;
use std::fmt::Display;

use elf::types::SectionHeader;

use sorry::TargetProcess;
use sorry::buffer::Buffer;
use sorry::buffer::codecache::CodeCache;

use crate::disass::{BasicBlock, EdgeKind, Disassembly, Function};
use crate::eh_frame::function_symbols;
use crate::error::{DamasError, Result};
use crate::insn::{CALL_GROUP, JUMP_GROUP, Insn};
use crate::log::BenchedFunction;
use crate::reloc::{RelocationManager, SectionRange};
use crate::reloc::dispatch::{
    DispatchFunction, DispatchJump, DispatchRet, FunctionDispatchTable, JumpDispatchTable,
    RetDispatchTable
};
use crate::reloc::dispatch::commands;
use crate::reloc::feature::Feature;
use crate::reloc::trans::{
    InjectableDispatchTable, IntoBinary, translate_jcc, translate_jump, translate_whole_basic_block
};

const NEEDED_CALL_SIZE: usize = 18;

/// Alias for a [`BTreeMap`] that maps a [`Function`] to an injectable dispatch table.
pub type FunctionTableMap<B, F, E> = BTreeMap<Function, InjectableDispatchTable<B, F, DispatchJump<F>, E>>;

/// Find a function in the function-table map that contains the given instruction address-wise.
/// Once the function has been found, the corresponding table is returned.
///
/// This function is used in the context of jump translations. When indirect non-call jump
/// instructions are translated, they need to target a jump dispatch table instead of a register.
/// For security matters, only the addresses that belong to the same function as the
/// jump-instruction address are considered valid targets. This is the reason why there is one
/// table for each function of the target process and that the correct table must be retrieved in
/// order to translate an indirect jump instruction.
pub fn find_jdt_from_insn<'a, B, F, E>(jdts: &'a FunctionTableMap<B, F, E>, insn: &Insn)
-> Option<(&'a Function, &'a InjectableDispatchTable<B, F, DispatchJump<F>, E>)>
where B: Buffer, F: Feature, E: Clone + Display {
    jdts.iter()
        .find( |(func, _)| func.start_address <= insn.address && insn.address < func.end_address )
}

/// Compute the padding needed after the given instruction to translate it to a call to the
/// dispatch table.
pub fn padding(insn: &Insn) -> usize {
    if insn.is_jump() {
        let size = insn.size;
        NEEDED_CALL_SIZE - min(size, NEEDED_CALL_SIZE)
    } else {
        0
    }
}

/// Compute the space to add to allow call and jump instructions to be expanded to reach the
/// dispatch tables in the worst case. What it does is take every calls and jumps and if the size
/// of the instruction is smaller than what is needed to insert a `call _dispatch_table`, the
/// number of additional bytes needed is computed and added to a total.
fn padding_added<D: Disassembly>(disass: &D) -> Result<u64> {
    let mut total = 0;

    for bb in disass.basic_blocks()? {
        let term = &bb.terminator;

        if term.is_jump() {
            total += padding(term);
        }
    }

    Ok(total as u64)
}

/// Representation of a .secure_text in the target process.
///
/// This structure is in charge of the management of the target code cache as well as the
/// modification and the injection of the basic blocks of code that should be injected into the
/// section. It is the main structure of both the monitor module and the whole Damas codebase.
pub struct STextSection<B, F1, F2, E, R>
where B: Buffer, F1: Feature, F2: Feature, E: Clone + Display, R: Clone + SectionRange {
    pub address: u64,
    pub size: u64,
    pub cache: B,
    // Dispatch table for function calls.
    pub dt: InjectableDispatchTable<B, F1, DispatchFunction<F1>, E>,
    // Dispatch table for ret instructions.
    pub rdt: InjectableDispatchTable<B, F2, DispatchRet<F2>, E>,
    pub relocs: RelocationManager<R>,
    jumps: Vec<Insn>,
    rets: Vec<Insn>,
    cursor: u64,
    benched_function: Option<BenchedFunction<B>>,
    main_bounds: (u64, u64),
    lazy: bool
}

//unsafe impl<B, F1, F2, E, R> Send for STextSection<B, F1, F2, E, R>
//where B: Buffer, F1: Feature, F2: Feature, E: Clone + Display, R: Clone + SectionRange {}

impl<B, F1, F2, E, R> STextSection<B, F1, F2, E, R>
where B: Buffer, F1: Feature, F2: Feature, E: Clone + Display, R: Clone + SectionRange {
    /// Initialize a new .secure_text section in the target process.
    ///
    /// This function allocates the buffer for the .secure_text section and retrieves the
    /// main-function's boundaries used by the ret-instruction translation process. The
    /// [`FunctionDispatchTable`] and the [`RetDispatchTable`] and stored as is for future use.
    ///
    /// [`FunctionDispatchTable`]: ../dispatch/type.FunctionDispatchTable.html
    /// [`RetDispatchTable`]: ../dispatch/type.RetDispatchTable.html
    pub fn new<D: Disassembly>(
        target: &TargetProcess,
        disass: &D,
        dt: FunctionDispatchTable<F1, E>,
        rdt: RetDispatchTable<F2, E>,
        lazy: bool)
        -> Result<STextSection<CodeCache, F1, F2, E, SectionHeader>> {
            // The program is running, there is a .text section.
            let text = target.get_target_metadata().get_section(".text").unwrap();
            let blocks = disass.blocks()?;
            let text_size: usize = blocks.into_iter().map( |(_, s)| s ).sum();

            let load_address = target.get_controller().get_absolute_load_address();
            let main_sym = target.get_all_symbols()
                .into_iter()
                .find( |sym| sym.name == "main" )
                .unwrap();
            let main_address = main_sym.value + load_address;

            // In theory, adding the computed paddings to the .text section size should work, but
            // if the computed basic blocks overlap, the size of the .secure_text section will be
            // more than the computed one.
            let stext_size = text_size as u64 + padding_added(disass)?;

            let cc = CodeCache::new(target.get_controller(), stext_size as usize)?;

            Ok(STextSection {
                address: cc.get_offset() as u64,
                size: stext_size,
                cache: cc,
                dt: InjectableDispatchTable::new(dt),
                rdt: InjectableDispatchTable::new(rdt),
                relocs: RelocationManager::new(&text.shdr, load_address),
                jumps: vec![],
                rets: vec![],
                cursor: 0,
                benched_function: None,
                main_bounds: (main_address, main_address + main_sym.size),
                lazy: lazy
            })
    }

    /// Create the dispatch jump tables for the functions of the given Disassembly.
    ///
    /// The list of the functions used by this function is not exactly the one given by the Disassembly. If
    /// the target process' ELF file contains symbols, they are used to consider entire functions
    /// and not EH frames as the [`FastDisassembly`] of [`angr`] would provide. The need of this operation
    /// is to avoid splitting actual functions into several smaller ones, otherwise the hypothesis
    /// that indirect jumps only lead to function-local addresses will break.
    ///
    /// The returned [`FunctionTableMap`] thus maps symbol-identified functions and additionally
    /// found functions to a table that contains only the blocks contained in the function's
    /// boundaries.
    ///
    /// [`FastDisassembly`]: https://docs.angr.io/built-in-analyses/cfg
    /// [`angr`]: https://angr.io/
    /// [`FunctionTableMap`]: ./type.FunctionTableMap.html
    pub fn create_function_tables<D>(&mut self, target: &TargetProcess, disass: &D, error: E)
    -> Result<FunctionTableMap<B, F1, E>>
    where D: Disassembly {
        let mut blocks = disass.basic_blocks()?;
        let symbol_functions = function_symbols(target);

        let mut boundaries: Vec<(u64, u64)> = disass.functions()?
            .into_iter()
            .map( |func| {
                // Find a function in the symbol list that contains the function in the
                // Disassembly. If there is none, the given function is used as is.
                let start = func.start_address;
                let opt = symbol_functions.iter().find( |f| f.start <= start && start < f.end );

                // Using the ELF function, the boundaries of the function are retrieved.
                match opt {
                    None => (func.start_address, func.end_address),
                    Some(sym_func) => (sym_func.start, sym_func.end)
                }
            }).collect();

        boundaries.dedup_by( |(sa, _), (sb, _)| sa == sb );
        boundaries.into_iter()
            .map( |(start, end)| {
                // We take every blocks in the basic-block list that are contained in the function
                // boundaries. The blocks placed before the current function start address are
                // removed from the list for performance.
                let func_blocks: Vec<BasicBlock> = blocks
                    .drain_filter( |bb| bb.address < end )
                    .filter( |bb| start <= bb.address )
                    .collect();

                (func_blocks, start, end)
            })
            // Remove the functions that don't contain any indirect jump if lazy mode is enabled.
            .filter( |(blocks, _, _)| !(self.lazy && blocks.iter().all( |b| {
                let term = &b.terminator;
                 ! term.is_indirect_jump() && term.bytes[0] != 0xff
            })) )
            .map( |(func_blocks, start, end)| {
                let jdt = JumpDispatchTable::new(error.clone());
                let mut idt = InjectableDispatchTable::new(jdt);

                disass.populate_dispatch_table(&mut idt.dt, &func_blocks, EdgeKind::Jump)?;
                let new_func = Function {
                    start_address: start,
                    end_address: end,
                    blocks: func_blocks
                };
                Ok((new_func, idt))
            }).collect()
    }

    /// Set a function to be benchmarked. The relocation process will take it into account and
    /// redirect every call to a wrapper that is to measure its execution time.
    pub fn set_benched_function(&mut self, func: BenchedFunction<B>) {
        self.benched_function = Some(func);
    }

    /// When a function has been marked for benchmarking, this function injects the wrapper around
    /// said function's call.
    ///
    /// If the given address matches the one of the benched function, the wrapper's target is set
    /// to the relocation target and the relocation target is set to the address of the wrapper.
    /// Otherwise it does not do anything.
    fn try_inject_wrapper<'a>(&mut self, target: &mut u64, bb: &'a BasicBlock) -> Result<()> {
        if let Some(ref mut bf) = &mut self.benched_function {
            if bf.origin == bb.address {
                bf.target = *target;
                *target = bf.wrapper.get_address() + bf.prologue;

                eprintln!("Target of {:#x} changed from {:#x} to {:#x}",
                    bb.address, bf.target, *target);

                let call_address = bf.wrapper.get_address() + bf.offset_to_call as u64 + 4;
                let offset = bf.target.wrapping_sub(call_address) as u32;
                bf.wrapper.write(&offset.to_ne_bytes(), bf.offset_to_call)?;

                self.relocs.set_benched_relocation((bf.target, *target));
            }
        }

        Ok(())
    }

    /// Copy the basic block into the target codecache, create the corresponding relocation object
    /// and return the terminator of the basic block if it needs a translation.
    fn relocate_basic_block<'a>(&'a mut self, bb: &'a BasicBlock)
    -> Result<Option<(Insn, u64)>> {
        if self.relocs.relocated_address(bb.address) == None {
            if bb.terminator.is_a(JUMP_GROUP) {
                self.jumps.push(bb.terminator.clone());
            } else if bb.terminator.is_a(CALL_GROUP) {
                self.rets.push(bb.terminator.clone());
            }

            let mut target = self.cache.get_address() + self.cursor;
            let term = bb.terminator.clone();
            let new_addr = target + bb.size - term.size as u64;
            self.try_inject_wrapper(&mut target, bb)?;

            let new_size = bb.size + padding(&term) as u64;
            self.relocs.insert(bb.address, bb.size as usize, target, new_size as usize);

            let trans = translate_whole_basic_block(&self.relocs, bb)
                .unwrap_or_else( || bb.bytes.clone());
            self.cache.write(&trans, self.cursor as usize)?;
            self.cursor += new_size;

            Ok(if term.is_jump() { Some((term, new_addr)) } else { None })
        } else {
            Ok(None)
        }
    }

    /// Compute the basic-block's terminator-instruction size. This function must take the
    /// benchmarked function into account and avoid returning the wrapper's first basic-block's
    /// terminator size (which would be 0), otherwise the code right after the relocated terminator
    /// would be corrupted with zeroes.
    fn terminator_size(&self, addr: u64) -> Option<u64> {
        Some(match self.benched_function {
            None => {
                let reloc = self.relocs.reloc_for_address(addr)?;
                reloc.target + reloc.new_size - addr
            }

            // If a function is to be benchmarked the function must compute the right basic-block
            // terminator size, not the wrapper's one.
            Some(ref bf) => {
                let reloc = self.relocs.reloc_for_address(bf.origin)?;
                if bf.target <= addr && addr < bf.target + reloc.new_size {
                    bf.target + reloc.new_size - addr
                } else {
                    let reloc = self.relocs.reloc_for_address(addr)?;
                    reloc.target + reloc.new_size - addr
                }
            }
        })
    }

    /// Assemble the given instruction and inject it in the target process at the given address.
    fn replace_instruction_in_stext<I: IntoBinary>(&mut self, addr: u64, new_insn: I)
    -> Result<()> {
        let offset = self.cache.get_address();
        let res = new_insn.into_binary(addr)?;

        let term_size = self.terminator_size(addr).unwrap_or(0);

        let mut pad = Vec::new();
        pad.resize(term_size as usize, 0x90);

        self.cache
            .write(&pad, (addr - offset) as usize)
            .map_err(&DamasError::from)?;
        self.cache
            .write(&res, (addr - offset) as usize)
            .map_err(&DamasError::from)
    }

    /// Fill the dispatch table with the relocations operated by this object.
    fn fill_dispatch_table<D: Disassembly>(&mut self, disass: &D) -> Result<()> {
        for func in disass.functions()? {
            let target = match self.relocs.iter().find( |rel| rel.origin == func.start_address ) {
                Some(reloc) => reloc.target,
                None => {
                    // Shouldn't come here.
                    let start = func.start_address;
                    eprintln!("Could not find a basic block beginning at address {:#x}", start);

                    self.relocs.relocated_address(start).unwrap_or(0)
                }
            };

            if target != 0 {
                self.dt.dt.add_function(func.start_address, target);
            }
        }

        Ok(())
    }

    /// Fill the dispatch jump table for the given function with the relocations operated by this
    /// object.
    fn fill_function_table<F>(
        &mut self,
        func: &Function,
        jdt: &mut InjectableDispatchTable<B, F, DispatchJump<F>, E>)
    where F: Feature {
        for bb in func.blocks.iter() {
            if let Some(reloc) = self.relocs.bb_reloc_from_insn(&bb.terminator) {
                jdt.dt.add_block(bb.address, bb.size as usize, reloc.target);
            }
        }
    }

    /// Fill the ret dispatch table for the whole program with the relocations operated by this
    /// object.
    fn fill_ret_table(&mut self) -> Result<()> {
        fn size_from_bytes(byte: u8, next: u8) -> u64 {
            match byte {
                0xff => 2,
                0x41 if next <= 0xd0 => 4,
                0x41 => 3,
                0x48 if next == 0xb8 => 18,
                _ => 5
            }
        }

        let mut originals = vec![];

        // For each call-site, we get the call instruction and the next one.
        for call in &self.rets {
            let next_insn = call.address + call.size as u64;

            // We get the relocation of both instructions.
            if let Some(reloc) = self.relocs.relocated_address(call.address) {
                if let Some(target) = self.relocs.relocated_address(next_insn) {
                    // We now need two pieces of information:
                    //  - the first byte of the relocated call instruction ;
                    //  - the second one for disambiguation.
                    let offset = reloc - self.cache.get_address();
                    let byte = self.cache.read_byte(offset as usize)?;
                    let next = self.cache.read_byte(offset as usize + 1)?;

                    let orig_size = size_from_bytes(call.bytes[0], call.bytes[1]);
                    let reloc_size = size_from_bytes(byte, next);

                    self.rdt.dt.add_ret(reloc + reloc_size, target);
                    originals.push((call.address + orig_size, target));
                }
            }
        }

        // If Damas has been attached to a running process via ptrace, it is possible that a return
        // instruction targets the original control-flow. We can prevent that by adding any
        // return-site from the original code in the table. However, we add it after the whole
        // table because such a case should be very rare.
        for (from, to) in originals {
            self.rdt.dt.add_ret(from, to);
        }

        Ok(())
    }

    /// Inject the call dispatch table and the given jump dispatch tables into the target binary.
    fn emit_tables<F>(&mut self, jdts: &mut FunctionTableMap<B, F, E>) -> Result<()>
    where F: Feature {
        for (_, jdt) in jdts.iter_mut() {
            let mut jcc = self.cache.make_another();
            jcc.allocate(jdt.dt.estimate_size())?;

            jdt.emit(jcc, 0)?;
        }

        Ok(())
    }

    /// Translate every jumps in the list using the given tables.
    ///
    /// Once an instruction has been translated, it is injected at the right address in the target
    /// process, replacing the old wrong instruction. The function returns the instructions that
    /// could not be translated, i.e. ret instructions.
    ///
    /// + Calls are either redirected to the main dispatch table or the target offset is changed to
    ///   target the first instruction of the right relocated function.
    ///
    /// + Unconditional jumps are handled the same way as calls except that the dispatch table must
    ///   be retrieved using [`find_jdt_from_insn`] using the jump instruction's address.
    ///
    /// + Conditional jumps are never conditional, so only their offset is modified. This can also
    ///   modify the length of the instruction (if the new offset is too big to be encoded in one
    ///   byte).
    fn translate_jump_list<F>(
        &mut self,
        jdts: &mut FunctionTableMap<B, F, E>,
        jumps: Vec<(Insn, u64)>) -> Result<Vec<(Insn, u64)>>
    where F: Feature {
        let mut res = vec![];

        jumps.into_iter()
            // jump is the original jump instruction and addr is the address of the relocated
            // instruction.
            .try_for_each( |(jump, addr)| {
                // Call instruction are translated using the call dispatch table.
                if jump.is_a(CALL_GROUP) {
                    let insn = translate_jump(&self.relocs, &jump, &self.dt).unwrap();
                    self.replace_instruction_in_stext(addr, insn)
                } else if jump.mnemonic == "ret" || jump.mnemonic == "retq" {
                    if self.main_bounds.0 <= jump.address && jump.address < self.main_bounds.1 {
                        Ok(())
                    } else {
                        res.push((jump, addr));
                        Ok(())
                    }
                } else if jump.mnemonic == "jmp" || jump.mnemonic == "jmpq" {
                    let jdt_opt = find_jdt_from_insn(jdts, &jump);
                    // If the jump instruction does not correspond to a jump dispatch table or
                    // there is no address associated to the table, the instruction is returned as
                    // is.
                    if let Some((_, jdt)) = jdt_opt {
                        let insn = match translate_jump(&self.relocs, &jump, jdt) {
                            Some(res) => res,
                            None => {
                                eprintln!("Could not translate '{} {}' at {:#x}",
                                    jump.mnemonic, jump.op_str, addr);
                                jump.bytes.clone()
                            }
                        };
                        self.replace_instruction_in_stext(addr, insn)
                    } else if ! jump.is_indirect_jump() {
                        // If there is no valid table but the jump is direct, there is still a
                        // chance it can be translated correctly. The call dispatch table is
                        // provided to the translate_jump but it is totally unused.
                        let insn = translate_jump(&self.relocs, &jump, &self.dt)
                            .unwrap_or_else( || jump.bytes );
                        self.replace_instruction_in_stext(addr, insn)
                    } else {
                        eprintln!("No translation for {:?} at {:#x}", jump, addr);
                        Ok(())
                    }
                } else {
                    // Jcc instructions are handled separately.
                    match translate_jcc(&self.relocs, &jump) {
                        Some(insn) => self.replace_instruction_in_stext(addr, insn),
                        None => Ok(eprintln!("Could not find a relocation for {:?}", jump))
                    }
                }
            }).map( |_| res )
    }

    /// Translate the ret instructions to direct jumps to the [`RetDispatchTable`].
    ///
    /// This operation must be done after the translation of all the other jumps since it requires
    /// the length of the calling instruction. Otherwise it works the same as the other
    /// translations:
    ///
    /// + The table is filled with entries
    /// + The buffer is allocated and the table is emitted and injected in it.
    /// + The ret instructions are translated into a direct jump to the table.
    ///
    /// [`RetDispatchTable`]: ../dispatch/type.RetDispatchTable.html
    fn translate_rets(&mut self, rets: Vec<(Insn, u64)>) -> Result<()> {
        self.fill_ret_table()?;

        // The ret dispatch table can be allocated now.
        let mut rcc = self.cache.make_another();
        rcc.allocate(self.rdt.dt.estimate_size())?;

        self.rdt.emit(rcc, 0)?;

        // Finally, the ret instructions are translated into jmp _ret_table
        rets.into_iter()
            .try_for_each( |(_, addr)| {
                let mut insn = vec![0xe9];
                let offset = self.rdt.address.unwrap().wrapping_sub(addr + 5) as u32;
                insn.extend(&offset.to_ne_bytes());
                self.replace_instruction_in_stext(addr, insn)
            })
    }

    /// Relocate the whole Disassembly into the .secure_text section and translate the jump instructions.
    pub fn relocate<D: Disassembly, Fe>(
        &mut self,
        target: &TargetProcess,
        disass: &D,
        make_err: Fe,
        linear: bool) -> Result<FunctionTableMap<B, F1, E>>
    where Fe: FnOnce(u64) -> E {
        // Relocate each basic block and retrieve a list of every jumps.
        let jumps = disass
            .basic_blocks()?
            .into_iter()
            // transpose makes the Result<Option> an Option<Result>, allowing to filter_map the
            // iterator and keeping only the Somes in the list.
            .map( |bb| self.relocate_basic_block(&bb).transpose() )
            .flatten()
            .collect::<Result<Vec<(Insn, u64)>>>()?;

        // Fill the dispatch table with relocation informations.
        self.fill_dispatch_table(disass)?;

        if let Some(ref bf) = self.benched_function {
            self.dt.dt.update_accepting_case(bf.origin, bf.wrapper.get_address());
        }

        // Create the new cache to inject the dispatch table to.
        let mut cc = self.cache.make_another();
        cc.allocate(self.dt.dt.estimate_size())?;

        self.dt.emit(cc, 0)?;

        let dt_addr = self.dt.dt.table_offset(self.dt.cache.as_ref().unwrap().get_address());
        let error = make_err(dt_addr);
        let mut jdts = self.create_function_tables(target, disass, error)?;

        for (func, jdt) in jdts.iter_mut() {
            self.fill_function_table(func, jdt);

            if linear {
                jdt.dt.display_strategy = commands::linear_table_strategy();
            }
        }

        self.emit_tables(&mut jdts)?;

        // Translate jumps and inject them.
        let rets = self.translate_jump_list(&mut jdts, jumps)?;

        // Translate the ret instructions, build the ret table and inject everything.
        self.translate_rets(rets)?;
        Ok(jdts)
    }
}

/// Create a .secure_text structure.
pub fn create_stext_section<D, F1, F2, E>(
    target: &TargetProcess,
    disass: &D,
    dt: FunctionDispatchTable<F1, E>,
    rdt: RetDispatchTable<F2, E>,
    lazy: bool) -> Result<STextSection<CodeCache, F1, F2, E, SectionHeader>>
where D: Disassembly, F1: Feature, F2: Feature, E: Clone+Display {
    STextSection::<CodeCache, F1, F2, _, SectionHeader>::new(target, disass, dt, rdt, lazy)
}

#[cfg(test)]
mod tests {
    use std::convert::TryFrom;
    use std::path::Path;

    use capstone::arch::x86::{X86OperandType, X86Reg};

    use sorry::assembly::create_capstone_object;
    use sorry::breakpoint::{Breakpoint, Mode};

    use crate::insn::reg;
    use crate::disass::LightDisassembly;
    use crate::reloc::dispatch::{DispatchTable, Target};

    use super::*;

    fn target_process(file: &str) -> TargetProcess {
        TargetProcess::new(file, vec![file])
    }

    fn light_disass(target: &TargetProcess, load_address: u64) -> Result<LightDisassembly> {
        let disass = LightDisassembly::new(Path::new(target.get_executable_name()), load_address)?;
        disass.generate_cfg(false, None)?;
        Ok(disass)
    }

    fn dummy_dispatch_table<'a>() -> FunctionDispatchTable<(), &'a str> {
        let mut dt = DispatchTable::new("call 0xdeadbeef");
        dt.add_entry(Target {
            op_str: String::from("%rdx"),
            op_type: X86OperandType::Reg(reg(X86Reg::X86_REG_RDX))
        });

        dt.add_function(0x1024, 0x2048);
        dt.add_function(0xb002, 0xcafe);

        dt
    }

    fn mock_stext_section<'a, F1: Feature, F2: Feature, R: Clone + SectionRange>(
        address: u64,
        size: u64,
        dt: FunctionDispatchTable<F1, &'a str>,
        rdt: RetDispatchTable<F2, &'a str>,
        text: &R) -> STextSection<Vec<u8>, F1, F2, &'a str, R> {
            STextSection {
                address: address,
                size: size,
                cache: vec![0 ; size as usize],
                dt: InjectableDispatchTable::new(dt),
                rdt: InjectableDispatchTable::new(rdt),
                relocs: RelocationManager::new(text, 0),
                jumps: vec![],
                rets: vec![],
                cursor: 0,
                benched_function: None,
                main_bounds: (0, 0),
                lazy: false
            }
    }

    fn disasm(code: &[u8], address: u64) -> Option<Insn> {
        let cs = create_capstone_object();
        let insns = cs.disasm_all(code, address).ok()?;

        insns.iter().last().map( |insn| Insn::try_from(insn).unwrap() )
    }

    #[test]
    fn call_padding_added() {
        let target = target_process("resources/no-pie");
        target.start().unwrap();

        match light_disass(&target, 0x400000) {
            Err(e) => panic!("{:?}", e),
            Ok(cfg) => {
                match padding_added(&cfg) {
                    Ok(padding) => assert_eq!(padding, 463),
                    Err(e) => panic!("{:?}", e)
                }
            }
        }
    }

    #[test]
    fn stext_section_new() {
        let target = target_process("resources/no-pie");
        target.start().unwrap();

        let mut brk = Breakpoint::new(0x401126, Mode::OneShot);
        let brk_res = brk.set(&target.get_controller())
            .and_then( |_| target.get_controller().resume(false))
            .and_then( |_| target.get_controller().wait(None))
            .and_then( |_| brk.clean_code(&target.get_controller()) )
            .and_then( |_| target.get_controller().load_map_file() );

        assert_eq!(brk_res, Ok(()));
        let rip = target.get_controller().get_registers().map( |regs| regs.rip );
        assert_eq!(rip, Ok(brk.get_address()));

        match light_disass(&target, 0x400000) {
            Err(e) => panic!("{:?}", e),
            Ok(cfg) => {
                match create_stext_section::<_, (), (), _>(
                    &target,
                    &cfg,
                    DispatchTable::new(""),
                    DispatchTable::new(""),
                    false) {
                    Err(e) => panic!("{:?}", e),
                    Ok(stext) => assert_eq!(stext.size, 908)
                }
            }
        }
    }

    #[test]
    fn replace_instruction_in_stext() {
        let target = target_process("resources/no-pie");
        target.start().unwrap();

        let mut brk = Breakpoint::new(0x401126, Mode::OneShot);
        let brk_res = brk.set(&target.get_controller())
            .and_then( |_| target.get_controller().resume(false))
            .and_then( |_| target.get_controller().wait(None))
            .and_then( |_| brk.clean_code(&target.get_controller()) )
            .and_then( |_| target.get_controller().load_map_file() );

        assert_eq!(brk_res, Ok(()));

        let mut stext = match light_disass(&target, 0x400000) {
            Err(e) => panic!("{:?}", e),
            Ok(cfg) => {
                match create_stext_section::<_, (), (), _>(
                    &target,
                    &cfg,
                    DispatchTable::new(""),
                    DispatchTable::new(""),
                    false) {
                    Err(e) => panic!("{:?}", e),
                    Ok(stext) => stext
                }
            }
        };

        let address = stext.cache.get_address();
        let insn = "callq 0x1234".to_string();
        let res = stext.replace_instruction_in_stext(address, insn);
        assert!(res.is_ok());
    }

    #[test]
    fn test_translate_jump() {
        let dt = dummy_dispatch_table();

        let call_rax = disasm(&[0xff, 0xd0], 0x1010).unwrap();
        let call_rdx = disasm(&[0xff, 0xd2], 0x1010).unwrap();
        let call_cafe = disasm(&[0xe8, 0xe9, 0xba, 0x00, 0x00], 0x1010).unwrap();

        let mut stext: STextSection::<_, (), (), _, _> =
            mock_stext_section(0, 0, dt, DispatchTable::new(""), &(0x1000, 0xD000));
        stext.relocs.insert(0x1000, 0x17, 0x2048, 0x17);
        stext.relocs.insert(0xCAFA, 0x17, 0xBEEF, 0x20);

        stext.dt.address = Some(0x400000);

        let tr_rax = translate_jump(&stext.relocs, &call_rax, &stext.dt).unwrap().into_binary(0x2058);
        let tr_rdx = translate_jump(&stext.relocs, &call_rdx, &stext.dt).unwrap().into_binary(0x2058);
        let tr_cafe = translate_jump(&stext.relocs, &call_cafe, &stext.dt).unwrap().into_binary(0x2058);

        let res_rax = Box::new(vec![0xe8, 0xa3, 0xdf, 0x3f, 0x00]).into_binary(0x2058);
        let res_rdx = Box::new(vec![0xe8, 0x9e, 0xdf, 0x3f, 0x00]).into_binary(0x2058);
        let res_cafe = Box::new(vec![0xe8, 0x96, 0x9e, 0x00, 0x00]).into_binary(0x2058);

        assert_eq!(res_rax.unwrap(), tr_rax.unwrap());
        assert_eq!(res_rdx.unwrap(), tr_rdx.unwrap());
        assert_eq!(res_cafe.unwrap(), tr_cafe.unwrap());
    }

    #[test]
    #[ignore]
    fn relocate_nolibs() {
        let binary = Path::new("resources/nolibs");
        let res = LightDisassembly::new(binary, 0)
            .and_then( |cfg| { cfg.generate_cfg(false, None)?; Ok(cfg) })
            .and_then( |_cfg| {
                let dt = dummy_dispatch_table();
                let stext = mock_stext_section::<(), (), _>(
                    0x400000,
                    0x40,
                    dt,
                    DispatchTable::new(""),
                    &(0x400000, 0x400019));
                //let mut jdts = stext.create_function_tables(&cfg, "")?;
                //stext.relocate(&cfg, &mut jdts)?;

                assert_ne!(stext.dt.address, None);

                Ok(())
            });

        if let Err(e) = res { panic!("{:?}", e) }
    }

    #[test]
    fn relocate_some_bb() -> Result<()> {
        let code = vec![
            0x8b,  0x43, 0x08,
            0x88, 0x43, 0x0a,
            0x8b, 0x45, 0x08,
            0xc7, 0x03, 0x0d, 0x00, 0x00, 0x00
        ];

        let mut stext: STextSection::<_, (), (), _, _> = mock_stext_section(
            0x0,
            0x2000,
            DispatchTable::new(""),
            DispatchTable::new(""),
            &(0x0000, 0x1000)
        );

        stext.cache = vec![0 ; 0x2000];

        let cs = create_capstone_object();
        let disasm = cs.disasm_count(&code[9..], 0, 1)?;
        let cs_insn = disasm.iter().last().unwrap();
        let insn = Insn::try_from(cs_insn).unwrap();

        let bb = BasicBlock {
            address: 0x0,
            size: 0xe,
            bytes: code.clone(),
            terminator: insn
        };

        let res = stext.relocate_basic_block(&bb);

        assert_eq!(Ok(None), res);

        let code_res = stext.cache[0 .. 0xf].to_vec();
        assert_eq!(code, code_res);
        Ok(())
    }
}
