//! The dispatch module is all about how functions are relocated. It works using a dispatch table
//! that maps the address of a function to the address of the relocated one. So if an indirect call
//! shall lead to a function, the dispatch table is able to redirect the control flow to the
//! relocated function.
//!
//! For instance, if an instruction `call %rax` shall jump to 0x1024 but the corresponding function
//! has been relocated to 0x2048, the dispatch code will be the following :
//!
//! ```asm
//! sub $0x1024, %rax
//! jz 0x2048
//! ```
//!
//! However, indirect jumps in x86 are not limited to `%rax`. Instead of having as much tables as
//! possible operands (an address or a register), the entry point of the unique table is variable
//! and depends on the operand. The goal here is to move the target address into `%rax` in order to
//! have only one table that uses `%rax` for computations.
//!
//! Here is an exemple dispatch table :
//!
//! ```asm
//!   mov %rdx, %rax     ; call *%rdx
//!   jmp dispatch_table
//!   mov %rcx, %rax     ; call *%rcx
//!   jmp dispatch_table
//! dispatch_table:
//!   cmp 0x1024, %rax
//!   jz 0x2048
//! ; ...
//! ```

pub mod commands;
pub mod serialize;

pub use serialize::*;

use std::fmt::{Display, Formatter};
use std::fmt::Result as FmtResult;
use std::marker::PhantomData;

use capstone::arch::x86::{X86Reg, X86OperandType};

use crate::asm::emit_binary_code;
use crate::error::Result;
use crate::insn::reg;
use crate::reloc::feature::Feature;

#[derive(Clone, Debug, PartialEq)]
pub struct Target {
    pub op_type: X86OperandType,
    pub op_str: String
}

impl Display for Target {
    fn fmt(&self, fmt: &mut Formatter<'_>) -> FmtResult {
        let chs: Vec<char> = self.op_str.chars().collect();
        let s = if chs[0] == '*' {
            chs[1..].iter().collect::<String>()
        } else {
            self.op_str.clone()
        };

        write!(fmt, "{}", s)
    }
}

impl Target {
    /// Test if the target operand represents an RIP-relative address.
    pub fn is_rip_relative(&self) -> bool {
        use capstone::arch::x86::X86Reg::*;

        match self.op_type {
            X86OperandType::Mem(mem) => {
                mem.base() == reg(X86_REG_RIP) && mem.index() == reg(X86_REG_INVALID)
            }

            _ => false
        }
    }
}

/// Entry point of the dispatch table according to some target operand.
#[derive(Clone, PartialEq)]
pub struct DispatchEntry {
    pub target: Target,
    pub size: usize,
    pub offset: u64
}

impl Display for DispatchEntry {
    fn fmt(&self, fmt: &mut Formatter<'_>) -> FmtResult {
        writeln!(fmt, "mov {},%rax", self.target)?;
        writeln!(fmt, "jmp _table_cases")
    }
}

impl DispatchEntry {
    /// Create a DispatchEntry that represents the default entry which uses `%rax` as input.
    pub fn rax_entry() -> Self {
        let target = Target {
            op_type: X86OperandType::Reg(reg(X86Reg::X86_REG_RAX)),
            op_str: String::from("%rax")
        };

        DispatchEntry {
            target: target,
            size: 0,
            offset: 0
        }
    }

    /// Compute the final size and the inner offset to the dispatch table address of an entry
    /// depending on its target operand.
    pub fn compute_entry_size(target: &Target, table_offset: u64) -> usize {
        use X86Reg::*;

        // Note that the offset is always positive.
        let jmp_offset = if table_offset >= 0x80 { 5 } else { 2 };

        match target.op_type {
            X86OperandType::Reg(_) => 3 + jmp_offset,
            X86OperandType::Mem(mem) => {
                // Only an immediate address.
                if mem.base() == reg(X86Reg::X86_REG_INVALID) {
                    7 + jmp_offset
                } else if mem.index() != reg(X86Reg::X86_REG_INVALID) {
                    let is_annoying = [reg(X86_REG_R13), reg(X86_REG_RBP)].contains(&mem.base());
                    let annoying_offset = if is_annoying { 1 } else { 0 };

                    let disp_size = match mem.disp() {
                        0 => annoying_offset,
                        x if -0x81 < x && x < 0x80 => 1,
                        _ => 4
                    };

                    disp_size + 4 + jmp_offset
                } else {
                    let is_annoying = [reg(X86_REG_R12), reg(X86_REG_RSP)].contains(&mem.base());
                    let is_very_annoying = [reg(X86_REG_R13), reg(X86_REG_RBP)]
                        .contains(&mem.base());

                    let annoying_offset = if is_annoying { 1 } else { 0 };
                    let very_annoying_offset = if is_very_annoying { 1 } else { 0 };

                    let disp_size = match mem.disp() {
                        0 => annoying_offset + very_annoying_offset,
                        x if -0x81 < x && x < 0x80 => 1 + annoying_offset,
                        _ => 4 + annoying_offset
                    };

                    disp_size + 3 + jmp_offset
                }
            },
            _ => 0 // unreachable!()
        }
    }
}

/// A dispatch case is a datatype that can be used to redirect an indirect jump. It links the
/// original target to the relocated one.
pub trait DispatchCase<F: Feature>: Clone + Display + PartialEq + HasKind {
    /// Get an address or integer that serves as an identifier.
    fn identifier(&self) -> u64;

    /// Get the x86 register used by the table to do the comparisons.
    fn preferred_register() -> String;

    /// Get the relocated address of a dispatch case in the table.
    fn get_relocation(&self, addr: u64) -> Option<u64>;

    /// Replace the target of the relocation.
    fn set_target(&mut self, target: u64);

    /// Test if the given address discriminates the dispatch case.
    fn accepts(&self, address: u64) -> bool;

    /// Get the maximum size in bytes of the case when assembled.
    fn estimate_size() -> usize;

    /// Get the code that must be executed right before the cases.
    fn before_cases() -> String;
}

/// A relocated function in the binary.
#[derive(Clone, Debug)]
pub struct DispatchFunction<F: Feature> {
    pub original_address: u64,
    pub relocated_address: u64,
    _feature: PhantomData<F>
}

impl<F: Feature> PartialEq for DispatchFunction<F> {
    fn eq(&self, other: &Self) -> bool {
        self.original_address == other.original_address
    }
}

impl<F: Feature> Display for DispatchFunction<F> {
    fn fmt(&self, fmt: &mut Formatter<'_>) -> FmtResult {
        writeln!(fmt, "cmp $0x{:x},%eax", self.original_address as u32)?;
        writeln!(fmt, "jnz _after_0x{:x}", self.original_address as u32)?;
        writeln!(fmt, "{}", F::entering_case(self))?;
        writeln!(fmt, "mov $0x{:x},%rax", self.relocated_address)?;
        writeln!(fmt, "{}", F::exiting_case(self))?;
        writeln!(fmt, "jmpq *%rax")?;

        if self.relocated_address <= u32::MAX as u64 {
            writeln!(fmt, "nop\nnop\nnop")?;
        }

        writeln!(fmt, "_after_0x{:x}:", self.original_address as u32)
    }
}

impl<F: Feature> HasKind for DispatchFunction<F> {
    fn kind() -> TableKind {
        TableKind::Main
    }
}

impl<F: Feature> DispatchCase<F> for DispatchFunction<F> {
    fn identifier(&self) -> u64 {
        self.original_address
    }

    fn preferred_register() -> String {
        String::from("%eax")
    }

    fn get_relocation(&self, addr: u64) -> Option<u64> {
        if self.original_address == addr { Some(self.relocated_address) } else { None }
    }

    fn set_target(&mut self, target: u64) {
        self.relocated_address = target;
    }

    fn accepts(&self, address: u64) -> bool {
        self.original_address == address
    }

    fn estimate_size() -> usize {
        19 + F::size_in_case()
    }

    fn before_cases() -> String {
        String::new()
    }
}

impl<F: Feature> DispatchFunction<F> {
    pub fn new(orig: u64, reloc: u64) -> Self {
        DispatchFunction {
            original_address: orig,
            relocated_address: reloc,
            _feature: PhantomData
        }
    }
}

/// A relocated ret instruction.
#[derive(Clone, Debug, PartialEq)]
pub struct DispatchRet<F: Feature> {
    pub original_address: u64,
    pub relocated_address: u64,
    _feature: PhantomData<F>
}

impl<F: Feature> Display for DispatchRet<F> {
    fn fmt(&self, fmt: &mut Formatter<'_>) -> FmtResult {
        writeln!(fmt, "cmp $0x{:x},%r11d", self.original_address as u32)?;
        writeln!(fmt, "jnz _after_0x{:x}", self.original_address as u32)?;
        writeln!(fmt, "{}", F::entering_case(self))?;
        writeln!(fmt, "mov $0x{:x},%r11", self.relocated_address)?;
        writeln!(fmt, "mov %r11,(%rsp)")?;
        writeln!(fmt, "mov -0x8(%rsp),%r11")?;
        writeln!(fmt, "add $0x8,%rsp")?;
        writeln!(fmt, "{}", F::exiting_case(self))?;
        writeln!(fmt, "jmpq *-0x8(%rsp)")?;

        if self.relocated_address < u32::MAX as u64 {
            writeln!(fmt, "nop\nnop\nnop")?;
        }

        writeln!(fmt, "_after_0x{:x}:", self.original_address as u32)
    }
}

impl<F: Feature> HasKind for DispatchRet<F> {
    fn kind() -> TableKind {
        TableKind::Return
    }
}

impl<F: Feature> DispatchCase<F> for DispatchRet<F> {
    fn identifier(&self) -> u64 {
        self.original_address
    }

    fn preferred_register() -> String {
        String::from("%r11d")
    }

    fn get_relocation(&self, addr: u64) -> Option<u64> {
        if self.original_address == addr { Some(self.relocated_address) } else { None }
    }

    fn set_target(&mut self, target: u64) {
        self.relocated_address = target;
    }

    fn accepts(&self, address: u64) -> bool {
        self.original_address == address
    }

    fn estimate_size() -> usize {
        36 + F::size_in_case()
    }

    fn before_cases() -> String {
        String::from("mov %r11,-0x8(%rsp)\nmov (%rsp),%r11\n")
    }
}

impl<F: Feature> DispatchRet<F> {
    pub fn new(orig: u64, reloc: u64) -> Self {
        DispatchRet {
            original_address: orig,
            relocated_address: reloc,
            _feature: PhantomData
        }
    }
}

/// A relocated indirect jump in the binary.
#[derive(Clone, Debug, PartialEq)]
pub struct DispatchJump<F: Feature> {
    pub original_block_address: u64,
    pub original_block_size: usize,
    pub relocated_block_address: u64,
    _feature: PhantomData<F>
}

impl<F: Feature> Display for DispatchJump<F> {
    fn fmt(&self, fmt: &mut Formatter<'_>) -> FmtResult {
        let (off, _) = self.relocated_block_address.overflowing_sub(self.original_block_address);
        let bb_end = self.original_block_address + self.original_block_size as u64;

        writeln!(fmt, "cmp $0x{:x},%eax", self.original_block_address as u32)?;
        writeln!(fmt, "jl _after_0x{:x}", self.original_block_address as u32)?;
        writeln!(fmt, "cmp $0x{:x},%eax", bb_end as u32)?;
        writeln!(fmt, "jge _after_0x{:x}", self.original_block_address as u32)?;

        writeln!(fmt, "{}", F::entering_case(self))?;

        if (std::i32::MIN as i64) < off as i64 && (off as i64) < std::i32::MAX as i64 {
            writeln!(fmt, "add ${}, %rax", off as i32)?;
            writeln!(fmt, "mov %rax, -8(%rsp)")?;
            writeln!(fmt, "popfq")?;
            writeln!(fmt, "mov -0x18(%rsp), %rax")?;

            writeln!(fmt, "{}", F::exiting_case(self))?;
            // The stack moved up 8 bytes, so the target address is now at -0x10(%rsp) instead of
            // -0x8(%rsp).
            writeln!(fmt, "jmpq *-0x10(%rsp)")?;
            writeln!(fmt, "nop\nnop\nnop\nnop")?;
        } else {
            writeln!(fmt, "movl ${}, -4(%rsp)", off >> 32)?;
            writeln!(fmt, "movl ${}, -8(%rsp)", off as i32)?;
            writeln!(fmt, "add %eax, -8(%rsp)")?;
            writeln!(fmt, "popfq")?;

            writeln!(fmt, "{}", F::exiting_case(self))?;

            writeln!(fmt, "jmpq *-0x10(%rsp)")?;
        }

        writeln!(fmt, "_after_0x{:x}:", self.original_block_address as u32)?;
        writeln!(fmt, "nop") // The nop is needed to compile with Keystone...
    }
}

impl <F: Feature> HasKind for DispatchJump<F> {
    fn kind() -> TableKind {
        TableKind::Any
    }
}

impl<F: Feature> DispatchCase<F> for DispatchJump<F> {
    fn identifier(&self) -> u64 {
        self.original_block_address
    }

    fn preferred_register() -> String {
        String::from("%eax")
    }

    fn get_relocation(&self, addr: u64) -> Option<u64> {
        let upper_bound = self.original_block_address + self.original_block_size as u64;

        if self.original_block_address <= addr && addr < upper_bound {
            let offset = addr - self.original_block_address;
            Some(self.relocated_block_address + offset)
        } else {
            None
        }
    }

    fn set_target(&mut self, target: u64) {
        self.relocated_block_address = target;
    }

    fn accepts(&self, address: u64) -> bool {
        let block_end = self.original_block_address + self.original_block_size as u64;
        self.original_block_address <= address && block_end < address
    }

    fn estimate_size() -> usize {
        40 + F::size_in_case()
    }

    fn before_cases() -> String {
        String::from("pushfq\n")
    }
}

/// Dispatch table that contains all the logic to dispatch indirect calls to the corresponding
/// relocated function address.
#[derive(Clone)]
pub struct DispatchTable<F: Feature, Case: DispatchCase<F>, E: Clone + Display> {
    pub entries: Vec<DispatchEntry>,
    pub cases: Vec<Case>,
    pub error_case: E,
    pub display_strategy: commands::Strategy<F, Case, E>,
    _feature: PhantomData<F>
}

impl<F, Case, E> HasKind for DispatchTable<F, Case, E>
where F: Feature, Case: DispatchCase<F>, E: Clone + Display {
    fn kind() -> TableKind {
        Case::kind()
    }
}

impl<F: Feature, E: Clone + Display> DispatchTable<F, DispatchFunction<F>, E> {
    /// Add a function to the dispatch table.
    pub fn add_function(&mut self, orig: u64, reloc: u64) {
        let func = DispatchFunction::new(orig, reloc);

        if ! self.cases.contains(&func) {
            self.cases.push(func);
        }
    }
}

impl<F: Feature, E: Clone + Display> DispatchTable<F, DispatchJump<F>, E> {
    /// Add a basic block to the dispatch table.
    pub fn add_block(&mut self, orig: u64, size: usize, reloc: u64) {
        let jump = DispatchJump {
            original_block_address: orig,
            original_block_size: size,
            relocated_block_address: reloc,
            _feature: PhantomData
        };

        self.cases.push(jump);
    }
}

impl<F: Feature, E: Clone + Display> DispatchTable<F, DispatchRet<F>, E> {
    /// Add a function to the dispatch table.
    pub fn add_ret(&mut self, orig: u64, reloc: u64) {
        let func = DispatchRet::new(orig, reloc);
        self.cases.push(func);
    }
}

impl<F: Feature, Case: DispatchCase<F>, E: Clone + Display> DispatchTable<F, Case, E> {
    /// Create an empty dispatch table with only the default `%rax` entry.
    pub fn new(e: E) -> Self {
        DispatchTable {
            entries: vec![DispatchEntry::rax_entry()],
            cases: vec![],
            error_case: e,
            display_strategy: commands::tree_table_strategy(),
            _feature: PhantomData
        }
    }

    /// Add an entry operand to the dispatch table.
    pub fn add_entry(&mut self, target: Target) {
        let rax = DispatchEntry::rax_entry();
        let front = self.entries.first().unwrap_or(&rax);
        let size = DispatchEntry::compute_entry_size(&target, front.offset);

        let entry = DispatchEntry {
            target: target,
            size: size,
            offset: front.offset + size as u64
        };

        // Test if the target is already present in the table entries instead of adding it blindly.
        if !self.entries.iter().any( |e| e.target == entry.target ) {
            self.entries.insert(0, entry);
        }
    }

    /// Update the dispatch case that accepts the given origin address.
    pub fn update_accepting_case(&mut self, origin: u64, target: u64) {
        for c in self.cases.iter_mut() {
            if c.accepts(origin) {
                c.set_target(target);
                break;
            }
        }
    }

    pub fn get_relocation(&self, addr: u64) -> Option<u64> {
        self.cases.iter()
            .map( |case| case.get_relocation(addr) )
            .find( |opt| opt.is_some() )
            .and_then( |res| res )
    }

    pub fn estimate_size(&self) -> usize {
        let func = self.display_strategy.total_size;
        func(self)
    }

    /// Get the offset from the dispatch table of the entry that matches the given jump operand.
    pub fn entry_offset(&self, op: X86OperandType) -> Option<u64> {
        self.entries.iter()
            .find( |entry| entry.target.op_type == op )
            .map( |entry| entry.offset )
    }

    /// Emit the binary code of the dispatch table at the given address.
    pub fn emit(&self, addr: u64) -> Result<Vec<u8>> {
        let code = format!("{}", self);
        emit_binary_code(code, addr)
    }

    /// Get the offset in the table at which comparisons start.
    pub fn table_offset(&self, address: u64) -> u64 {
        let entries = self.entries.first().map( |entry| entry.offset as usize ).unwrap_or(0);
        F::size_before_entries(self) as u64 + entries as u64 + address
    }
}

impl<F: Feature, Case: DispatchCase<F>, E: Clone + Display> Display for DispatchTable<F, Case, E> {
    fn fmt(&self, fmt: &mut Formatter<'_>) -> FmtResult {
        // Somehow, the function needs to be bound to be called. Otherwise, rustc whines about
        // display_command not being a method of DispatchTable.
        let func = self.display_strategy.command;
        func(self, fmt)
    }
}

pub type FunctionDispatchTable<F, E> = DispatchTable<F, DispatchFunction<F>, E>;
pub type JumpDispatchTable<F, E> = DispatchTable<F, DispatchJump<F>, E>;
pub type RetDispatchTable<F, E> = DispatchTable<F, DispatchRet<F>, E>;

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn display_dispatch_function() {
        let df = DispatchFunction::<()>::new(0x1024, 0x2048);
        let s = format!("{}", df);
        let exp_s = "cmp $0x1024,%eax\n".to_string()
            + "jnz _after_0x1024\n\n"
            + "mov $0x2048,%rax\n\n"
            + "jmpq *%rax\n"
            + "nop\n"
            + "nop\n"
            + "nop\n"
            + "_after_0x1024:\n";

        assert_eq!(exp_s, s);
    }

    #[test]
    fn display_dispatch_jump() {
        let dj = DispatchJump::<()> {
            original_block_address: 0x400000,
            original_block_size: 16,
            relocated_block_address: 0x800000,
            _feature: PhantomData
        };

        let s = format!("{}", dj);
        let exp_s = "cmp $0x400000,%eax\n".to_string()
            + "jl _after_0x400000\n"
            + "cmp $0x400010,%eax\n"
            + "jge _after_0x400000\n\n"
            + "add $4194304, %rax\n"
            + "mov %rax, -8(%rsp)\n"
            + "popfq\n"
            + "mov -0x18(%rsp), %rax\n\n"
            + "jmpq *-0x10(%rsp)\n"
            + "nop\n"
            + "nop\n"
            + "nop\n"
            + "nop\n"
            + "_after_0x400000:\n"
            + "nop\n";

        assert_eq!(exp_s, s);
    }

    #[test]
    fn display_empty_dispatch_table() {
        let err = "call *0xdeadbeef\n";
        let mut dt: DispatchTable<(), DispatchFunction<()>, _> = DispatchTable::new(err);
        dt.display_strategy = commands::linear_table_strategy();
        let s = format!("{}", dt);
        let exp_s = "\ncall *0xdeadbeef\n\n";

        assert_eq!(exp_s, s);
    }

    #[test]
    fn display_dummy_dispatch_table() {
        let err = "call *0xdeadbeef\n";
        let mut dt: DispatchTable<(), _, _> = DispatchTable::new(err);
        dt.display_strategy = commands::linear_table_strategy();

        dt.add_entry(Target {
            op_str: String::from("%rdx"),
            op_type: X86OperandType::Reg(reg(X86Reg::X86_REG_RDX))
        });
        dt.add_entry(Target {
            op_str: String::from("%rdi"),
            op_type: X86OperandType::Reg(reg(X86Reg::X86_REG_RDI))
        });

        dt.add_function(0x1024, 0x2048);
        dt.add_function(0xb002, 0xcafe);

        let s = format!("{}", dt);
        let exp = "\nmov %rdi,%rax\n".to_owned()
            + "jmp _table_cases\n"
            + "mov %rdx,%rax\n"
            + "jmp _table_cases\n"
            + "_table_cases:\n\n\n"
            + "cmp $0x1024,%eax\n"
            + "jnz _after_0x1024\n\n"
            + "mov $0x2048,%rax\n\n"
            + "jmpq *%rax\n"
            + "nop\n"
            + "nop\n"
            + "nop\n"
            + "_after_0x1024:\n"
            + "cmp $0xb002,%eax\n"
            + "jnz _after_0xb002\n\n"
            + "mov $0xcafe,%rax\n\n"
            + "jmpq *%rax\n"
            + "nop\n"
            + "nop\n"
            + "nop\n"
            + "_after_0xb002:\n"
            + "call *0xdeadbeef\n\n";

        assert_eq!(exp, s);
    }

    #[test]
    fn far_call_case_size() -> Result<()> {
        let estimate = DispatchFunction::<()>::estimate_size();
        let ret = DispatchFunction::<()>::new(0x555c40097000, 0xaaac4009f00b);

        let bin = emit_binary_code(format!("{}", ret), 0xfffc4009f00b)?;
        assert_eq!(estimate, bin.len());

        Ok(())
    }

    #[test]
    fn call_case_size() -> Result<()> {
        let estimate = DispatchFunction::<()>::estimate_size();
        let ret = DispatchFunction::<()>::new(0x1024, 0x2048);

        let bin = emit_binary_code(format!("{}", ret), 0x4096)?;
        assert_eq!(estimate, bin.len());

        Ok(())
    }

    #[test]
    fn far_ret_case_size() -> Result<()> {
        let estimate = DispatchRet::<()>::estimate_size();
        let ret = DispatchRet::<()>::new(0x555c40097000, 0xaaac4009f00b);

        let bin = emit_binary_code(format!("{}", ret), 0xfffc4009f00b)?;
        assert_eq!(estimate, bin.len());

        Ok(())
    }

    #[test]
    fn ret_case_size() -> Result<()> {
        let estimate = DispatchRet::<()>::estimate_size();
        let ret = DispatchRet::<()>::new(0x1024, 0x2048);

        let bin = emit_binary_code(format!("{}", ret), 0x4096)?;
        assert_eq!(estimate, bin.len());

        Ok(())
    }

    #[test]
    fn far_jump_case_size() -> Result<()> {
        let estimate = DispatchJump::<()>::estimate_size();
        let ret = DispatchJump::<()> {
            original_block_address: 0x555c40097000,
            original_block_size: 0xe,
            relocated_block_address: 0xaaac4009f00b,
            _feature: PhantomData
        };

        let bin = emit_binary_code(format!("{}", ret), 0xfffc4009f00b)?;
        assert_eq!(estimate, bin.len());

        Ok(())
    }

    #[test]
    fn jump_case_size() -> Result<()> {
        let estimate = DispatchJump::<()>::estimate_size();
        let ret = DispatchJump::<()> {
            original_block_address: 0x1024,
            original_block_size: 0xe,
            relocated_block_address: 0x2048,
            _feature: PhantomData
        };

        let bin = emit_binary_code(format!("{}", ret), 0x4096)?;
        assert_eq!(estimate, bin.len());

        Ok(())
    }
}
