use std::convert::TryInto;
use std::fmt::Display;

use serde::{Deserialize, Serialize};

use sorry::buffer::Buffer;
use sorry::TargetProcess;

use crate::error::Result;
use crate::logging::function_name_from_address;
use crate::reloc::SectionRange;
use crate::reloc::dispatch::{DispatchCase, DispatchTable};
use crate::reloc::feature::{Counter, Feature};
use crate::reloc::patch::TablePatcher;
use crate::reloc::stext::{FunctionTableMap, STextSection};
use crate::reloc::trans::InjectableDispatchTable;

#[derive(Serialize, Deserialize, Debug)]
pub enum TableKind {
    Main, Return, Any
}

pub trait HasKind {
    fn kind() -> TableKind;
}

/// A table does not need to be fully serialized. What is really interesting is the order in which
/// its [`cases`] are ordered. The goal of the serializer is to be able to save this order in a
/// file and then retrieve it for another Damas run.
///
/// [`cases`]: ../trait.DispatchCase.html
#[derive(Serialize, Deserialize, Debug)]
pub struct TableSerializer {
    pub kind: TableKind,
    pub symbol: String,
    pub indexes: Vec<usize>
}

/// Retrieve the counter values from the given injectable table. The table is supposed already
/// injected and used, so that the counters are not all null. This function does not take the table
/// counter into account because it is only used for analysis and not for optimizations.
fn get_counters<B, C, E>(table: &InjectableDispatchTable<B, Counter, C, E>) -> Result<Vec<u32>>
where B: Buffer, C: DispatchCase<Counter>, E: Clone + Display {
    let mut res = vec![];
    let mut idx = 1;

    let case_count = table.dt.cases.len();
    let buffer = table.cache.as_ref().unwrap();

    while idx <= case_count {
        let offset = idx * 4;

        // Read a counter value and convert it to a proper u32. We statically know that we are
        // reading 4 bytes from the buffer so the call to try_into shall not fail. The value can be
        // safely unwrapped.
        let word = buffer.read(4, offset)?;
        let counter = u32::from_ne_bytes(word.try_into().unwrap());

        res.push(counter);
        idx += 1;
    }

    Ok(res)
}

impl TableSerializer {
    /// Create a serializer using an existing table and the offset in the ELF file it corresponds
    /// to.
    pub fn from_table<B, Case, E, S>(
        symbol: S,
        table: &InjectableDispatchTable<B, Counter, Case, E>) -> Result<Self>
        where B: Buffer, Case: DispatchCase<Counter>, E: Clone + Display, S: AsRef<str> {
            let kind = DispatchTable::<Counter, Case, E>::kind();

            let mut indexes = get_counters(table)?
                .into_iter()
                .enumerate()
                .collect::<Vec<_>>();

            indexes.sort_by( |(_, counter1), (_, counter2)| counter2.cmp(counter1) );

            Ok(TableSerializer {
                kind: kind,
                symbol: symbol.as_ref().to_string(),
                indexes: indexes.into_iter().map( |(idx, _)| idx ).collect()
            })
    }

    /// Using a serializer and an original table, this function create an appropriate
    /// [TablePatcher] that can patch the given table to give it the same case-order as the one
    /// described by the serializer. In other words, this function creates a table from another
    /// table and a serializer.
    pub fn make_patcher<'a, B, F, C, E>(&self, orig: &'a mut InjectableDispatchTable<B, F, C, E>)
        -> TablePatcher<'a, B, F, C, E>
        where B: Buffer, F: Feature, C: DispatchCase<F>, E: Clone + Display {
            let mut patcher = TablePatcher::new(orig);

            for (at, idx) in self.indexes.iter().enumerate() {
                if at != *idx {
                    let case = patcher.original().dt.cases[*idx].clone();
                    patcher.replace_at(at, case);
                }
            }

            patcher
    }
}

/// Type for the serialization of a whole Damas execution. It will handle the main call table, the
/// return table as well as all the jump tables. This type is the one that must be used by the main
/// function to read or write a DispatchTable configuration.
#[derive(Deserialize, Serialize, Debug)]
pub struct DamasSerializer(Vec<TableSerializer>);

impl DamasSerializer {
    /// Create a DamasSerializer from every tables of the process.
    pub fn new<B, E, R>(
        target: &TargetProcess,
        stext: &STextSection<B, Counter, Counter, E, R>,
        jdts: &FunctionTableMap<B, Counter, E>) -> Result<Self>
    where B: Buffer,
          E: Clone + Display,
          R: Clone + SectionRange {
        let main_serializer = TableSerializer::from_table("", &stext.dt)?;
        let ret_serializer = TableSerializer::from_table("", &stext.rdt)?;

        let mut vec = Vec::with_capacity(jdts.len() + 2);
        vec.push(main_serializer);
        vec.push(ret_serializer);

        for (func, table) in jdts.iter() {
            if !table.dt.cases.is_empty() {
                let sym = function_name_from_address(target, func);
                let serializer = TableSerializer::from_table(sym, table)?;
                vec.push(serializer);
            }
        }

        Ok(DamasSerializer(vec))
    }

    /// Given the current serializer, a target process and each tables it bears, every table is
    /// patched according to the serializers.
    pub fn patch_tables<B, E, R>(
        &mut self,
        target: &TargetProcess,
        stext: &mut STextSection<B, Counter, Counter, E, R>,
        jdts: &mut FunctionTableMap<B, Counter, E>) -> Result<()>
    where B: Buffer,
          E: Clone + Display,
          R: Clone + SectionRange {
        let mut iterator = self.0.iter();
        let main_serializer = iterator.next().unwrap();
        let ret_serializer = iterator.next().unwrap();
        let serializers: Vec<&TableSerializer> = iterator.collect();

        stext.rdt.dt.cases.sort_by_key( |case| case.identifier() );

        main_serializer.make_patcher(&mut stext.dt).patch_whole_table()?;
        ret_serializer.make_patcher(&mut stext.rdt).patch_whole_table()?;

        for (func, table) in jdts.iter_mut() {
            let sym = function_name_from_address(target, func);

            if let Some(seria) = serializers.iter().find( |s| s.symbol == sym ) {
                seria.make_patcher(table).patch_whole_table()?;
            } else {
                let symbol = function_name_from_address(target, func);
                eprintln!("No serializer found for symbol {}", symbol);
            }
        }

        Ok(())
    }
}
