use std::fmt::{Display, Formatter};
use std::fmt::Result as FmtResult;

use crate::asm::emit_binary_code;
use crate::reloc::dispatch::{DispatchCase, DispatchEntry, DispatchTable};
use crate::reloc::feature::Feature;

#[derive(Clone)]
pub struct Strategy<F, Case, E>
where F: Feature, Case: DispatchCase<F>, E: Clone + Display {
    pub command: fn(&DispatchTable<F, Case, E>, &mut Formatter<'_>) -> FmtResult,
    pub total_size: fn(&DispatchTable<F, Case, E>) -> usize
}

/// Display the given table as a linear list of cases.
fn display_linear_table<F, Case, E>(
    table: &DispatchTable<F, Case, E>,
    fmt: &mut Formatter<'_>) -> FmtResult
where F: Feature, Case: DispatchCase<F>, E: Clone + Display {
    let rax_entry = DispatchEntry::rax_entry();

    writeln!(fmt, "{}", F::before_entries(table))?;

    if !table.cases.is_empty() {
        for entry in table.entries.iter() {
            if *entry != rax_entry {
                write!(fmt, "{}", entry)?;
            }
        }

        writeln!(fmt, "_table_cases:")?;

        writeln!(fmt, "{}", Case::before_cases())?;

        writeln!(fmt, "{}", F::entering_table(table))?;

        for func in table.cases.iter() {
            write!(fmt, "{}", func)?;
        }
    }

    writeln!(fmt, "{}", table.error_case)
}

fn total_size_linear_table<F, Case, E>(table: &DispatchTable<F, Case, E>) -> usize
where F: Feature, Case: DispatchCase<F>, E: Clone + Display {
    let cases = Case::estimate_size() * table.cases.len();
    let entries = table.entries.first().map( |entry| entry.offset as usize ).unwrap_or(0);

    let code = format!("{}", table.error_case);
    let error = emit_binary_code(code, 0)
        .map( |asm| asm.len() )
        .unwrap_or(0);

    let before_cases = Case::before_cases();
    let before = if before_cases.is_empty() {
        0
    } else {
        emit_binary_code(Case::before_cases(), 0)
            .map( |asm| asm.len() )
            .unwrap_or(0)
    } + F::size_before_cases(table);

    let before_entries = F::size_before_entries(table);
    before_entries + before + cases + entries + error
}

fn split<T>(slice: &[T]) -> (&[T], &T, &[T]) {
    if slice.len() == 2 {
        (&slice[0 .. 1], &slice[1], &[])
    } else {
        let len = slice.len();
        let half = len / 2;

        // If the slice length is odd, there is an obvious pivot. Otherwise, we take the element at
        // half the length as pivot. Since the slice is 0-indexed, we get the desired left bias.
        let pivot_idx = half;

        (&slice[0 .. pivot_idx], &slice[pivot_idx], &slice[pivot_idx + 1 ..])
    }
}

fn leaf_count_recursive<T>(slice: &[T]) -> usize {
    if slice.len() < 2 {
        slice.len()
    } else {
        let (left, _, right) = split(slice);
        let right_count = if right.is_empty() { 0 } else { leaf_count_recursive(right) };
        leaf_count_recursive(left) + right_count
    }
}

fn total_size_tree_table<F, Case, E>(table: &DispatchTable<F, Case, E>) -> usize
where F: Feature, Case: DispatchCase<F>, E: Clone + Display {
    let entries = table.entries.first().map( |entry| entry.offset as usize ).unwrap_or(0);

    let case_count = table.cases.len();
    let leafs = leaf_count_recursive(table.cases.as_slice());
    let nodes = case_count - leafs;

    let cases = case_count * Case::estimate_size() + leafs * 7 + nodes * 13;

    let code = format!("{}", table.error_case);
    let error = emit_binary_code(code, 0)
        .map( |asm| asm.len() )
        .unwrap_or(0);

    let before_cases = Case::before_cases();
    let before = if before_cases.is_empty() {
        0
    } else {
        emit_binary_code(Case::before_cases(), 0)
            .map( |asm| asm.len() )
            .unwrap_or(0)
    } + F::size_before_cases(table);

    let before_entries = F::size_before_entries(table);
    before_entries + before + cases + entries + error
}

fn one_step<F, Case>(slice: &[Case]) -> String
where F: Feature, Case: DispatchCase<F> {
    if slice.is_empty() {
        "jmp _table_error_case".to_string()
    } else if slice.len() == 1 {
        let case = &slice[0];
        format!("{}\njmp _table_error_case\n", case)
    } else {
        let reg = Case::preferred_register();
        let (left, pivot, right) = split(slice);
        let id = pivot.identifier() as u32;
        let lcode = one_step(left);
        let rcode = one_step(right);

        let pcode = format!("{}", pivot);
        let dispatch = format!("cmp ${:#x}, {}\njg _{0:#x}_pivot_rcode", id, reg);
        format!("{}\n{}\n{}\n_{:#x}_pivot_rcode:\n{}", pcode, dispatch, lcode, id, rcode)
    }
}

/// Display the table as if it was a binary tree indexed on the case ID.
fn display_tree_table<F, Case, E>(
    table: &DispatchTable<F, Case, E>,
    fmt: &mut Formatter<'_>) -> FmtResult
where F: Feature, Case: DispatchCase<F>, E: Clone + Display {
    let mut sorted_cases = table.cases.clone();
    sorted_cases.sort_by_key( |a| a.identifier() );

    let rax_entry = DispatchEntry::rax_entry();

    writeln!(fmt, "{}\n", F::before_entries(table))?;

    for entry in table.entries.iter() {
        if *entry != rax_entry {
            write!(fmt, "{}", entry)?;
        }
    }

    if !table.cases.is_empty() {
        writeln!(fmt, "_table_cases:")?;

        writeln!(fmt, "{}", Case::before_cases())?;

        writeln!(fmt, "{}", F::entering_table(table))?;

        let tree = one_step(sorted_cases.as_slice());
        writeln!(fmt, "{}", tree)?;
    }

    write!(fmt, "_table_error_case:\n{}\n", table.error_case)?;

    Ok(())
}

pub fn linear_table_strategy<F, Case, E>() -> Strategy<F, Case, E>
where F: Feature, Case: DispatchCase<F>, E: Clone + Display {
    Strategy {
        command: display_linear_table,
        total_size: total_size_linear_table
    }
}

pub fn tree_table_strategy<F, Case, E>() -> Strategy<F, Case, E>
where F: Feature, Case: DispatchCase<F>, E: Clone + Display {
    Strategy {
        command: display_tree_table,
        total_size: total_size_tree_table
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_leaf_count() {
        let counts: std::ops::Range<usize> = 1 .. 16;

        let exp: Vec<usize> = vec![1, 1, 2, 2, 2, 3, 4, 4, 4, 4, 4, 5, 6, 7, 8];
        let got: Vec<usize> = counts.map( |n| {
            let slice: Vec<usize> = (1..n+1).collect();
            leaf_count_recursive(&slice)
        }).collect();

        assert_eq!(exp, got);
    }

    #[test]
    fn test_split() {
        let slice1 = [1, 2, 3, 4, 5];
        let res1 = split(&slice1);
        assert_eq!((vec![1, 2].as_slice(), &3, vec![4, 5].as_slice()), res1);

        let slice2 = [1, 2, 3, 4];
        let res2 = split(&slice2);
        assert_eq!((vec![1, 2].as_slice(), &3, vec![4].as_slice()), res2);
    }
}
