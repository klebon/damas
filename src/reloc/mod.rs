//! Module for everything related to code relocation.
//!
//! This module and its submodules handle everything that is related to relocation. In Damas, this
//! means that the original code of the target process is rewritten almost as is in a dynamically
//! allocated buffer and then modified to add protections.
//!
//! Every indirect jumps are replaced by direct jumps to [`dispatch tables`] and direct jumps are
//! translated so that their operand still corresponds to the right target instruction. Ret
//! instructions are considered as indirect jumps but their translation is done afterwards for
//! technical reasons.
//!
//! [`dispatch tables`]: ./dispatch/index.html

pub mod dispatch;
pub mod feature;
pub mod manager;
pub mod stext;
pub mod patch;
pub mod trans;

use std::cmp::Ordering;

use elf::types::SectionHeader;

pub use crate::reloc::manager::*;

#[derive(Clone, Debug, Eq)]
pub struct Relocation {
    pub origin: u64,
    pub target: u64,
    pub size: u64,
    pub new_size: u64
}

impl PartialEq for Relocation {
    fn eq(&self, other: &Self) -> bool {
        self.origin == other.origin
    }
}

impl Ord for Relocation {
    fn cmp(&self, other: &Self) -> Ordering {
        self.origin.cmp(&other.origin)
    }
}

impl PartialOrd for Relocation {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Relocation {
    pub fn relocate(&self, address: u64) -> u64 {
        address - self.origin + self.target
    }

    pub fn original(&self, address: u64) -> u64 {
        address - self.target + self.origin
    }
}

/// This trait is an abstraction over the information needed inside a section header for the
/// relocation manager to work. In practice, the relocation manager needs to know what addresses
/// are inside the .text section to only relocate them (i.e. leave the .plt alone).
pub trait SectionRange {
    fn begin(&self, load_address: u64) -> u64;
    fn end(&self, load_address: u64) -> u64;
}

impl SectionRange for SectionHeader {
    fn begin(&self, load_address: u64) -> u64 {
        self.addr + load_address
    }

    fn end(&self, load_address: u64) -> u64 {
        self.addr + self.size + load_address
    }
}

impl SectionRange for (u64, u64) {
    fn begin(&self, load_address: u64) -> u64 {
        self.0 + load_address
    }

    fn end(&self, load_address: u64) -> u64 {
        self.1 + load_address
    }
}
