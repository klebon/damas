use std::fmt::Display;
use std::marker::PhantomData;

use crate::reloc::dispatch::{DispatchCase, DispatchTable};

pub trait Feature: Clone + PartialEq {
    fn size_in_case() -> usize { 0 }

    fn size_before_entries<F, Case, E>(_: &DispatchTable<F, Case, E>) -> usize
    where F: Feature, Case: DispatchCase<F>, E: Clone + Display {
        0
    }

    fn size_before_cases<F, Case, E>(_: &DispatchTable<F, Case, E>) -> usize
    where F: Feature, Case: DispatchCase<F>, E: Clone + Display {
        0
    }

    fn before_entries<F, Case, E>(_: &DispatchTable<F, Case, E>) -> String
    where F: Feature, Case: DispatchCase<F>, E: Clone + Display {
        String::new()
    }

    fn entering_case<F, Case>(_: &Case) -> String where F: Feature, Case: DispatchCase<F> {
        String::new()
    }

    fn exiting_case<F, Case>(_: &Case) -> String where F: Feature, Case: DispatchCase<F> {
        String::new()
    }

    fn entering_table<F, Case, E>(_: &DispatchTable<F, Case, E>) -> String
    where F: Feature, Case: DispatchCase<F>, E: Clone + Display {
        String::new()
    }
}

impl Feature for () {
}

#[derive(Clone, PartialEq)]
pub struct Both<First, Second> where First: Feature, Second: Feature {
    _first: PhantomData<First>,
    _second: PhantomData<Second>
}

impl<First, Second> Feature for Both<First, Second> where First: Feature, Second: Feature {
    fn size_in_case() -> usize {
        First::size_in_case() + Second::size_in_case()
    }

    fn size_before_entries<F, Case, E>(table: &DispatchTable<F, Case, E>) -> usize
    where F: Feature, Case: DispatchCase<F>, E: Clone + Display {
        First::size_before_entries(table) + Second::size_before_entries(table)
    }

    fn size_before_cases<F, Case, E>(table: &DispatchTable<F, Case, E>) -> usize
    where F: Feature, Case: DispatchCase<F>, E: Clone + Display {
        First::size_before_cases(table) + Second::size_before_cases(table)
    }

    fn before_entries<F, Case, E>(table: &DispatchTable<F, Case, E>) -> String
    where F: Feature, Case: DispatchCase<F>, E: Clone + Display {
        format!("{}\n{}", First::before_entries(table), Second::before_entries(table))
    }

    fn entering_case<F, Case>(case: &Case) -> String where F: Feature, Case: DispatchCase<F> {
        format!("{}\n{}", First::entering_case(case), Second::entering_case(case))
    }

    fn exiting_case<F, Case>(case: &Case) -> String where F: Feature, Case: DispatchCase<F> {
        format!("{}\n{}", First::entering_case(case), Second::exiting_case(case))
    }

    fn entering_table<F: Feature, Case, E>(table: &DispatchTable<F, Case, E>) -> String
    where F: Feature, Case: DispatchCase<F>, E: Clone + Display {
        format!("{}\n{}", First::entering_table(table), Second::entering_table(table))
    }
}

#[derive(Clone, PartialEq)]
pub struct Counter;

impl Feature for Counter {
    fn size_in_case() -> usize {
        7 // incl _some_address
    }

    fn size_before_entries<F, Case, E>(table: &DispatchTable<F, Case, E>) -> usize
    where F: Feature, Case: DispatchCase<F>, E: Clone + Display {
        table.cases.len() * 4 + 4
    }

    fn size_before_cases<F, Case, E>(_: &DispatchTable<F, Case, E>) -> usize
    where F: Feature, Case: DispatchCase<F>, E: Clone + Display {
        7
    }

    fn before_entries<F, Case, E>(table: &DispatchTable<F, Case, E>) -> String
    where F: Feature, Case: DispatchCase<F>, E: Clone + Display {
        let mut res = String::from("_counter_for_table: .long 0x0\n");
        let mut cases = table.cases.clone();
        cases.sort_by_key( |a| a.identifier() );

        for case in cases.iter() {
            let line = format!("_counter_for_{:#x}: .long 0x0\n", case.identifier() as u32);
            res.push_str(line.as_str());
        }

        res
    }

    fn entering_case<F, Case>(case: &Case) -> String where F: Feature, Case: DispatchCase<F> {
        format!("incl _counter_for_{:#x}(%rip)", case.identifier() as u32)
    }

    fn entering_table<F, Case, E>(_: &DispatchTable<F, Case, E>) -> String
    where F: Feature, Case: DispatchCase<F>, E: Clone + Display {
        String::from("incl _counter_for_table(%rip)")
    }
}
