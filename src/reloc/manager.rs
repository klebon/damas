use std::collections::HashMap;
use std::collections::hash_map::IntoIter as HashMapIntoIter;
use std::collections::hash_map::Values;

use capstone::arch::x86::X86OperandType;

use crate::insn::Insn;
use crate::reloc::{Relocation, SectionRange};

/// The relocation manager is an abstraction over the list of basic-block relocations. It is able
/// to translate an original address to the relocated one and vice-versa. It can also make the
/// difference between an address from the .text section and an address from the .plt section.
pub struct RelocationManager<R: SectionRange + Clone> {
    forward_addresses: Vec<u64>,
    backward_addresses: Vec<u64>,
    relocs: HashMap<u64, Relocation>,
    text: R,
    load_address: u64,
    benched_reloc: Option<(u64, u64)>
}

pub struct IntoIter {
    pairs: HashMapIntoIter<u64, Relocation>
}

impl Iterator for IntoIter {
    type Item = Relocation;

    fn next(&mut self) -> Option<Self::Item> {
        self.pairs.next()
            .map( |(_, reloc)| reloc )
    }
}

impl<R: SectionRange + Clone> IntoIterator for RelocationManager<R> {
    type Item = Relocation;
    type IntoIter = IntoIter;

    fn into_iter(self) -> Self::IntoIter {
        IntoIter {
            pairs: self.relocs.into_iter()
        }
    }
}

impl<R: SectionRange + Clone> RelocationManager<R> {
    pub fn new(text: &R, load_address: u64) -> Self {
        RelocationManager {
            forward_addresses: vec![],
            backward_addresses: vec![],
            relocs: HashMap::new(),
            text: text.clone(),
            load_address: load_address,
            benched_reloc: None
        }
    }

    pub fn iter(&self) -> Values<'_, u64, Relocation> {
        self.relocs.values()
    }

    /// Compute the relocated target of a direct jump. The instruction must be a direct jump,
    /// otherwise the program will panic because of the normally unreachable path.
    pub fn direct_jump_target(&self, insn: &Insn) -> u64 {
        use X86OperandType::*;

        let target = match insn.operands[0].op_type {
            Imm(i) => i as u64,
            Mem(mem) => insn.address + mem.disp() as u64 + insn.size as u64,
            _ => unreachable!()
        };

        let text_begin = self.text.begin(self.load_address);
        let text_end = self.text.end(self.load_address);

        if text_begin <= target && target < text_end {
            if let Some(new_target) = self.relocated_address(target) {
                new_target
            } else {
                println!("Could not find a relocation for the address {:#x}", target);
                target
            }
        } else {
            target
        }
    }

    /// Add a new relocation to the RelocationManager.
    pub fn insert(&mut self, origin: u64, size: usize, target: u64, new_size: usize) {
        let reloc = Relocation {
            origin: origin,
            size: size as u64,
            target: target,
            new_size: new_size as u64
        };

        self.forward_addresses.push(origin);
        self.backward_addresses.push(target);
        self.relocs.insert(origin, reloc);
    }

    pub fn set_benched_relocation(&mut self, reloc: (u64, u64)) {
        self.benched_reloc = Some(reloc)
    }

    /// Get the relocation of the basic block that contains the given address.
    pub fn reloc_for_address(&self, address: u64) -> Option<&Relocation> {
        let reloc_addr = match self.forward_addresses.binary_search(&address) {
            Ok(_) => address,
            Err(0) => 0,
            Err(idx) => self.forward_addresses[idx - 1]
        };

        let reloc = self.relocs.get(&reloc_addr)?;

        if address < reloc.origin + reloc.size {
            Some(reloc)
        } else {
            let reloc_addr = match self.backward_addresses.binary_search(&address) {
                Ok(idx) => self.forward_addresses[idx],
                Err(0) => 0,
                Err(idx) => self.forward_addresses[idx - 1]
            };

            let reloc = self.relocs.get(&reloc_addr)?;
            if address < reloc.target + reloc.new_size { Some(reloc) } else { None }
        }
    }

    /// Find the basic block that contains the given instruction and return the corresponding
    /// relocation.
    pub fn bb_reloc_from_insn(&self, insn: &Insn) -> Option<&Relocation> {
        self.reloc_for_address(insn.address)
    }

    /// Get the address of the relocated instruction that correspond to the instruction at the
    /// given address.
    pub fn relocated_address(&self, addr: u64) -> Option<u64> {
        self.reloc_for_address(addr)
            .map( |reloc| reloc.relocate(addr) )
    }

    /// Same as [`relocated_address`] but takes into account that there may be a
    /// benchmarked-function wrapper. This way, it returns the relocated address of the function,
    /// not the one of the wrapper.
    ///
    /// [`relocated_address`]: ./struct.RelocationManager.html#method.relocated_address
    pub fn relocated_address_with_wrapper(&self, addr: u64) -> Option<u64> {
        if let Some((target, wrapper)) = self.benched_reloc {
            let reloc = self.reloc_for_address(addr)?;

            if reloc.target <= wrapper && wrapper < reloc.target + reloc.new_size {
                Some(reloc.relocate(addr) - reloc.target + target)
            } else {
                self.relocated_address(addr)
            }
        } else {
            self.relocated_address(addr)
        }
    }

    /// Get the address of the original instruction that correspond to the one relocated at the
    /// given address.
    pub fn original_address(&self, addr: u64) -> Option<u64> {
        let reloc_addr = match self.backward_addresses.binary_search(&addr) {
            Ok(idx) => self.forward_addresses[idx],
            Err(0) => 0,
            Err(idx) => self.forward_addresses[idx-1]
        };

        self.relocs.get(&reloc_addr).map( |reloc| reloc.original(addr) )
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn create_reloc_manager() {
        RelocationManager::new(&(0x1000, 0xffff), 0);
    }

    #[test]
    fn relocated_address() {
        let mut reloc = RelocationManager::new(&(0x1000, 0xffff), 0);
        reloc.insert(0x1024, 0x10, 0x10000, 0x15);

        let got = reloc.relocated_address(0x1024);
        assert_eq!(Some(0x10000), got);
    }

    #[test]
    fn relocated_address_in_block() {
        let mut reloc = RelocationManager::new(&(0x1000, 0xffff), 0);
        reloc.insert(0x1024, 0x10, 0x10000, 0x15);

        let got = reloc.relocated_address(0x1030);
        assert_eq!(Some(0x1000c), got);
    }

    #[test]
    fn original_address() {
        let mut reloc = RelocationManager::new(&(0x1000, 0xffff), 0);
        reloc.insert(0x1024, 0x10, 0x10000, 0x15);

        let got = reloc.original_address(0x10000);
        assert_eq!(Some(0x1024), got);
    }

    #[test]
    fn original_address_in_block() {
        let mut reloc = RelocationManager::new(&(0x1000, 0xffff), 0);
        reloc.insert(0x1024, 0x10, 0x10000, 0x15);

        let got = reloc.original_address(0x1000f);
        assert_eq!(Some(0x1033), got);
    }
}
