//! Module for instruction translations.
//!
//! This module contains functions that take care of the actual instruction translations after
//! basic-blocks have been injected. The goal of these functions is to ensure that the translated
//! jump instruction leads to the equivalent address inside the relocated code or to a
//! [`dispatch table`].
//!
//! [`dispatch table`]: ../dispatch/index.html

use std::convert::TryFrom;
use std::fmt::{Debug, Display};
use std::marker::PhantomData;

use capstone::arch::x86::X86OperandType;

use sorry::assembly::create_capstone_object;
use sorry::buffer::Buffer;
use sorry::error::Error as SorryError;

use crate::asm::emit_binary_code;
use crate::disass::BasicBlock;
use crate::error::{DamasError, Result};
use crate::insn::{CALL_GROUP, REG_RIP, Insn};
use crate::reloc::{RelocationManager, SectionRange};
use crate::reloc::dispatch::{DispatchCase, DispatchTable};
use crate::reloc::feature::Feature;

/// Size of a relative direct call.
const CALL_REL_SIZE: usize = 5;

/// This trait defines types that can be translated to binary code. This can be a binary code
/// buffer or a string that contains assembly code.
pub trait IntoBinary: Debug {
    fn into_binary(self, address: u64) -> Result<Vec<u8>>;
}

impl IntoBinary for Vec<u8> {
    fn into_binary(self, _: u64) -> Result<Vec<u8>> {
        Ok(self)
    }
}

impl IntoBinary for String {
    fn into_binary(self, address: u64) -> Result<Vec<u8>> {
        emit_binary_code(self, address)
    }
}

/// Layer over a dispatch table that make it injectable into a Buffer.
pub struct InjectableDispatchTable<B, F, Case, E>
where B: Buffer, F: Feature, Case: DispatchCase<F>, E: Clone + Display {
    pub address: Option<u64>,
    pub cache: Option<B>,
    pub dt: DispatchTable<F, Case, E>,
    _feature: PhantomData<F>
}

impl<B, F, Case, E> InjectableDispatchTable<B, F, Case, E>
where B: Buffer, F: Feature, Case: DispatchCase<F>, E: Clone + Display {
    /// Create a new InjectableDispatchTable from a DispatchTable.
    pub fn new(dt: DispatchTable<F, Case, E>) -> Self {
        InjectableDispatchTable::<B, F, Case, E> {
            address: None,
            cache: None,
            dt: dt,
            _feature: PhantomData
        }
    }

    /// Emit the binary code from the table and inject it into the given cache.
    ///
    /// The cache must be correctly allocated, otherwise the emission will fail.
    pub fn emit(&mut self, mut cache: B, offset: usize) -> Result<()> {
        let address = cache.get_address();
        let bin = self.dt.emit(address)?;

        cache.write(&bin, offset)?;

        self.address = Some(self.dt.table_offset(address));
        self.cache = Some(cache);

        Ok(())
    }
}

/// Translate a jump instruction to make it target the right address. Depending on the operand,
/// different translation strategies are applied:
///
/// If the jump is direct (either immediate or RIP relative), the operand is changed to the
/// relocated address corresponding to the original target address.
///
/// If the jump is an indirect call, it is redirected to the correct entry of the dispatch table.
/// If the jump is an indirect jump it is redirected to it's function's dispatch table.
pub fn translate_jump<B, F, C, E, R>(
    relocs: &RelocationManager<R>,
    insn: &Insn,
    dt: &InjectableDispatchTable<B, F, C, E>) -> Option<Vec<u8>>
where B: Buffer, F: Feature, C: DispatchCase<F>, E: Clone + Display, R: Clone + SectionRange {
    let new_rip = relocs.relocated_address_with_wrapper(insn.address)?;
    let mut next_rip = new_rip + CALL_REL_SIZE as u64;

    if ! insn.is_indirect_jump() {
        let opcode = insn.bytes[0];
        let new_opcode: u8 = if insn.is_a(CALL_GROUP) { 0xe8 } else { 0xe9 };

        let new_target = relocs.direct_jump_target(insn);
        let offset_brut = new_target.wrapping_sub(next_rip) as i64;

        if opcode == 0xff {
            // movabs the address into %rax.
            let operand = new_target.to_ne_bytes();
            let mut res = vec![0x48, 0xb8];
            res.extend(operand.iter());
            // mov (%rax), %rax.
            res.extend([0x48, 0x8b, 0x00].iter());

            next_rip += res.len() as u64;

            let dt_offset = dt.address?.wrapping_sub(next_rip) as u32;
            res.extend(&[new_opcode]);
            res.extend(&dt_offset.to_ne_bytes());

            Some(res)
        } else if (std::i32::MIN as i64) < offset_brut && offset_brut < std::i32::MAX as i64 {
            let offset = offset_brut as i32;
            let operand = offset.to_ne_bytes();
            let mut res = vec![new_opcode];
            res.extend(operand.iter());
            Some(res)
        } else {
            // movabs the address to %rax.
            let operand = new_target.to_ne_bytes();
            let mut res = vec![0x48, 0xb8];
            res.extend(operand.iter());
            // Far call to %rax.
            res.extend([0xff, 0xd0].iter());
            Some(res)
        }
    }

    else {
        let target = insn.operands[0].op_type.clone();
        let dt_offset = dt.dt.entry_offset(target)?;

        // Set the right opcode. If the instruction is a call, it is 0xe8. If it is a jmp, it
        // is 0xe9.
        let mut res = if ! insn.is_a(CALL_GROUP) {
            // If the instruction is a jmp, %rax is saved and then restored by the table.
            next_rip += 5;
            vec![0x48, 0x89, 0x44, 0x24, 0xe8, 0xe9]
        } else {
            vec![0xe8]
        };

        let offset = (dt.address? - dt_offset).wrapping_sub(next_rip) as i32;
        let operand = offset.to_ne_bytes();

        res.extend(operand.iter());
        Some(res)
    }
}

/// Translate a Jcc jump instruction. Since these instructions only take relative offsets as
/// parameter, the translation is straightforward. The main challenge is to take care of the
/// instruction size. Indeed, if the instruction was a 2-byte Jcc but the new offset is too big to
/// be encoded into a 2-byte instruction, it becomes a 5-byte instruction.
///
/// The target address is retrieved to compute the relocated target address. Then, the new offset
/// is computed and injected into the old binary code to replace the old offset.
pub fn translate_jcc<R: Clone + SectionRange>(relocs: &RelocationManager<R>, insn: &Insn)
-> Option<Vec<u8>> {
    use X86OperandType::*;

    let op = &insn.operands[0].op_type;

    let old_target = match *op {
        Imm(i) => i as u64,
        Mem(mem) => mem.disp() as u64 + insn.address + insn.size as u64,
        _ => unreachable!()
    };

    let new_rip = relocs.relocated_address_with_wrapper(insn.address)? + insn.size as u64;
    let new_target = relocs.relocated_address(old_target)?;

    let offset = new_target.wrapping_sub(new_rip);
    let signed_offset = offset as i64;

    // If the instruction is 2-byte long, then the operand is rel8, otherwise, it is a rel16/32
    // operand.
    Some(if insn.size == 2 && -0x80 < signed_offset && signed_offset < 0x80 {
        vec![insn.bytes[0], offset as u8]
    } else if insn.size == 2 {
        // The 4 LSB are kept and the 7 becomes an 8.
        let opcode = (insn.bytes[0] & 0x0f) + 0x80;
        let mut vec = vec![0x0f, opcode];

        // If the new offset is positive and increase past 0x80, the jump instruction must become a
        // rel32 jcc and the offset will be decreased by 4. However if the offset is negative, it
        // will be increased by 4.
        let op = (signed_offset - 4) as u32;
        vec.extend(op.to_ne_bytes().iter());
        vec
    } else {
        let op_size = insn.size - 2;
        let mut vec = vec![0x0f, insn.bytes[1]];

        // rel16 or rel32 ?
        if op_size == 2 {
            let op = offset as u16;
            vec.extend(op.to_ne_bytes().iter());
        } else {
            let op = offset as u32;
            vec.extend(op.to_ne_bytes().iter());
        }

        vec
    })
}

/// Retrieve the offset in the instruction where the given value appears.
fn operand_offset(insn: &Insn, disp: i32) -> usize {
    let mut offset = 0;
    let disp_bytes = disp.to_ne_bytes();

    while ! insn.bytes[offset..].starts_with(&disp_bytes) {
        offset += 1
    }

    offset
}

/// Translate any RIP relative instruction so that the operand value stays unchanged. Only jumps
/// need to be aware of relocations, other instructions must be able to access data from
/// RIP-relative addresses.
pub fn translate_rip_rel_insn<R: Clone + SectionRange>(relocs: &RelocationManager<R>, insn: &Insn)
-> Option<Vec<u8>> {
    use X86OperandType::*;

    insn.operands.iter().find( |op| {
        matches!(op.op_type, Mem(sibd) if sibd.base() == REG_RIP || sibd.index() == REG_RIP)
    }).and_then( |op| {
        // Copy the current instruction.
        let mut bytes = insn.bytes.clone();
        let old_rip = insn.address as i64 + insn.size as i64;

        // Compute the new instruction address and its old operand value.
        let new_rip = relocs.relocated_address_with_wrapper(insn.address)? + insn.size as u64;
        let target = match op.op_type {
            Mem(sibd) => sibd.disp() + old_rip,
            _ => unreachable!()
        };

        // Compute the new offset to rewrite the operand as offset(%rip).
        let offset = (target - new_rip as i64) as i32;
        let offset_bytes = offset.to_ne_bytes();
        let op_offset = operand_offset(insn, (target - old_rip) as i32);

        for i in &[0, 1, 2, 3] {
            bytes[op_offset + i] = offset_bytes[*i];
        }

        Some(bytes)
    })
}

/// Translate a whole basic block except its terminator.
pub fn translate_whole_basic_block<R: Clone + SectionRange>(relocs: &RelocationManager<R>,
                                                            bb: &BasicBlock) -> Option<Vec<u8>> {
    let cs = create_capstone_object();

    let mut insns: Vec<Insn> = cs.disasm_all(&bb.bytes, bb.address)
        .map_err( |err| DamasError::from(SorryError::CapstoneError(err)) )
        .and_then( |insns| insns.iter().map(Insn::try_from).collect() )
        .ok()?;


    if bb.terminator.is_forward_jump() {
        let _ = insns.pop();
    }

    let binary: Option<Vec<Vec<u8>>> = insns.into_iter()
        .map( |insn| {
            let opt = translate_rip_rel_insn(relocs, &insn);
            (insn, opt)
        })
        .map( |(insn, opt)| if opt.is_none() {
            if insn.op_str.contains("%rip") {
                eprintln!("Could not translate instruction {:#x}: {} {}",
                    insn.address, insn.mnemonic, insn.op_str);
            }

            Some(insn.bytes)
        } else { opt } )
        .collect();

    binary.map( |v| -> Vec<u8> { v.into_iter().flatten().collect() })
        .map( |mut v| {
            if bb.terminator.is_forward_jump() {
                v.extend(&bb.terminator.bytes);
            }

            v
        })
}

#[cfg(test)]
mod tests {
    use super::*;

    use crate::reloc::dispatch::{DispatchJump, DispatchTable};
    use crate::reloc::dispatch::commands;

    /// Alias type for the tables manipulated in these tests.
    type DummyTable<'a> = InjectableDispatchTable<Vec<u8>, (), DispatchJump<()>, &'a str>;

    fn create_dummy_dispatch_table<'a>() ->  DummyTable<'a> {
        let mut dt = InjectableDispatchTable::new(DispatchTable::new("nop"));
        dt.dt.display_strategy = commands::linear_table_strategy();

        dt.dt.add_block(0x1024, 0x10, 0x10000);
        dt.dt.add_block(0x1035, 0x10, 0x10010);
        dt.dt.add_block(0x1050, 0x8, 0x10028);

        dt
    }

    fn create_reloc_manager() -> RelocationManager<(u64, u64)> {
        let mut reloc = RelocationManager::new(&(0x1000, 0xffff), 0);

        reloc.insert(0x1024, 0x10, 0x10000, 0x10);
        reloc.insert(0x1035, 0x10, 0x10010, 0x18);
        reloc.insert(0x1050, 0x8, 0x10028, 0x8);

        reloc
    }

    fn disasm(code: &[u8], address: u64) -> Option<Insn> {
        let cs = create_capstone_object();
        let insns = cs.disasm_all(code, address).ok()?;

        insns.iter().last().map( |insn| Insn::try_from(insn).unwrap() )
    }

    #[test]
    fn translate_rip_rel_mov() {
        let insn = disasm(&[0x48, 0x8b, 0x05, 0x13, 0x00, 0x00, 0x00], 0x1024).unwrap();
        let reloc = create_reloc_manager();

        let tr = translate_rip_rel_insn(&reloc, &insn);

        assert_eq!(Some(vec![0x48, 0x8b, 0x05, 0x37, 0x10, 0xff, 0xff]), tr);
    }
}
