use std::collections::VecDeque;
use std::fmt::Display;
use std::marker::PhantomData;

use sorry::buffer::Buffer;

use crate::asm::emit_binary_code;
use crate::error::Result;
use crate::reloc::dispatch::{DispatchCase, DispatchTable};
use crate::reloc::feature::Feature;
use crate::reloc::trans::InjectableDispatchTable;

#[derive(Debug, PartialEq)]
pub enum TableModification<F: Feature, Case: DispatchCase<F>> {
    Replacement(usize, Case),
    Swap(usize, usize),
    _Feature(PhantomData<F>)
}

pub struct TablePatcher<'a, B: Buffer, F: Feature, Case: DispatchCase<F>, E: Clone + Display> {
    original: &'a mut InjectableDispatchTable<B, F, Case, E>,
    mods: VecDeque<TableModification<F, Case>>
}

impl<'a, B, F, Case, E> TablePatcher<'a, B, F, Case, E>
where B: Buffer, F: Feature, Case: DispatchCase<F>, E: Clone + Display {
    /// Create a new patcher from the given table that serves as an original on which modifications
    /// will be appied.
    pub fn new(table: &mut InjectableDispatchTable<B, F, Case, E>) -> TablePatcher<B, F, Case, E> {
        TablePatcher {
            original: table,
            mods: VecDeque::new()
        }
    }

    /// Get the original table.
    pub fn original(&self) -> &InjectableDispatchTable<B, F, Case, E> {
        self.original
    }

    /// Replace the dispatch case at the given index with a new one.
    pub fn replace_at(&mut self, idx: usize, case: Case) {
        let m = TableModification::Replacement(idx, case);
        self.mods.push_back(m);
    }

    /// Swap the cases at the given indexes.
    pub fn swap_at(&mut self, idx1: usize, idx2: usize) {
        let m = TableModification::Swap(idx1, idx2);
        self.mods.push_back(m);
    }

    /// Replace the given case with a new one.
    pub fn replace(&mut self, orig: &Case, with: Case) -> Option<usize> {
        let idx = self.original.dt.cases.iter().position( |elem| elem == orig )?;
        let m = TableModification::Replacement(idx, with);
        self.mods.push_back(m);

        Some(idx)
    }

    /// Swap the given cases.
    pub fn swap(&mut self, orig: &Case, with: &Case) -> Option<(usize, usize)> {
        let idx1 = self.original.dt.cases.iter().position( |elem| elem == orig )?;
        let idx2 = self.original.dt.cases.iter().position( |elem| elem == with )?;
        let m = TableModification::Swap(idx1, idx2);
        self.mods.push_back(m);

        Some((idx1, idx2))
    }

    /// Apply all the modifications and return the new table.
    pub fn patch(mut self) -> Result<()> {
        for modif in self.mods {
            apply_modification(&mut self.original, modif)?;
        }

        Ok(())
    }

    pub fn patch_whole_table(mut self) -> Result<()> {
        for modif in self.mods {
            apply_modification_no_emit(&mut self.original, modif)?;
        }

        let address = self.original.address.unwrap();
        let cache = self.original.cache.as_mut().unwrap();
        let buffer = self.original.dt.emit(address)?;
        cache.write(buffer.as_ref(), 0).map_err( |err| err.into() )
    }

    pub fn patch_whole_table_no_write(mut self) -> Result<Vec<u8>> {
        for modif in self.mods {
            apply_modification_no_emit(&mut self.original, modif)?;
        }

        let address = self.original.address.unwrap();
        self.original.dt.emit(address)
    }
}

fn apply_modification_no_emit<B, F, Case, E>(
    table: &mut InjectableDispatchTable<B, F, Case, E>,
    modif: TableModification<F, Case>) -> Result<()>
where B: Buffer, F: Feature, Case: DispatchCase<F>, E: Clone + Display {
    match modif {
        TableModification::Replacement(idx, case) => table.dt.cases[idx] = case,
        TableModification::Swap(idx1, idx2) => {
            let case1 = table.dt.cases[idx1].clone();
            let case2 = table.dt.cases[idx2].clone();

            apply_modification_no_emit(table, TableModification::Replacement(idx1, case2))?;
            apply_modification_no_emit(table, TableModification::Replacement(idx2, case1))?;
        }

        TableModification::_Feature(_) => {}
    }

    Ok(())
}

/// Apply the given TableModification on the given table.
fn apply_modification<B: Buffer, F: Feature, Case: DispatchCase<F>, E: Clone + Display>(
    table: &mut InjectableDispatchTable<B, F, Case, E>,
    modif: TableModification<F, Case>) -> Result<()> {
    let buffer = &mut table.cache.as_mut().unwrap();
    let first_case_offset = table.dt.table_offset(0);

    match modif {
        TableModification::Replacement(idx, case) => {
            let offset = case_address(&table.dt, first_case_offset, idx);
            let address = table.address.as_ref().unwrap() + offset;

            let code = format!("{}", case);
            let bin = emit_binary_code(code, address)?;

            buffer.write(&bin, offset as usize)?;

            table.dt.cases[idx] = case;
        }

        TableModification::Swap(idx1, idx2) => {
            let case1 = table.dt.cases[idx1].clone();
            let case2 = table.dt.cases[idx2].clone();

            apply_modification(table, TableModification::Replacement(idx1, case2))?;
            apply_modification(table, TableModification::Replacement(idx2, case1))?;
        }

        TableModification::_Feature(_) => {}
    }

    Ok(())
}

fn case_address<F: Feature, Case: DispatchCase<F>, E: Clone + Display>(
    _: &DispatchTable<F, Case, E>,
    base: u64,
    idx: usize) -> u64 {
    base + (idx * Case::estimate_size()) as u64
}

#[cfg(test)]
#[allow(unused_imports)]
mod test_patcher {
    use std::collections::VecDeque;
    use std::marker::PhantomData;

    use crate::reloc::dispatch::{DispatchTable, DispatchFunction};
    use crate::reloc::dispatch::commands;
    use crate::reloc::patch::{TablePatcher, TableModification};
    use crate::reloc::trans::InjectableDispatchTable;

    fn create_empty_table<'a>()
    -> InjectableDispatchTable::<Vec<u8>, (), DispatchFunction<()>, &'a str> {
        let mut dt = DispatchTable::<(), DispatchFunction<()>, &str>::new("nop\n");
        dt.display_strategy = commands::linear_table_strategy();
        InjectableDispatchTable::<Vec<u8>, (), DispatchFunction<()>, &str>::new(dt)
    }

    #[test]
    fn create_a_valid_patcher() {
        let mut jdt = create_empty_table();
        TablePatcher::new(&mut jdt);
    }

    #[test]
    fn add_modifications_at() {
        let mut jdt = create_empty_table();
        let mut patcher = TablePatcher::new(&mut jdt);

        patcher.swap_at(0, 1);
        patcher.swap_at(1, 2);
        patcher.replace_at(3, DispatchFunction::new(0x1234, 0x2048));

        let mut deque = VecDeque::new();
        deque.push_back(TableModification::Swap(0, 1));
        deque.push_back(TableModification::Swap(1, 2));
        deque.push_back(TableModification::Replacement(3, DispatchFunction::new(0x1234, 0x2048)));

        assert_eq!(patcher.mods, deque);
    }

    #[test]
    fn add_modifications() {
        let mut jdt = create_empty_table();
        let mut patcher = TablePatcher::new(&mut jdt);

        let f1 = DispatchFunction::new(0x1, 0x10);
        let f2 = DispatchFunction::new(0x2, 0x20);
        let f3 = DispatchFunction::new(0x3, 0x30);
        let f4 = DispatchFunction::new(0x4, 0x40);

        patcher.original.dt.add_function(0x1, 0x10);
        patcher.original.dt.add_function(0x2, 0x20);
        patcher.original.dt.add_function(0x3, 0x30);
        patcher.original.dt.add_function(0x4, 0x40);

        patcher.swap(&f1, &f2);
        patcher.swap(&f2, &f3);
        patcher.replace(&f4, DispatchFunction::new(0x1234, 0x2048));

        let mut deque = VecDeque::new();
        deque.push_back(TableModification::Swap(0, 1));
        deque.push_back(TableModification::Swap(1, 2));
        deque.push_back(TableModification::Replacement(3, DispatchFunction::new(0x1234, 0x2048)));

        assert_eq!(patcher.mods, deque);
    }

    #[test]
    fn apply_modifications() {
        let mut jdt = create_empty_table();
        let mut patcher = TablePatcher::new(&mut jdt);

        let f1 = DispatchFunction::new(0x1001, 0x1010);
        let f2 = DispatchFunction::new(0x1002, 0x1020);
        let f3 = DispatchFunction::new(0x1003, 0x1030);

        patcher.original.dt.add_function(0x1001, 0x1010);
        patcher.original.dt.add_function(0x1002, 0x1020);
        patcher.original.dt.add_function(0x1003, 0x1030);

        eprintln!("{}", patcher.original.dt);

        patcher.swap(&f1, &f2);
        patcher.swap(&f2, &f3);

        let mut buf = Vec::<u8>::new();
        buf.resize(patcher.original.dt.estimate_size(), 0);

        patcher.original.address = Some(0x1000);
        let res = patcher.original.emit(buf, 0);
        assert!(res.is_ok());

        let res = patcher.patch();
        assert!(res.is_ok());

        let exp = vec![
            0x3d, 0x02, 0x10, 0x00, 0x00, 0x75, 0x0c, 0x48, 0xc7, 0xc0, 0x20, 0x10, 0x00, 0x00,
            0xff, 0xe0, 0x90, 0x90, 0x90, 0x3d, 0x03, 0x10, 0x00, 0x00, 0x75, 0x0c, 0x48, 0xc7,
            0xc0, 0x30, 0x10, 0x00, 0x00, 0xff, 0xe0, 0x90, 0x90, 0x90, 0x3d, 0x01, 0x10, 0x00,
            0x00, 0x75, 0x0c, 0x48, 0xc7, 0xc0, 0x10, 0x10, 0x00, 0x00, 0xff, 0xe0, 0x90, 0x90,
            0x90, 0x90
        ];

        assert_eq!(jdt.cache.unwrap(), exp);
    }
}
