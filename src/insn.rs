use std::convert::TryFrom;

use capstone::Insn as CsInsn;
use capstone::{InsnGroupId, RegId};
use capstone::arch::ArchOperand;
use capstone::arch::x86::{X86InsnGroup, X86Operand, X86OperandType, X86Reg};

use sorry::assembly::create_capstone_object;

use crate::error::{DamasError, Result};

pub const REG_RIP: RegId = RegId(X86Reg::X86_REG_RIP as u16);
pub const REG_INVALID: RegId = RegId(X86Reg::X86_REG_INVALID as u16);

pub const CALL_GROUP: InsnGroupId = InsnGroupId(X86InsnGroup::X86_GRP_CALL as u8);
pub const JUMP_GROUP: InsnGroupId = InsnGroupId(X86InsnGroup::X86_GRP_JUMP as u8);
pub const RET_GROUP: InsnGroupId = InsnGroupId(X86InsnGroup::X86_GRP_RET as u8);

/// Build a RegId from a X86Reg value.
pub fn reg(r: u32) -> RegId {
    RegId(r as u16)
}

/// Escape the leading '*' character from a jump target if it is a register.
pub fn escape_target_str(op_str: &str, op: &X86OperandType) -> String {
    match op {
        X86OperandType::Reg(_) => {
            if let Some(stripped) = op_str.strip_prefix('*') {
                stripped.to_string()
            } else {
                op_str.to_string()
            }
        }
        _ => op_str.to_string()
    }
}

#[derive(Clone, Debug, PartialEq)]
pub struct Insn {
    pub address: u64,
    pub size: usize,
    pub bytes: Vec<u8>,
    pub op_str: String,
    pub mnemonic: String,
    pub operands: Vec<X86Operand>,
    pub groups: Vec<InsnGroupId>
}

impl<'a> TryFrom<CsInsn<'a>> for Insn {
    type Error = DamasError;

    fn try_from(insn: CsInsn<'a>) -> Result<Self> {
        use ArchOperand::*;

        let cs = create_capstone_object();
        let detail = cs.insn_detail(&insn)?;

        let operands = detail.arch_detail().operands().iter().map( |op| {
            match op {
                X86Operand(op) => op.clone(),
                _ => unreachable!()
            }
        }).collect();

        let groups = detail.groups().collect();

        Ok(Insn {
            address: insn.address(),
            size: insn.bytes().len(),
            bytes: insn.bytes().into(),
            op_str: insn.op_str().unwrap().to_string(),
            mnemonic: insn.mnemonic().unwrap().to_string(),
            operands: operands,
            groups: groups
        })
    }
}

impl Insn {
    /// Test if the instruction belongs to the given group.
    pub fn is_a(&self, group: InsnGroupId) -> bool {
        self.groups.iter().any( |g| *g == group)
    }

    /// Test if the instruction is a forward jump.
    pub fn is_forward_jump(&self) -> bool {
        self.groups.iter().any( |g| *g == CALL_GROUP || *g == JUMP_GROUP )
    }

    /// Test if the instruction is a jump.
    pub fn is_jump(&self) -> bool {
        self.groups.iter().any( |g| *g == CALL_GROUP || *g == JUMP_GROUP || *g == RET_GROUP )
    }

    /// Test if the instruction is a forward jump and if the jump is indirect.
    pub fn is_indirect_jump(&self) -> bool {
        use X86OperandType::*;

        if self.is_forward_jump() {
            match self.operands[0].op_type {
                Imm(_) => false,
                Mem(mem) if mem.base() == REG_RIP && mem.index() == REG_INVALID => false,
                _ => true
            }
        } else {
            false
        }
    }
}

#[cfg(test)]
mod tests {
    use sorry::assembly::create_capstone_object;

    use super::*;

    #[test]
    fn is_a() {
        let buf = vec![
            0xe8, 0x00, 0x00, 0x00, 0x00,
            0xff, 0x25, 0x05, 0x00, 0x00, 0x00,
            0xff, 0xd0
        ];

        let cs = create_capstone_object();
        let insns_res = cs.disasm_all(&buf, 0);

        assert!(insns_res.is_ok());

        let insns = insns_res.unwrap();

        let got_call: Result<Vec<bool>> = insns
            .iter()
            .map( |insn| Insn::try_from(insn).map( |i| i.is_a(CALL_GROUP) ) )
            .collect();

        assert!(got_call.is_ok());
        assert_eq!(vec![true, false, true], got_call.unwrap());

        let got_jmp: Result<Vec<bool>> = insns
            .iter()
            .map( |insn| Insn::try_from(insn).map( |i| i.is_a(JUMP_GROUP) ) )
            .collect();

        assert!(got_jmp.is_ok());
        assert_eq!(vec![false, true, false], got_jmp.unwrap());
    }

    #[test]
    fn is_indirect_jump() {
        let buf = vec![
            0xe8, 0x00, 0x00, 0x00, 0x00,
            0xff, 0x25, 0x05, 0x00, 0x00, 0x00,
            0xff, 0xd0,
            0x48, 0x01, 0x14, 0x25, 0x04, 0x00, 0x00, 0x00
        ];

        let cs = create_capstone_object();
        let insns = cs.disasm_all(&buf, 0);

        assert!(insns.is_ok());

        let got: Result<Vec<bool>> = insns.unwrap()
            .iter()
            .map( |insn| Insn::try_from(insn).map( |i| i.is_indirect_jump() ) )
            .collect();

        assert!(got.is_ok());
        assert_eq!(vec![false, false, true, false], got.unwrap());
    }
}
