//! Error module of Damas.

use std::io::Error as IOError;

use capstone::Error as CsError;

use nix::Error as NixError;

use pyo3::PyErr;

use sorry::error::Error as SorryError;

use crate::disass::light_disass::get_exn_str;
use crate::eh_frame::Error as EhFrameError;
use crate::monitor::Error as MonitorError;

/// Error type for the whole project.
#[derive(Debug)]
pub enum DamasError {
    /// Error in Capstone lirary.
    CsError(CsError),
    /// IO Error.
    IOError(IOError),
    /// Error in Sorry library.
    SorryError(SorryError),
    /// Error in .eh_frame information discovery.
    EhFrameError(EhFrameError),
    /// Error in Nix library.
    NixError(NixError),
    /// PyO3 error.
    PyError(PyErr),
    /// Error in the monitoring of the target process.
    MonitorError(MonitorError),
    /// Custom error that has a string representation.
    CustomError(String)
}

impl From<CsError> for DamasError {
    fn from(err: CsError) -> Self {
        DamasError::CsError(err)
    }
}

impl From<IOError> for DamasError {
    fn from(err: IOError) -> Self {
        DamasError::IOError(err)
    }
}

impl From<NixError> for DamasError {
    fn from(err: NixError) -> Self {
        DamasError::NixError(err)
    }
}

impl From<SorryError> for DamasError {
    fn from(err: SorryError) -> Self {
        DamasError::SorryError(err)
    }
}

impl From<EhFrameError> for DamasError {
    fn from(err: EhFrameError) -> Self {
        DamasError::EhFrameError(err)
    }
}

impl From<MonitorError> for DamasError {
    fn from(err: MonitorError) -> Self {
        DamasError::MonitorError(err)
    }
}

impl From<PyErr> for DamasError {
    fn from(err: PyErr) -> Self {
        DamasError::PyError(err)
    }
}

impl PartialEq for DamasError {
    fn eq(&self, rhs: &Self) -> bool {
        use DamasError::*;

        match (self, rhs) {
            (CsError(err1), CsError(err2)) => err1 == err2,
            (IOError(err1), IOError(err2)) => err1.raw_os_error() == err2.raw_os_error(),
            (SorryError(err1), SorryError(err2)) => err1 == err2,
            (EhFrameError(err1), EhFrameError(err2)) => err1 == err2,
            (NixError(err1), NixError(err2)) => err1 == err2,
            (MonitorError(err1), MonitorError(err2)) => err1 == err2,
            (CustomError(err1), CustomError(err2)) => err1 == err2,
            (PyError(err1), PyError(err2)) => {
                let s1 = get_exn_str(err1);
                let s2 = get_exn_str(err2);
                s1 == s2
            }
            _ => false
        }
    }
}

pub type Result<T> = std::result::Result<T, DamasError>;
