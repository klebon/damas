//! Module for disassembly manipulation. This module defines the Disassembly trait and some
//! implementations such as the MockDisassembly for testing and LightDisassembly that is built with
//! the Python library angr.

use std::cmp::Ordering;
use std::fmt::Display;

pub mod light_disass;

pub use crate::disass::light_disass::LightDisassembly;
use crate::error::Result;
use crate::insn::{escape_target_str, Insn};
use crate::reloc::dispatch::{DispatchCase, DispatchTable, Target};
use crate::reloc::feature::Feature;

/// Shallow representation of a basic block. It only contains its address, size and raw bytes. The
/// only instruction that is disassembled is the terminator.
#[derive(Clone, Debug, PartialEq)]
pub struct BasicBlock {
    pub address: u64,
    pub size: u64,
    pub bytes: Vec<u8>,
    pub terminator: Insn
}

/// Shallow representation of a function in the binary. It contains the start and end addresses as
/// well as the blocks that compose the function.
#[derive(Clone, Debug)]
pub struct Function {
    pub start_address: u64,
    pub end_address: u64,
    pub blocks: Vec<BasicBlock>
}

impl PartialEq for Function {
    fn eq(&self, other: &Self) -> bool {
        self.start_address == other.start_address
    }
}

impl Eq for Function {
}

impl Ord for Function {
    fn cmp(&self, other: &Self) -> Ordering {
        self.start_address.cmp(&other.start_address)
    }
}

impl PartialOrd for Function {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

/// Kind of edge in the Disassembly. This enum tells what kind of instruction terminates the given basic
/// block.
#[derive(Debug, PartialEq, Eq)]
pub enum EdgeKind {
    /// A function call.
    Call,
    /// A function return.
    Ret,
    /// A jump, it does not matter whether it is conditional or not.
    Jump,
    /// Any non control-flow transfer instruction.
    Misc
}

impl From<&str> for EdgeKind {
    fn from(s: &str) -> Self {
        match s {
            "call" => EdgeKind::Call,
            "callq" => EdgeKind::Call,
            "ret" => EdgeKind::Ret,
            "jmp" => EdgeKind::Jump,
            "jmpq" => EdgeKind::Jump,
            _ => EdgeKind::Misc
        }
    }
}

#[derive(Debug)]
pub struct CfgEdge {
    pub kind: EdgeKind,
    pub source: u64,
    pub destinations: Vec<u64>
}

/// Get the jump target of every CTI terminator of the list of basic blocks.
pub fn block_terminator_targets(blocks: &[BasicBlock])
-> Result<Vec<(EdgeKind, Target)>> {
    let mut ops = Vec::with_capacity(blocks.len());

    for bb in blocks {
        let term = &bb.terminator;

        if ! term.is_forward_jump() { continue };
        if term.operands.is_empty() { continue };

        let op = term.operands[0].clone();
        let target = Target {
            op_type: op.op_type.clone(),
            op_str: escape_target_str(&term.op_str, &op.op_type)
        };

        // Create the EdgeKind using the instruction's mnemonic.
        let kind = EdgeKind::from(term.mnemonic.as_str());
        ops.push((kind, target));
    }

    Ok(ops)
}

pub trait Disassembly {
    /// Return the functions that compose this Disassembly.
    fn functions(&self) -> Result<Vec<Function>>;

    /// Return a list of pairs of address and size that represent the basic blocks of the program.
    fn blocks(&self) -> Result<Vec<(u64, usize)>>;

    /// Return a list of basic blocks placed relative to a load_address.
    fn basic_blocks(&self) -> Result<Vec<BasicBlock>>;

    /// Use the process' indirect control-flow instructions to build the entries of the given
    /// dispatch table. Only the instructions of the given kind are taken care of, allowing to
    /// populate tables for calls or jumps.
    fn populate_dispatch_table<F, Case, E>(
        &self,
        dt: &mut DispatchTable<F, Case, E>,
        blocks: &[BasicBlock],
        kind: EdgeKind) -> Result<()>
    where F: Feature, Case: DispatchCase<F>, E: Clone + Display {
        use capstone::arch::x86::X86OperandType::*;

        block_terminator_targets(blocks)
            .map( |targets| {
                for (k, target) in targets {
                    if k != kind { continue }
                    // RIP relative jumps are considered direct at this point.
                    if target.is_rip_relative() { continue };
                    match target.op_type {
                        Reg(_) | Mem(_) => dt.add_entry(target),
                        _ => ()
                    }
                }
            })
    }
}

/// Structure that mocks a Disassembly. It is totally useless in production but tests should use it.
pub struct MockDisassembly {
    pub functions: Vec<Function>,
    pub blocks: Vec<(u64, usize)>,
    pub basic_blocks: Vec<BasicBlock>
}

impl Disassembly for MockDisassembly {
    fn functions(&self) -> Result<Vec<Function>> {
        Ok(self.functions.clone())
    }

    fn blocks(&self) -> Result<Vec<(u64, usize)>> {
        Ok(self.blocks.clone())
    }

    fn basic_blocks(&self) -> Result<Vec<BasicBlock>> {
        Ok(self.basic_blocks.clone())
    }
}
