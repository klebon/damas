use std::convert::TryFrom;
use std::env::current_exe;
use std::fs::File;
use std::io::Read;
use std::path::Path;

use pyo3::prelude::*;
use pyo3::types::{IntoPyDict, PyBool, PyList, PyString};

use sorry::assembly::*;

use crate::disass::{BasicBlock, Disassembly, Function};
use crate::eh_frame::EhFrameFunction;
use crate::error::{DamasError, Result};
use crate::insn::Insn;

/// Helper function that simplifies utilization of Python inside the functions of this module.
pub fn with_python<F, R, E: Into<Result<R>>>(func: F) -> E where F: FnOnce(Python) -> E {
    let gil = Python::acquire_gil();
    let py = gil.python();

    func(py)
}

/// Retrieve the LightCFG Python class and returns the corresponding PyObject.
pub fn light_cfg_class(py: Python) -> PyResult<PyObject> {
    // The directory containing Damas executable file.
    let exe_dir = current_exe()
        .map( |path| path.parent().and_then(&Path::to_str).unwrap().to_string() )
        .unwrap();
    let cwd = PyString::new(py, &exe_dir);

    // Initialize the Python environment so that it can find the light.py file.
    py_run!(py, cwd, r#"
    import sys
    if cwd not in sys.path:
        sys.path.insert(0, cwd)
    "#);
    let light = py.import("light")?;
    light.get("LightCFG").map( |x| x.to_object(py) )
}

/// Rust binding of the Python class "LightDisassembly" from the light module. The point of this class is
/// to allow Damas to get the function list from the .eh_frame of an ELF object as well as a Disassembly of
/// the program that it can manipulate to add protections to it.
#[derive(Debug)]
pub struct LightDisassembly {
    py_obj: PyObject,
    load_address: u64
}

impl Disassembly for LightDisassembly {
    /// Get every functions in the binary that angr was able to discover.
    fn functions(&self) -> Result<Vec<Function>> {
        with_python( |py| {
            // Retrieve a list of (start address, end address, block-bound list) from angr.
            self.py_obj.call_method0(py, "functions")
                .and_then( |res| res.extract::<Vec<(u64, u64, Vec<(u64, usize)>)>>(py) )
                .map_err(DamasError::from)
                // Build basic blocks from the list of block bounds.
                .and_then( |vec| {
                    let mut file = File::open(&self.executable_path()?)?;
                    let mut buf = Vec::new();
                    file.read_to_end(&mut buf)?;

                    let len = buf.len() as u64;
                    vec.into_iter()
                        .filter( |(_, end, _)| (*end).wrapping_sub(self.load_address) < len )
                        .map( |(addr, end, blks)| {
                            let valid_blks = blks.into_iter()
                                .filter( |(addr, _)| self.load_address <= *addr )
                                .collect();
                            self.function_from(&buf, addr, end, valid_blks)
                        })
                        .collect()
                })
        })
    }

    /// Get the address and size of every blocks in the Disassembly.
    fn blocks(&self) -> Result<Vec<(u64, usize)>> {
        with_python( |py| {
            self.py_obj.call_method0(py, "blocks")
                .and_then( |res| {
                    Ok(res.extract::<Vec<(u64, usize)>>(py)?
                        .into_iter()
                        .filter( |(addr, _)| self.load_address <= *addr )
                        .collect())
                })
                .map_err(DamasError::from)
        })
    }

    /// Get every basic blocks of the program.
    fn basic_blocks(&self) -> Result<Vec<BasicBlock>> {
        let mut file = File::open(&self.executable_path()?)?;
        let mut buf = Vec::new();
        file.read_to_end(&mut buf)?;

        let mut res: Vec<BasicBlock> = vec![];
        let blocks = self.blocks()?
            .into_iter()
            .filter( |(addr,size)| {
                let lower_bound = addr.wrapping_sub(self.load_address);
                let upper_bound = lower_bound + (*size as u64);
                upper_bound < buf.len() as u64
            });

        for (address, size) in blocks {
            if let Some(bb) = self.basic_block(&buf, address, size)? {
                res.push(bb)
            }
        }

        Ok(res)
    }
}

impl LightDisassembly {
    /// Build a new LightDisassembly for the given program loaded at the given address.
    pub fn new(path: &Path, load_address: u64) -> Result<Self> {
        with_python( |py| {
            let light = light_cfg_class(py)?;
            let py_path = path.to_str().unwrap().to_object(py);

            let main_opts = [("base_addr", load_address)].into_py_dict(py);
            let kwargs = [("main_opts", main_opts)].into_py_dict(py);

            light.call(py, (py_path,), Some(kwargs))
                .map( |obj| Self { py_obj: obj, load_address: load_address } )
                .map_err(DamasError::from)
        })
    }

    /// Get the path of the executable file used to build this Disassembly.
    pub fn executable_path(&self) -> Result<String> {
        with_python( |py| {
            self.py_obj.getattr(py, "executable_path")?
                .extract::<String>(py)
                .map_err(DamasError::from)
        })
    }

    /// Find and disassemble the basic block at the given address in the buffer and create a
    /// BasicBlock object.
    fn basic_block(&self, buf: &[u8], address: u64, size: usize) -> Result<Option<BasicBlock>> {
        let cs = create_capstone_object();

        let lower_bound = address as usize - self.load_address as usize;
        let upper_bound = lower_bound + size;

        let slice = &buf[lower_bound .. upper_bound];
        let insns = cs.disasm_all(slice, address)?;

        let insn_opt = insns.iter().last();

        let insn_res = match insn_opt.map(Insn::try_from) {
            None => Ok(None),
            Some(Err(e)) => Err(e),
            Some(Ok(i)) => Ok(Some(i))
        };

        Ok(insn_res?.map( |insn| BasicBlock {
            address: address,
            size: size as u64,
            bytes: slice.to_vec(),
            terminator: insn
        }))
    }

    /// Build a Function from the blocks given in parameter.
    fn function_from(&self, buf: &[u8], start: u64, end: u64, blk_pairs: Vec<(u64, usize)>)
    -> Result<Function> {
        let blocks = blk_pairs.into_iter()
            .filter(|(_, size)| *size > 0 )
            .map( |(addr, size)| self.basic_block(buf, addr, size).transpose() )
            .flatten()
            .collect::<Result<Vec<BasicBlock>>>()?;

        Ok(Function {
            start_address: start,
            end_address: end,
            blocks: blocks
        })
    }

    /// Tell if the binary is position independant.
    pub fn is_pic(&self) -> Result<bool> {
        with_python( |py| {
            self.py_obj.call_method0(py, "is_pic")
                .and_then( |res| res.extract::<bool>(py) )
                .map_err(DamasError::from)
        })
    }

    /// Generate a Disassembly for the given binary using the provided function starts. If indirect is
    /// true, angr will try to resolve indirect jumps.
    pub fn generate_cfg(&self, indirect: bool, functions: Option<Vec<EhFrameFunction>>)
    -> Result<()> {
        with_python( |py| {
            let py_indirect = PyBool::new(py, indirect);

            match functions {
                Some(funcs) => {
                    let starts = funcs.iter()
                        .map( |f| f.start.to_object(py) );

                    let py_starts = PyList::new(py, starts);
                    self.py_obj.call_method1(py, "generate_cfg", (py_indirect, py_starts))
                        .map_err(DamasError::from)
                }

                None => self.py_obj
                    .call_method1(py, "generate_cfg", (py_indirect,))
                    .map_err(DamasError::from)
            }
        })?;
        Ok(())
    }

    /// Retrieve a block from its offset inside the ELF file.
    /// TODO: rename this feature because it uses offsets or make angr load at the right address.
    pub fn block_from_address(&self, address: u64) -> Result<PyObject> {
        with_python( |py| {
            self.py_obj
                .call_method1(py, "block_from_address", (address,))
                .map_err(DamasError::from)
        })
    }
}

pub fn get_exn_str(d: impl ToPyObject) -> String {
    let gil = Python::acquire_gil();
    let py = gil.python();
    d.to_object(py)
        .call_method0(py, "__str__")
        .and_then( |res| res.extract(py) )
        .unwrap_or_else( |_| "Could not extract".to_string())
}

#[cfg(test)]
mod tests {
    use crate::disass::block_terminator_targets;
    use crate::eh_frame::scan_eh_frame;
    use crate::error::DamasError;

    use super::*;

    #[test]
    fn get_light_cfg_class() {
        let gil = Python::acquire_gil();
        let py = gil.python();

        let light_res = light_cfg_class(py);
        if light_res.is_err() {
            let err = get_exn_str(light_res.unwrap_err());
            assert_ne!(err, err);
        }
    }

    #[test]
    fn new_light_cfg() {
        let cfg = LightDisassembly::new(Path::new("/bin/false"), 0);

        match cfg {
            Err(DamasError::PyError(py_err)) => {
                let err = get_exn_str(py_err);
                assert_ne!(err, err);
            }
            _ => {}
        }
    }

    #[test]
    #[ignore]
    fn light_cfg_functions() {
        let binary = Path::new("resources/dummy");

        let funcs = scan_eh_frame(binary).unwrap();
        let res = LightDisassembly::new(binary, 0)
            .and_then( |cfg| { cfg.generate_cfg(false, Some(funcs))?; Ok(cfg) })
            .and_then( |cfg| {
                let funcs: Vec<u64> = cfg.functions()?.iter().map( |f| f.start_address ).collect();
                let exp = vec![0x1020, 0x1040, 0x1139, 0x1150, 0x11c0];

                assert_eq!(exp, funcs);
                Ok(())
            });

        match res {
            Err(DamasError::PyError(py_err)) => {
                let err = get_exn_str(py_err);
                assert_ne!(err, err);
            }
            _ => {}
        }
    }

    #[test]
    fn light_cfg_block_terminator_operands() {
        let binary = Path::new("resources/no-pie");

        let _funcs = scan_eh_frame(binary).unwrap();
        let res = LightDisassembly::new(binary, 0x400000)
            .and_then( |cfg| { cfg.generate_cfg(false, None)?; Ok(cfg) })
            .and_then( |cfg| {
                let term_operands = block_terminator_targets(&cfg.basic_blocks()?)?;
                let exp_operands = vec![
                    "0x401016", "%rax", "*0x2fe4(%rip)", "*0x2fe2(%rip)", "0x401020",
                    "*0x2f7a(%rip)", "0x4010a0", "0x4010a0", "%rax", "0x4010e8", "0x4010e8",
                    "%rax", "0x401110", "0x401080", "0x4010b0", "0x401030", "0x401000", "0x401196",
                    "*(%r12, %rbx, 8)", "0x401180"
                ];
                let op_strs: Vec<&str> = term_operands.iter()
                    .map( |(_, tgt)| tgt.op_str.as_str() )
                    .collect();

                assert_eq!(exp_operands, op_strs);
                Ok(())
            });

        if res.is_err() {
            match res.unwrap_err() {
                DamasError::PyError(py_err) => {
                    let err = get_exn_str(py_err);
                    panic!("Python error: {:?}", err);
                }

                err => panic!("{:?}", err)
            }
        }
    }

    #[test]
    fn light_cfg_basic_blocks() {
        let binary = Path::new("resources/no-pie");

        let res = LightDisassembly::new(binary, 0x400000)
            .and_then( |cfg| { cfg.generate_cfg(false, None)?; Ok(cfg) })
            .and_then( |cfg| {
                let mut blocks = cfg.basic_blocks()?;
                blocks.sort_by( |a, b| a.address.partial_cmp(&b.address).unwrap() );

                assert_eq!(blocks.len(), 42);

                let addresses: Vec<u64> = blocks.iter().map( |bb| bb.address ).collect();
                let exp_addr = vec![
                    0x401000, 0x401014, 0x401016, 0x401020, 0x40102c, 0x401030, 0x401036, 0x401040,
                    0x40106e, 0x40106f, 0x401070, 0x401075, 0x401080, 0x40108d, 0x401099, 0x4010a0,
                    0x4010a1, 0x4010b0, 0x4010cf, 0x4010db, 0x4010e2, 0x4010e8, 0x4010e9, 0x4010f0,
                    0x4010fd, 0x401106, 0x40110f, 0x401110, 0x401111, 0x401120, 0x401126, 0x401136,
                    0x40113d, 0x401140, 0x401172, 0x401178, 0x401180, 0x40118d, 0x401196, 0x4011a5,
                    0x4011b0, 0x4011b8
                ];
                assert_eq!(exp_addr, addresses);

                let bytes: Vec<Vec<u8>> = blocks.iter()
                    .map( |bb| bb.terminator.bytes.clone() )
                    .collect();
                let exp_bytes = vec![
                    vec![0x74, 0x2],
                    vec![0xff, 0xd0],
                    vec![0xc3],
                    vec![0xff, 0x25, 0xe4, 0x2f, 0x0, 0x0],
                    vec![0xf, 0x1f, 0x40, 0x0],
                    vec![0xff, 0x25, 0xe2, 0x2f, 0x0, 0x0],
                    vec![0xe9, 0xe0, 0xff, 0xff, 0xff],
                    vec![0xff, 0x15, 0x7a, 0x2f, 0x0, 0x0],
                    vec![0xf4],
                    vec![0x90],
                    vec![0xc3],
                    vec![0x90],
                    vec![0x74, 0x13],
                    vec![0x74, 0x7],
                    vec![0xff, 0xe0],
                    vec![0xc3],
                    vec![0xf, 0x1f, 0x40, 0x0],
                    vec![0x74, 0x19],
                    vec![0x74, 0xd],
                    vec![0xff, 0xe0],
                    vec![0x66, 0xf, 0x1f, 0x44, 0x0, 0x0],
                    vec![0xc3],
                    vec![0xf, 0x1f, 0x80, 0x0, 0x0, 0x0, 0x0],
                    vec![0x75, 0x13],
                    vec![0xe8, 0x7a, 0xff, 0xff, 0xff],
                    vec![0xc3],
                    vec![0x90],
                    vec![0xc3],
                    vec![0xf, 0x1f, 0x40, 0x0],
                    vec![0xeb, 0x8a],
                    vec![0xe8, 0xfa, 0xfe, 0xff, 0xff],
                    vec![0xc3],
                    vec![0xf, 0x1f, 0x0],
                    vec![0x67, 0xe8, 0x8e, 0xfe, 0xff, 0xff],
                    vec![0x74, 0x1e],
                    vec![0x66, 0xf, 0x1f, 0x44, 0x0, 0x0],
                    vec![0x41, 0xff, 0x14, 0xdc],
                    vec![0x75, 0xea],
                    vec![0xc3],
                    vec![0x66, 0x66, 0x2e, 0xf, 0x1f, 0x84, 0x0, 0x0, 0x0, 0x0, 0x0],
                    vec![0xc3],
                    vec![0xc3]
                ];
                assert_eq!(exp_bytes, bytes);

                Ok(blocks)
            });

        if res.is_err() {
            match res.unwrap_err() {
                DamasError::PyError(py_err) => {
                    let err = get_exn_str(py_err);
                    panic!("Python error: {:?}", err);
                }

                err => panic!("{:?}", err)
            }
        }
    }
}
