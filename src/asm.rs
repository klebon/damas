use std::env::current_exe;
use std::io::Write;
use std::path::PathBuf;
use std::process::{Command, Stdio};

use crate::error::{DamasError, Result};

fn executable_path() -> Result<PathBuf> {
    let mut dir = current_exe()?;
    dir.pop();
    Ok(dir)
}

/// Call the script as.sh to assemble the given assembly code and return the generated binary code.
pub fn emit_binary_code(code: String, addr: u64) -> Result<Vec<u8>> {
    let mut file = executable_path()?;
    file.push("as.sh");

    let mut child = Command::new(file.to_str().unwrap_or("./as.sh"))
        .arg(format!("0x{:x}", addr))
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .spawn()
        .map_err(DamasError::IOError)?;

    {
        let stdin = child.stdin.as_mut().unwrap();
        stdin.write_all(code.as_bytes())?;
    }

    child.wait_with_output()
        .map( |bin| bin.stdout )
        .map_err(DamasError::IOError)
}
